//
//  LocalAgentTests.swift
//  LamaProTests
//
//  Created by Михаил on 22.01.2023.
//

import XCTest
@testable import LamaPro

class LocalAgentTests: XCTestCase {
    
    static let context = LocalAgent.Context(cacheFolderName: "cache", encoder: JSONEncoder(), decoder: JSONDecoder(), fileManager: FileManager.default)
    let localAgent = LocalAgent(context: LocalAgentTests.context)
    
    override func tearDownWithError() throws {
        
        let rootFolderURL = try localAgent.rootFolderURL()
        let urls = try Self.context.fileManager.contentsOfDirectory(at: rootFolderURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
        for url in urls {
            
            try Self.context.fileManager.removeItem(at: url)
        }
    }
    
    //MARK: - FileName
    
    func testFileName_Item() throws {
        
        // when
        let result = localAgent.fileName(for: SampleType.self)
        
        // then
        XCTAssertEqual(result, "sampletype.json")
    }
    
    func testFileName_Array() throws {
        
        // when
        let result = localAgent.fileName(for: [SampleType].self)
        
        // then
        XCTAssertEqual(result, "array_sampletype.json")
    }
    
    func testFileName_Dictionary() throws {
        
        // when
        let result = localAgent.fileName(for: Dictionary<String, SampleType>.self)
        
        // then
        XCTAssertEqual(result, "dictionary_string_sampletype.json")
    }
    
    func testFileName_Set() throws {
        
        // when
        let result = localAgent.fileName(for: Set<SampleType>.self)
        
        // then
        XCTAssertEqual(result, "set_sampletype.json")
    }
    
    //MARK: - Store
    
    func testStore_Item() throws {
        
        // given
        let sample = SampleType()
        
        // when
        try localAgent.store(sample)
        
        // then
        let rootFolderURL = try localAgent.rootFolderURL()
        let fileUrl = rootFolderURL.appendingPathComponent("sampletype.json")
        let data = try Data(contentsOf: fileUrl)
        let result = try JSONDecoder().decode(SampleType.self, from: data)
        
        XCTAssertEqual(result, sample)
    }
    
    func testStore_Array() throws {
        
        // given
        let sample = SampleType()
        
        // when
        try localAgent.store([sample])
        
        // then
        let rootFolderURL = try localAgent.rootFolderURL()
        let fileUrl = rootFolderURL.appendingPathComponent("array_sampletype.json")
        let data = try Data(contentsOf: fileUrl)
        let result = try JSONDecoder().decode([SampleType].self, from: data)
        
        XCTAssertEqual(result, [sample])
    }
    
    func testStore_Dictionary() throws {
        
        // given
        let sample = ["sample": SampleType()]
        // when
        try localAgent.store(sample)
        
        // then
        let rootFolderURL = try localAgent.rootFolderURL()
        let fileUrl = rootFolderURL.appendingPathComponent("dictionary_string_sampletype.json")
        let data = try Data(contentsOf: fileUrl)
        let result = try JSONDecoder().decode(Dictionary<String, SampleType>.self, from: data)
        
        XCTAssertEqual(result, sample)
    }
    
    //MARK: - Load
    
    func testLoad_Item() throws {
        
        // given
        let sample = SampleType()
        
        // when
        try localAgent.store(sample)
        let result = localAgent.load(type: SampleType.self)
        
        // then
        XCTAssertEqual(result, sample)
    }
    
    func testLoad_Array() throws {
        
        // given
        let sample = SampleType()
        
        // when
        try localAgent.store([sample])
        let result = localAgent.load(type: [SampleType].self)
        
        // then
        XCTAssertEqual(result, [sample])
    }
    
    func testLoad_Dictionary() throws {
        
        // given
        let sample = SampleType()
        
        // when
        try localAgent.store(["sample": sample])
        let result = localAgent.load(type: Dictionary<String, SampleType>.self)
        
        // then
        XCTAssertEqual(result, ["sample": sample])
    }
    
    //MARK: - Clear
    
    func testClear_Item() throws {
        
        // given
        let sample = SampleType()
        try localAgent.store(sample)
        let rootFolderURL = try localAgent.rootFolderURL()
        let fileUrl = rootFolderURL.appendingPathComponent("sampletype.json")
        XCTAssertTrue(Self.context.fileManager.fileExists(atPath: fileUrl.path))
        
        // when
        try localAgent.clear(type: SampleType.self)
        
        // then
        XCTAssertFalse(Self.context.fileManager.fileExists(atPath: fileUrl.path))
    }
    
    func testClear_Array() throws {
        
        // given
        let sample = SampleType()
        try localAgent.store([sample])
        let rootFolderURL = try localAgent.rootFolderURL()
        let fileUrl = rootFolderURL.appendingPathComponent("array_sampletype.json")
        XCTAssertTrue(Self.context.fileManager.fileExists(atPath: fileUrl.path))
        
        // when
        try localAgent.clear(type: [SampleType].self)
        
        // then
        XCTAssertFalse(Self.context.fileManager.fileExists(atPath: fileUrl.path))
    }
    
    func testClear_Dictionary() throws {
        
        // given
        let sample = SampleType()
        try localAgent.store(["sample" : sample])
        let rootFolderURL = try localAgent.rootFolderURL()
        let fileUrl = rootFolderURL.appendingPathComponent("dictionary_string_sampletype.json")
        XCTAssertTrue(Self.context.fileManager.fileExists(atPath: fileUrl.path))
        
        // when
        try localAgent.clear(type: Dictionary<String, SampleType>.self)
        
        // then
        XCTAssertFalse(Self.context.fileManager.fileExists(atPath: fileUrl.path))
    }
    
}

extension LocalAgentTests {
    
    struct SampleType: Codable, Hashable, Equatable {
        
        var id = UUID()
    }
}
