//
//  ServerAgentTests.swift
//  LamaProTests
//
//  Created by Михаил on 22.01.2023.
//

import XCTest
@testable import LamaPro

class ServerAgentTests: XCTestCase {
    
    let serverAgent = ServerAgent(enviroment: .test)
    let token = UUID().uuidString

    func testRequest_With_ServerCommand() throws {

        // given
        let command = ServerCommands.AuthController.Login()
        
        // when
        let request = try serverAgent.request(with: command)
        
        // then
        XCTAssertEqual(request.httpMethod, "GET")
        XCTAssertNil(request.httpBody)
//        XCTAssertEqual(request.value(forHTTPHeaderField: "X-XSRF-TOKEN"), token)
    }
}
