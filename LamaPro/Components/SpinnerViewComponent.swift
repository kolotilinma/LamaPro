//
//  SpinnerViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 23.01.2023.
//

import SwiftUI

//MARK: - ViewModel

public extension SpinnerView {
    
    class ViewModel: ObservableObject {
        
        @Published var isAnimating: Bool
        @Published var subTitle: String?
        
        public init(isAnimating: Bool = true, subTitle: String? = "Подождите...") {
            
            self.subTitle = subTitle
            self.isAnimating = isAnimating
        }
    }
}

//MARK: - View

public struct SpinnerView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    public init(viewModel: SpinnerView.ViewModel) {
        
        self.viewModel = viewModel
    }
    
    public var body: some View {
        
        ZStack {
            
            Color.black
                .opacity(0.3)
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 16) {
                
                if let subTitle = viewModel.subTitle {
                    
                    Text(subTitle)
                        .foregroundColor(.lamaGrayDark)
                        .font(.interRegular(size: 13))
                        .padding(.top, 24)
                        .padding(.horizontal, 40)
                }
                
                AnimatedHUDView()
                    .frame(width: 40, height: 40)
                    .padding(.bottom, 24)
                
            }
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .foregroundColor(.lamaWhite)
                    .shadow(color: .black.opacity(0.2),
                            radius: 6, x: 2, y: 4)
            )
            .padding()
        }
    }
}
