//
//  FieldView.swift
//  LamaPro
//
//  Created by user on 20.03.2023.
//

import SwiftUI

struct fieldView<Content>: View where Content: View {
    
    var views: Content
    var imageName: String
    var title: String
    var subtitle: String?
    
    init(imageName: String, title: String, subtitle: String? = nil, @ViewBuilder content: () -> Content) {
        
        self.imageName = imageName
        self.title = title
        self.subtitle = subtitle
        self.views = content()
    }
    
    var body: some View {
        VStack {
            HStack(alignment: .top) {
                Image(imageName)
                    .foregroundColor(.lamaBlack)
                    .frame(width: 15, height: 15)
                
                Text(title)
                    .font(.interSemiBold(size: 15))
                    .foregroundColor(.lamaBlack)
                if let subtitle = subtitle {
                    Text(subtitle)
                        .font(.interRegular(size: 15))
                        .foregroundColor(.lamaBlack)
                }
                Spacer()
                if imageName == "companyMail" {
                    Button("Написать") {
                        print("hola")
                    }
                    .font(.interRegular(size: 13))
                    .foregroundColor(.lamaGreen)
                    .background(
                        Color.lamaGreen
                            .frame(height: 1)
                            .offset(y: 10)
                    )
                }
            }
            Group() {
                    EmptyView()
                        .frame(width: 15, height: 15)
                    views.foregroundColor(.black)
            }
        }
    }
}
