//
//  ProfileFooterView.swift
//  LamaPro
//
//  Created by user on 20.03.2023.
//

import SwiftUI
import WebKit


struct ProfileFooterView: View {
    @State private var showWebView = false
    
    var body: some View {
        VStack(spacing: 20) {
            
            fieldView(imageName: "workTime", title: "Режим работы:") {
                Text("ПН • ПТ c 09:00 до 18:00")
                    .font(.interRegular(size: 15))
                    .foregroundColor(.lamaBlack)
                    .frame(maxWidth: .infinity, alignment: .leading)
                Text("СБ • ВС — выходные")
                    .font(.interRegular(size: 15))
                    .foregroundColor(.lamaBlack)
                    .frame(maxWidth: .infinity, alignment: .leading)
                Divider()
            }
            
            fieldView(imageName: "companyPin", title: "Адрес:") {
                Text("МО, г. Раменское ш.\n4-й км Донинского, стр. За")
                    .font(.interRegular(size: 15))
                    .foregroundColor(.lamaBlack)
                    .frame(maxWidth: .infinity, alignment: .leading)
                Divider()
            }
            
            fieldView(imageName: "companyPhone", title: "Телефон офиса продаж:") {
                HStack {
                    Text("+7 (495) 122-22-54")
                        .font(.interRegular(size: 15))
                        .foregroundColor(.lamaBlack)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Spacer()
                    button(title: "Позвонить") {
                        
                        let phone = "+74951222254"
                        guard let url = URL(string: "telprompt://\(phone)"),
                              UIApplication.shared.canOpenURL(url) else {
                            return
                        }
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
                Divider()
            }
            
            fieldView(imageName: "companyPhone", title: "Телефон склада:") {
                HStack {
                    VStack {
                        Text("+7 (926) 363-72-32")
                            .font(.interRegular(size: 15))
                            .foregroundColor(.lamaBlack)
                            .frame(maxWidth: .infinity, alignment: .leading)
                        Text("* Звонить для самовывоза\nоплаченного заказа")
                            .font(.interRegular(size: 10))
                            .foregroundColor(.lamaBlack)
                            .frame(maxWidth: .infinity, alignment: .leading)
                    }
                    button(title: "Позвонить") {
                        
                        let phone = "+79263637232"
                        guard let url = URL(string: "telprompt://\(phone)"),
                              UIApplication.shared.canOpenURL(url) else {
                            return
                        }
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
                Divider()
            }
            
            fieldView(imageName: "companyMail", title: "E-mail:", subtitle: "info@lama-pro.ru") {
                
//                let mailtoString = "mailto:info@lama-pro.ru"
//                    .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//                guard let mailtoUrl = URL(string: mailtoString) else { }
//
//                if UIApplication.shared.canOpenURL(mailtoUrl) {
//                    
//                    UIApplication.shared.open(mailtoUrl, options: [:])
//                }
            }
            
            footer
                .padding(.top, 80)
        }
    }
}

struct ProfileFooterView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileFooterView()
    }
}


//MARK: footer view
extension ProfileFooterView {
    var footer: some View {
        VStack(spacing: 20) {
            Image("LogoSmall")
            link
            label
        }
    }
    
    var link: some View {
        
        Button {
            showWebView.toggle()
        } label: {
            Text("Политика конфиденциальности")
                .frame(height: 20)
                .font(.interRegular(size: 13))
                .foregroundColor(.lamaBlack)
                .background(
                    Color.lamaBlack
                        .frame(height: 1)
                        .offset(y: 10)
                )
        }
        .sheet(isPresented: $showWebView) {
            if let url = URL(string: "https://lama-pro.ru/") {
                WebView(url: url)
            }
        }
    }
    
    var label: some View {
        Text("© 2020 — 2022 Лама ПРО")
            .frame(height: 20)
            .font(.interRegular(size: 13))
            .foregroundColor(.lamaBlack)
        
    }
}


struct WebView: UIViewRepresentable {

    var url: URL

    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }

    func updateUIView(_ webView: WKWebView, context: Context) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}
