//
//  footerButton.swift
//  LamaPro
//
//  Created by user on 20.03.2023.
//

import SwiftUI

struct button: View {
    var title: String
    var action: () -> Void
    var body: some View {
        Button(title) {
            action()
        }
        .font(.interRegular(size: 13))
        .foregroundColor(.lamaGreen)
        .background(
            Color.lamaGreen
                .frame(height: 1) // underline's height
                .offset(y: 10) // underline's y pos
        )
    }
}
