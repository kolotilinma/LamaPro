//
//  AnimatedHUDView.swift
//  LamaPro
//
//  Created by Михаил on 23.01.2023.
//

import SwiftUI

public struct AnimatedHUDView: View {
    
    var animatedViewSize: CGSize = CGSize(width: 32, height: 32)
    var lineWidth: CGFloat = 6
    @State var degrees: CGFloat = 0
    
    public init(animatedViewSize: CGSize = CGSize(width: 32, height: 32), lineWidth: CGFloat = 6) {
        
        self.animatedViewSize = animatedViewSize
        self.lineWidth = lineWidth
    }
    
    private var foreverAnimation: Animation {
        Animation.linear(duration: 1.0)
            .repeatForever(autoreverses: false)
    }
    
    public var body: some View {
        
        let gradient = Gradient(colors: [.lamaGreen, .clear])
        let radGradient = AngularGradient(gradient: gradient, center: .center, angle: .degrees(-25))
        
        Circle()
            .trim(from: 0.0, to: 0.3)
            .stroke(style: StrokeStyle(lineWidth: lineWidth, lineCap: .round))
            .fill(radGradient)
            .rotationEffect(Angle(degrees: Double(degrees)))
        
            .animation(Animation.linear(duration: 1).repeatForever(autoreverses: false), value: degrees)
            .onAppear() {
                DispatchQueue.main.async {
                    degrees = 360
                }
            }
    }
}

struct AnimatedHUDView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        AnimatedHUDView()
            .frame(width: 40, height: 40)
            .previewLayout(.fixed(width: 50, height: 50))
        
        AnimatedHUDView(animatedViewSize: .init(width: 70, height: 70), lineWidth: 8)
            .frame(width: 70, height: 70)
            .previewLayout(.fixed(width: 80, height: 80))
    }
}
