//
//  TextFieldViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

extension TextFieldView {
    
    class ViewModel: ObservableObject {
        
        @Published var textField: LamaTextField.ViewModel
        
        internal init(text: String? = nil, placeholder: String? = nil, isEnabled: Bool = true, validationStyle: LamaTextField.ViewModel.ValidationStyle = .none, keyboardType: UIKeyboardType = .default, isSecureText: Bool = false, isFirstResponder: Bool = false) {
            
            self.textField = .init(text: text, placeholder: placeholder, isEnabled: isEnabled, validationStyle: validationStyle, isSecureText: isSecureText, isFirstResponder: isFirstResponder, keyboardType: keyboardType)
        }
        
    }
}


struct TextFieldView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack {
            
            LamaTextField(viewModel: viewModel.textField)
                .frame(height: 40)
            
            Divider()
        }
    }
}

struct TextFieldViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        VStack {
            
            TextFieldView(viewModel: .init(placeholder: "Введите адрес"))
            
            TextFieldView(viewModel: .init(text: "123123", placeholder: "Введите адрес"))
        }
        .previewLayout(.sizeThatFits)
    }
}
