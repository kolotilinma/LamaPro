//
//  SearchBarViewComponent..swift
//  LamaPro
//
//  Created by Михаил on 05.03.2023.
//

import SwiftUI
import Combine

extension SearchBarView {
    
    class ViewModel: ObservableObject {
        
        let action: PassthroughSubject<Action, Never> = .init()
        
        @Published var searchText: String = ""
        @Published var searchValue: SearchValue = .empty
        @Published var isEditing: Bool = false
        
        let textPlaceholder: String
        
        var bindings = Set<AnyCancellable>()
        
        init(textPlaceholder: String = "Поиск") {
            
            self.textPlaceholder = textPlaceholder
            
            bind()
        }
        
        func bind() {
            $searchText
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] value in
                    if searchText.isEmpty {
                        self.searchValue = .empty

                    } else {
                        self.searchValue = .noEmpty
                    }
                }
                .store(in: &bindings)
        }
        
        func clear() {
            
            self.searchText = ""
        }
        
        func stopEditing() {
            UIApplication.shared.endEditing()
            isEditing = false
        }
        
        func search() {
            
            UIApplication.shared.endEditing()
            action.send(SearchBarViewAction.SearchText(text: searchText))
            isEditing = false
            clear()
        }
        
        enum SearchValue {
            case empty
            case noEmpty
            case noResult
        }
    }
}

struct SearchBarView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        HStack {
            
            HStack {
                
                Image.ic24Search
                    .renderingMode(.template)
                    .resizable()
                    .foregroundColor(.lamaGrayDark)
                    .frame(width: 16, height: 16)
                
                TextField(viewModel.textPlaceholder, text: $viewModel.searchText)
                    .disableAutocorrection(true)
                    .accentColor(.lamaBlack)
                    .onTapGesture {
                        viewModel.isEditing = true
                    }
                
                if viewModel.searchValue == .noEmpty {
                    Button {
                        viewModel.stopEditing()
                        viewModel.clear()
                    } label: {
                        Image.ic16Clear
                    }
                }
            }
            .padding(.horizontal, 12)
            .frame(height: 36)
            .background {
                Rectangle()
                    .foregroundColor(Color(hex: "#EEEEEF"))
            }
            .cornerRadius(10)
            
            if viewModel.isEditing {
                
                Button {
                    viewModel.search()
//                    viewModel.stopEditing()
                } label: {
                    Text("Найти")
                        .foregroundColor(.lamaBlack)
                }
            }
        }
        .frame(height: 36)
        .animation(.default, value: viewModel.isEditing)
    }
}

struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        
        VStack(spacing: 20) {
            
            SearchBarView(viewModel: .init())
            
            SearchBarView(viewModel: .init(textPlaceholder: "Поиск"))
        }
        .padding(.horizontal, 12)
    }
}

enum SearchBarViewAction {
    
    struct SearchText: Action {
        
        let text: String
    }
}
