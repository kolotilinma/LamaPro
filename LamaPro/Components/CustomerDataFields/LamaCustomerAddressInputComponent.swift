//
//  LamaCustomerAddressInputComponent.swift
//  LamaPro
//
//  Created by user on 20.03.2023.
//

import SwiftUI

struct LamaCustomerAddressInputComponent: View {
    @State var isTextFieldEnabled = [Int: Bool]()
    @Binding var adresses: [String]
    
    var body: some View {
        
        VStack {
            ForEach(0..<adresses.count, id: \.self) { index in
                HStack {
                    
                    TextField("Введите адрес", text: $adresses[index]) { state in
                        isTextFieldEnabled[index] = state
                    }
                    .lineLimit(1)
                    
                    Image(isTextFieldEnabled[index] ?? false ? "deleteAddressButton" : "pencil")
                        .renderingMode(.original)
                        .foregroundColor(.lamaGray)
                        .onTapGesture {
                            adresses.remove(at: index)
                        }
                }
                Divider()
                    .padding(.bottom, 5)
            }
            HStack {
                Text("Добавить новый адрес...")
                    .foregroundColor(.lamaGrayDark)
                Spacer()
                Image(systemName: "plus")
                    .renderingMode(.template)
                    .foregroundColor(.lamaGrayDark)
                    .onTapGesture {
                        withAnimation(.easeOut) {
                            adresses.append("")
                        }
                    }
            }
        }
    }
}

struct LamaCustomerAddressInputComponent_Previews: PreviewProvider {
    static var previews: some View {
        LamaCustomerAddressInputComponent(adresses: .constant(["bdjkgbksd"]))
    }
}
