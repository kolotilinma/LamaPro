//
//  LamaCustomerGenderComponent.swift
//  LamaPro
//
//  Created by user on 11.03.2023.
//

import SwiftUI

struct LamaCustomerGenderComponent: View {
    @State var title: String
    @Binding var currentData: String
    @State var isSubMenuShowed = false
    @State var manGender: Bool = false
    
    var body: some View {

        VStack {
            HStack {
                Text(title)
                    .font(.interSemiBold(size: 17))
                    .foregroundColor(.lamaBlack)

                Text(currentData != "" ? currentData : "Не указано")
                Spacer()
                Image("pencil")
                    .renderingMode(.template)
                    .foregroundColor(isSubMenuShowed ? .gray : .lamaGray )
                    .onTapGesture {
                        withAnimation(.spring()) {
                            isSubMenuShowed.toggle()
                        }
                    }
            }
            .padding(.bottom, 5)
            
            if isSubMenuShowed {
                HStack {
                    Text("Мужской")
                    Spacer()
                    Image(systemName: manGender ? "checkmark.circle.fill" : "circle")
                        .onTapGesture {
                            currentData = "Мужской"
                            manGender = true
                            isSubMenuShowed.toggle()

                        }
                }
                Divider()
                HStack {
                    Text("Женский")
                    Spacer()
                    Image(systemName: manGender ? "circle" : "checkmark.circle.fill" )
                        .onTapGesture {
                            currentData = "Женский"
                            manGender = false
                            isSubMenuShowed.toggle()

                        }
                }
                Divider()
            } else {
                Divider()
            }
        }
        .onAppear {
            manGender = detectGender(currentData)

        }
    }
    
    
    func detectGender(_ gender: String?) -> Bool {
        switch gender {
        case "Мужской":
            return true
        case "Женский":
            return false
        default:
            return false
        }
    }
}

struct LamaCustomerGenderComponent_Previews: PreviewProvider {
    static var previews: some View {
        LamaCustomerGenderComponent(title: "Пол", currentData: .constant(""))
    }
}
