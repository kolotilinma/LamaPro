//
//  LamaTextFieldComponent.swift
//  LamaPro
//
//  Created by Михаил on 04.01.2023.
//

import SwiftUI
import UIKit

//MARK: - ViewModel

extension LamaTextField {
    
    class ViewModel: ObservableObject {
        
        @Published var text: String?
        @Published var isEnabled: Bool
        let placeholder: String?
        let isSecureText: Bool
        var isFirstResponder: Bool
        
        var dismissKeyboard: () -> Void
        
        private let validationStyle: ValidationStyle
        
        internal init(text: String? = nil, placeholder: String? = nil, isEnabled: Bool = true, validationStyle: ValidationStyle = .none, isSecureText: Bool = false, isFirstResponder: Bool = true) {
            
            self.text = text
            self.placeholder = placeholder
            self.isEnabled = isEnabled
            self.validationStyle = validationStyle
            self.isSecureText = isSecureText
            self.isFirstResponder = isFirstResponder
            self.dismissKeyboard = {}
        }
        
        var isValid: Bool {
            guard let text = text else { return false }
            switch validationStyle {
                
//            case .email:
//                return text.isValidEmail
                
            case .none:
                return false
            }
        }
        
        enum ValidationStyle {
            
//            case email
            case none
        }
    }
}

//MARK: - View

struct LamaTextField: UIViewRepresentable {
    
    @ObservedObject var viewModel: ViewModel
    
    var font: UIFont? = .FontWithName(.interRegular, size: 17)
    var backgroundColor: Color
    var textColor: Color
    var tintColor: Color
    var borderColor: Color
    var keyboardType: UIKeyboardType
    var isFirstResponder: Bool
    
    init(viewModel: ViewModel,
         font: UIFont? = .FontWithName(.interRegular, size: 17),
         backgroundColor: Color = .lamaBackground,
         textColor: Color = .lamaBlack,
         tintColor: Color = .lamaBlack,
         borderColor: Color = .lamaBackground,
         keyboardType: UIKeyboardType = .default) {
        
        self.viewModel = viewModel
        self.font = font
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.tintColor = tintColor
        self.borderColor = borderColor
        self.keyboardType = keyboardType
        self.isFirstResponder = viewModel.isFirstResponder
    }
    
    private let textField = TextFieldWithPadding()

    func makeUIView(context: Context) -> TextFieldWithPadding {
        
        textField.delegate = context.coordinator
        textField.placeholder = viewModel.placeholder
        textField.font = font
        textField.backgroundColor = backgroundColor.uiColor()
        textField.textColor = textColor.uiColor()
        textField.tintColor = tintColor.uiColor()
        textField.layer.cornerRadius = 8
        textField.layer.borderColor = borderColor.uiColor().cgColor
        textField.layer.borderWidth = 1
        textField.keyboardType = keyboardType
        
        if viewModel.isSecureText {
            
            textField.isSecureTextEntry = true
            let clearButton = UIButton(type: .custom)
            clearButton.setImage(UIImage(named: "showhide"), for: .normal)
            clearButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            clearButton.contentMode = .scaleAspectFit
            clearButton.addTarget(context.coordinator,
                                  action: #selector(Coordinator.toggleSecure(sender:) ), for: .touchUpInside)
            textField.rightView = clearButton
            textField.rightViewMode = .whileEditing
            
        }
        
        if isFirstResponder {
            textField.becomeFirstResponder()
        }
        textField.addTarget(context.coordinator,
                            action: #selector(Coordinator.onTextUpdate), for: .editingChanged)
        viewModel.dismissKeyboard = { textField.resignFirstResponder() }
        
        if !viewModel.isSecureText {
            context.coordinator.validateText()
        }
        return textField
    }
    
    func updateUIView(_ uiView: TextFieldWithPadding, context: Context) {
        
        uiView.text = viewModel.text
        uiView.isUserInteractionEnabled = viewModel.isEnabled
    }
    
    //MARK: - Coordinator
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, UITextFieldDelegate {

        var control: LamaTextField

        init(_ control: LamaTextField) {
            self.control = control
        }

        @objc func toggleSecure(sender : AnyObject) {
            self.control.textField.isSecureTextEntry.toggle()
        }
        
        @objc func onTextUpdate(textField: UITextField) {
            self.control.viewModel.text = textField.text
            validateText()
        }

        func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.backgroundColor = Color.lamaBackground.uiColor()
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            textField.backgroundColor = control.backgroundColor.uiColor()
        }
        
        func validateText() {
            if self.control.viewModel.isValid {
                
                let imageView = UIImageView(image: UIImage(named: "checkmark"))
                imageView.frame = .init(x: 0, y: 0, width: 24, height: 24)
                self.control.textField.rightView = imageView
                self.control.textField.rightViewMode = .always
                
            } else {
                
                self.control.textField.rightView = nil
                self.control.textField.rightViewMode = .never
            }
        }
        
    }
    
    //MARK: - TextFieldWithPadding
    
    class TextFieldWithPadding: UITextField {
        
        var textPadding = UIEdgeInsets(
            top: 0,
            left: 16,
            bottom: 0,
            right: 16
        )

        override func textRect(forBounds bounds: CGRect) -> CGRect {
            let rect = super.textRect(forBounds: bounds)
            return rect.inset(by: textPadding)
        }

        override func editingRect(forBounds bounds: CGRect) -> CGRect {
            let rect = super.editingRect(forBounds: bounds)
            return rect.inset(by: textPadding)
        }
        
        override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
            let rect = super.rightViewRect(forBounds: bounds)
            return rect.offsetBy(dx: -16, dy: 0)
        }
    }
}

//MARK: - Preview

struct LamaTextField_Previews: PreviewProvider {
    
    static var previews: some View {
        
        VStack(spacing: 30) {
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Normal"))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "email@mail.com", validationStyle: .none))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "uvalid.Email.Lama.com", validationStyle: .none))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Disabled", isEnabled: false))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Secure", isSecureText: true))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Custom", isEnabled: true),
                          font: UIFont.FontWithName(.interBold, size: 17),
                          backgroundColor: .lamaGray,
                          textColor: .lamaRed,
                          tintColor: .lamaRed,
                          borderColor: .clear,
                          keyboardType: .emailAddress)
            .frame(height: 48)
        }
        .padding(18)
    }
}
