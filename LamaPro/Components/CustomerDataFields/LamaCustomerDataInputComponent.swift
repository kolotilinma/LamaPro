//
//  LamaCustomerDataInputComponent.swift
//  LamaPro
//
//  Created by user on 11.03.2023.
//

import SwiftUI



struct LamaCustomerDataInputComponent: View {
    @State var title: String
    @Binding var currentData: String
    @State var isTextFieldEnabled = false
    @State var inputError = false
    
    var body: some View {

        VStack {
            HStack {
                if !currentData.isEmpty && isTextFieldEnabled {
                    Image(inputError ? "crossmark" : "checkmark")
                }
                Text(title)
                    .font(.interSemiBold(size: 17))
                    .foregroundColor(.lamaBlack)

                TextField("Не указано", text: $currentData) { state in
                    isTextFieldEnabled = state
                }
                
                .onChange(of: currentData, perform: { newValue in
                    performValidation(newValue)
                })
                
                .lineLimit(1)
                Image(inputError && isTextFieldEnabled ? "errortextInformer" : "pencil")
                    .renderingMode(inputError ? .original : .template )
                    .foregroundColor(isTextFieldEnabled ? .gray : .lamaGray )
            }
            Divider()
        }
    }
}

struct LamaCustomerDataInputComponent_Previews: PreviewProvider {
    static var previews: some View {
        LamaCustomerDataInputComponent(title: "Денис", currentData: .constant("Денисов"))
    }
}

extension LamaCustomerDataInputComponent {
    
    func performValidation(_ newValue: String) {
        switch title {
        case "Имя", "Фамилия", "Отчество" :
            if !newValue.isAlphabetic || currentData.count > 20  {
                inputError = true
            } else {
                inputError = false
            }
        case "Телефон":
            if !newValue.isNumeratic || currentData.count > 12 || currentData.count < 11 {
                inputError = true
            } else {
                inputError = false
            }
        case "E-Mail":
            if !newValue.isValidEmail || currentData.count > 30 {
                inputError = true
            } else {
                inputError = false
            }
        default:
            break
        }
    }
}
