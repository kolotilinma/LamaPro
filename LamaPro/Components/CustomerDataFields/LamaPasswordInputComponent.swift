//
//  LamaPasswordInputComponent.swift
//  LamaPro
//
//  Created by user on 20.03.2023.
//

import SwiftUI

struct LamaPasswordInputComponent: View {
    @State var title: String
    @Binding var currentData: String
    @State var isTextFieldEnabled = false
    @State var inputError = false
    
    var body: some View {

        VStack {
            HStack {
                if !currentData.isEmpty && isTextFieldEnabled {
                    Image(inputError ? "crossmark" : "checkmark")
                }
                Text(title)
                    .font(.interSemiBold(size: 17))
                    .foregroundColor(.lamaBlack)

                TextField("Не указано", text: $currentData) { state in
                    isTextFieldEnabled = state
                }
                .textContentType(.password)
                .lineLimit(1)
                Image(inputError && isTextFieldEnabled ? "errortextInformer" : "pencil")
                    .renderingMode(inputError ? .original : .template )
                    .foregroundColor(isTextFieldEnabled ? .gray : .lamaGray )
            }
            Divider()
        }
    }
}

struct LamaPasswordInputComponent_Previews: PreviewProvider {
    static var previews: some View {
        LamaPasswordInputComponent(title: "", currentData: .constant(""))
    }
}
