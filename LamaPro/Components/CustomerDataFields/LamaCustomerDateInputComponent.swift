//
//  LamaCustomerDateInputComponent.swift
//  LamaPro
//
//  Created by user on 20.03.2023.
//

import SwiftUI

struct LamaCustomerDateInputComponent: View {
    @State var title: String
    @Binding var currentData: String
    @State var isTextFieldEnabled = false
    @State var inputError = false
    
    var body: some View {

        VStack {
            HStack {
                if !currentData.isEmpty && isTextFieldEnabled {
                    Image(inputError ? "crossmark" : "checkmark")
                }
                Text(title)
                    .font(.interSemiBold(size: 17))
                    .foregroundColor(.lamaBlack)
                
                DatePickerTextField(placeHolder: "Не выбрано", currentValue: $currentData)
                
//                TextField("Не указано", text: $currentData) { state in
//                    isTextFieldEnabled = state
//                }
                
                .onChange(of: currentData, perform: { newValue in
                    performValidation(newValue)
                })
                
                .lineLimit(1)
                Image(inputError && isTextFieldEnabled ? "errortextInformer" : "pencil")
                    .renderingMode(inputError ? .original : .template )
                    .foregroundColor(isTextFieldEnabled ? .gray : .lamaGray )
            }
            Divider()
        }
    }
}

struct LamaCustomerDateInputComponent_Previews: PreviewProvider {
    static var previews: some View {
        LamaCustomerDateInputComponent(title: "", currentData: .constant("19.02.1992"))
    }
}

extension LamaCustomerDateInputComponent {
        
    func performValidation(_ newValue: String) {
        switch title {
        case "Дата рождения":
            if !newValue.isValidBirth  {
                inputError = true
            } else {
                inputError = false
            }
        default:
            break
        }
    }
}



