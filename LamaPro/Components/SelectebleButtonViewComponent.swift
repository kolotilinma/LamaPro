//
//  SelectebleButtonViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI

extension SelectebleButtonView {
    
    final class ViewModel: ObservableObject {
        
        @Published var title: String
        @Published var selected: Bool
        
        let action: () -> Void
        
        internal init(title: String, selected: Bool, action: @escaping () -> Void) {
            
            self.title = title
            self.selected = selected
            self.action = action
        }
    }
}

struct SelectebleButtonView: View {
    
    @ObservedObject var viewModel: ViewModel

    var body: some View {
        
        Button(action: viewModel.action) {
            
            HStack(alignment: .center) {
                
                Text(viewModel.title)
                    .font(.interRegular(size: 17))
                    .foregroundColor(.lamaBlack)
                
                Spacer()
                
                Image(systemName: viewModel.selected ? "checkmark.circle.fill" : "circle")
                    .foregroundColor(viewModel.selected ? .lamaBlack : .lamaGray)
            }
        }
    }
}

struct SelectebleButtonViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        VStack(spacing: 16) {
            
            SelectebleButtonView(viewModel: .init(title: "Физическое лицо", selected: true, action: {}))
         
            SelectebleButtonView(viewModel: .init(title: "Юридическое лицо", selected: false, action: {}))
        }
        .padding()
        .previewLayout(.sizeThatFits)
    }
}
