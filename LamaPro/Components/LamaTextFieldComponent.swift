//
//  LamaTextFieldComponent.swift
//  LamaPro
//
//  Created by Михаил on 04.01.2023.
//

import SwiftUI
import UIKit

//MARK: - ViewModel

extension LamaTextField {
    
    class ViewModel: ObservableObject {
        
        @Published var text: String?
        @Published var isEnabled: Bool
        let placeholder: String?
        let isSecureText: Bool
        var isFirstResponder: Bool
        var keyboardType: UIKeyboardType
        var dismissKeyboard: () -> Void
        
        let validationStyle: ValidationStyle
        
        internal init(text: String? = nil, placeholder: String? = nil, isEnabled: Bool = true, validationStyle: ValidationStyle = .none, isSecureText: Bool = false, isFirstResponder: Bool = true, keyboardType: UIKeyboardType = .default) {
            
            self.text = text
            self.placeholder = placeholder
            self.isEnabled = isEnabled
            self.validationStyle = validationStyle
            self.isSecureText = isSecureText
            self.isFirstResponder = isFirstResponder
            self.keyboardType = keyboardType
            self.dismissKeyboard = {}
        }
        
        var isValid: Bool {
            
            guard let text = text else { return false }
            switch validationStyle {

            case .email:
                return text.isValidEmail

            case .text:
                return text.isEmpty == false
                
            case .none:
                return text.isEmpty == false
            }
        }
        
        enum ValidationStyle {
            
            case email
            case text
            case none
        }
    }
}

//MARK: - View

struct LamaTextField: UIViewRepresentable {
    
    @ObservedObject var viewModel: ViewModel
    
    var font: UIFont? = .FontWithName(.interRegular, size: 17)
    var backgroundColor: Color
    var textColor: Color
    var tintColor: Color
    var borderColor: Color
    var keyboardType: UIKeyboardType
    var isFirstResponder: Bool
    
    init(viewModel: ViewModel,
         font: UIFont? = .FontWithName(.interRegular, size: 17),
         backgroundColor: Color = .white,
         textColor: Color = .lamaBlack,
         tintColor: Color = .lamaBlack,
         borderColor: Color = .white) {
        
        self.viewModel = viewModel
        self.font = font
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.tintColor = tintColor
        self.borderColor = borderColor
        self.keyboardType = viewModel.keyboardType
        self.isFirstResponder = viewModel.isFirstResponder
    }
    
    private let textField = TextFieldWithPadding()

    func makeUIView(context: Context) -> TextFieldWithPadding {
        
        textField.delegate = context.coordinator
        textField.placeholder = viewModel.placeholder
        textField.font = font
        textField.backgroundColor = backgroundColor.uiColor()
        textField.textColor = textColor.uiColor()
        textField.tintColor = tintColor.uiColor()
        textField.layer.cornerRadius = 8
        textField.layer.borderColor = borderColor.uiColor().cgColor
        textField.layer.borderWidth = 1
        textField.keyboardType = keyboardType
        
        if viewModel.isSecureText {
            
            context.coordinator.createSecureButton(isSecure: viewModel.isSecureText)
        }
        
        if isFirstResponder {
            textField.becomeFirstResponder()
        }
        textField.addTarget(context.coordinator,
                            action: #selector(Coordinator.onTextUpdate), for: .editingChanged)
        viewModel.dismissKeyboard = { textField.resignFirstResponder() }
        
        if !viewModel.isSecureText {
            
            context.coordinator.validateText()
        }
        return textField
    }
    
    func updateUIView(_ uiView: TextFieldWithPadding, context: Context) {
        
        uiView.text = viewModel.text
        uiView.isUserInteractionEnabled = viewModel.isEnabled
    }
    
    //MARK: - Coordinator
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, UITextFieldDelegate {

        var control: LamaTextField

        init(_ control: LamaTextField) {
            self.control = control
        }

        @objc func toggleSecure(sender : AnyObject) {
            
            self.control.textField.isSecureTextEntry.toggle()
            self.createSecureButton(isSecure: control.textField.isSecureTextEntry)
        }
        
        @objc func onTextUpdate(textField: UITextField) {
            self.control.viewModel.text = textField.text
            validateText()
        }

        func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.backgroundColor = Color.lamaBackground.uiColor()
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            textField.backgroundColor = control.backgroundColor.uiColor()
        }
        
        func validateText() {
            
            if  self.control.viewModel.validationStyle != .none {
                
                if self.control.viewModel.isValid {
                    
                    let imageView = UIImageView(image: UIImage(named: "checkmark"))
                    imageView.frame = .init(x: 0, y: 0, width: 24, height: 24)
                    self.control.textField.leftView = imageView
                    self.control.textField.leftViewMode = .always
                    
                } else {
                    let imageView = UIImageView(image: UIImage(named: "crossmark"))
                    imageView.frame = .init(x: 0, y: 0, width: 24, height: 24)
                    self.control.textField.leftView = imageView
                    self.control.textField.leftViewMode = .always
                    //                self.control.textField.leftView = nil
                    //                self.control.textField.leftViewMode = .never
                }
            }
        }
        
        
        func createSecureButton(isSecure: Bool) {
            self.control.textField.isSecureTextEntry = isSecure
            let clearButton = UIButton(type: .custom)
            clearButton.setImage(isSecure ? UIImage(named: "showhide") : UIImage(named: "showhidesecure"), for: .normal)
            clearButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            clearButton.contentMode = .scaleAspectFit
            clearButton.addTarget(self,
                                  action: #selector(toggleSecure(sender:) ), for: .touchUpInside)
            self.control.textField.rightView = clearButton
            self.control.textField.rightViewMode = .always
        }
        
    }
    
    //MARK: - TextFieldWithPadding
    
    class TextFieldWithPadding: UITextField {
        
        var textPadding = UIEdgeInsets(
            top: 0,
            left: 10,
            bottom: 0,
            right: 0
        )

        override func textRect(forBounds bounds: CGRect) -> CGRect {
            let rect = super.textRect(forBounds: bounds)
            return rect.inset(by: textPadding)
        }

        override func editingRect(forBounds bounds: CGRect) -> CGRect {
            let rect = super.editingRect(forBounds: bounds)
            return rect.inset(by: textPadding)
        }
        
        override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
            let rect = super.rightViewRect(forBounds: bounds)
            return rect.offsetBy(dx: -16, dy: 0)
        }
    }
}

//MARK: - Preview

struct LamaTextField_Previews: PreviewProvider {
    
    static var previews: some View {
        
        VStack(spacing: 30) {
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Normal", validationStyle: .text))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "email@mail.com", validationStyle: .email))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "uvalid.Email.Lama.com", validationStyle: .email))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Disabled", isEnabled: false))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Secure", isSecureText: true))
                .frame(height: 48)
            
            LamaTextField(viewModel: LamaTextField.ViewModel(text: "Custom", isEnabled: true, keyboardType: .emailAddress),
                          font: UIFont.FontWithName(.interBold, size: 17),
                          backgroundColor: .lamaGray,
                          textColor: .lamaRed,
                          tintColor: .lamaRed,
                          borderColor: .clear)
            .frame(height: 48)
        }
        .padding(20)
    }
}
