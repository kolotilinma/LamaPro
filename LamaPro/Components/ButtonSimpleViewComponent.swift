//
//  ButtonSimpleViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI

//MARK: - ViewModel

extension ButtonSimpleView {
    
    class ViewModel: ObservableObject {
        
        let title: String
        let icon: Image?
        let backgroundColor: Color
        let disabled: Bool
        let height: CGFloat
        let withBackAction: Bool
        
        let action: () -> Void

        internal init(title: String, icon: Image? = nil, backgroundColor: Color = .lamaGreenDark, disabled: Bool = false, height: CGFloat = 30, withBackAction: Bool = false, action: @escaping () -> Void) {

            self.title = title
            self.icon = icon
            self.backgroundColor = backgroundColor
            self.disabled = disabled
            self.height = height
            self.withBackAction = withBackAction
            self.action = action
        }
    }
}

//MARK: - ButtonSimpleView

struct ButtonSimpleView: View {
    
    @ObservedObject var viewModel: ButtonSimpleView.ViewModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var body: some View {
        
        Button {
            viewModel.action()
            if viewModel.withBackAction {
                mode.wrappedValue.dismiss()
            }
            
        } label: {
          
            HStack(alignment: .center, spacing: 8) {
                
                if let icon = viewModel.icon {
                    icon
                        .resizable()
                        .scaledToFit()
                        .frame(width: 15, height: 15)
                }
                
                Text(viewModel.title)
                    .font(.interMedium(size: 15))
            }
            .frame(maxWidth: .infinity)
            .foregroundColor(.white)
            
        }
        .frame(height: viewModel.height)
        .background(content: {
            viewModel.disabled ? Color.lamaGray : viewModel.backgroundColor
        })
        .disabled(viewModel.disabled)
        .cornerRadius(30)
    }
}

//MARK: - Preview

struct ButtonSimpleView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        VStack(spacing: 20) {
            
            ButtonSimpleView(viewModel: .init(title: "Заказано", icon: Image(systemName: "clock"), action: {} ))
            
            ButtonSimpleView(viewModel: .init(title: "Показать", backgroundColor: .lamaGreen, height: 50, action: {} ))
            
            ButtonSimpleView(viewModel: .init(title: "В корзине", icon: Image(systemName: "checkmark"), action: {}))
            
            ButtonSimpleView(viewModel: .init(title: "Под заказ", height: 30, action: {}))
                .padding(.horizontal, 70)
        }
        .padding()
        .previewLayout(.sizeThatFits)
    }
}

