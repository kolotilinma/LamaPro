//
//  NavigationBarViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI

extension NavigationBarView {
    
    class ViewModel: ObservableObject {
        
        @Published var title: Title
        @Published var leftButtons: [BaseButtonViewModel]
        @Published var rightButtons: [BaseButtonViewModel]
        @Published var textColor: Color
        @Published var foreground: Color
        @Published var backgroundColor: Color
        @Published var stase: State
        
        
        internal init(title: Title,
                      leftButtons: [BaseButtonViewModel] = [],
                      rightButtons: [ButtonViewModel] = [],
                      textColor: Color = .lamaBlack,
                      foreground: Color = .lamaBlack,
                      backgroundColor: Color = .lamaBackground,
                      stase: State = .normal) {
            
            self.title = title
            self.leftButtons = leftButtons
            self.rightButtons = rightButtons
            self.textColor = textColor
            self.foreground = foreground
            self.backgroundColor = backgroundColor
            self.stase = stase
        }
        
        enum State {
            
            case normal
            case hidden
            case withLogo
//            case search(searchViewModel) TODO: Написать компонент для поиска
        }
        
        enum Title {
            
            case text(String)
            case image(Image)
            case picker(TitlePicker)
            case empty
        }
        
        class BaseButtonViewModel: ObservableObject ,Identifiable {
            
            let id: UUID = UUID()
        }
        
        class ButtonViewModel: BaseButtonViewModel {
            
            let icon: Image
            let action: () -> Void
            
            init(icon: Image, action: @escaping () -> Void) {
                
                self.icon = icon
                self.action = action
                super.init()
            }
        }
        
        class TextButtonViewModel: BaseButtonViewModel {
            
            let icon: Image
            let text: String
            let action: () -> Void
            
            init(icon: Image, text: String, action: @escaping () -> Void) {
                
                self.icon = icon
                self.text = text
                self.action = action
                super.init()
            }
        }
        
        class BackButtonViewModel: BaseButtonViewModel {
            
            let icon: Image
            let action: () -> Void
            
            init(icon: Image, action: @escaping () -> Void) {
                
                self.icon = icon
                self.action = action
                super.init()
            }
        }
        
        class BageButtonViewModel: BaseButtonViewModel {
            
            let icon: Image
            @Published var bageCount: String?
            let action: () -> Void
            
            init(icon: Image, bageCount: String?, action: @escaping () -> Void) {
                
                self.icon = icon
                self.bageCount = bageCount
                self.action = action
                super.init()
            }
        }
        
        class TitlePicker: ObservableObject {
            
            @Published var selected = 0
            var items: [String]
            
            init(selected: Int, items: [String] = ["Вход", "Регистрация"]) {
                
                self.selected = selected
                self.items = items
            }
        }
    }
}

struct NavigationBarView: View {

    @ObservedObject var viewModel: ViewModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var leftPlaceholdersCount: Int {
        
        return max(viewModel.rightButtons.count - viewModel.leftButtons.count, 0)
    }
    
    var rightPlaceholdersCount: Int {
        
        return max(viewModel.leftButtons.count - viewModel.rightButtons.count, 0)
    }
    
    var body: some View {
        
        switch viewModel.stase {
            
        case .normal:
            
            HStack(spacing: 0) {
                
                HStack(alignment: .center, spacing: 18) {
                    
                    ForEach(viewModel.leftButtons) { button in
                        
                        switch button {
                        case let backButtonViewModel as NavigationBarView.ViewModel.BackButtonViewModel:
                            Button {
                                
                                mode.wrappedValue.dismiss()
                                backButtonViewModel.action()
                                
                            } label: {
                                
                                backButtonViewModel.icon
                                    .resizable()
                                    .renderingMode(.template)
                                    .scaledToFit()
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(viewModel.foreground)
                            }
                            
                        case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                            Button(action: buttonViewModel.action){
                                
                                buttonViewModel.icon
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(viewModel.foreground)
                            }
                        default:
                            EmptyView()
                        }
                    }
                    
                    ForEach(0..<leftPlaceholdersCount, id: \.self) { _ in
                        Spacer().frame(width: 20, height: 20)
                    }
                }
                
                Spacer()
                
                switch viewModel.title {
                    
                case .picker(let picker):
                    PickerView(viewModel: picker)
                        .padding(.horizontal, 40)
                    
                case .text(let title):
                    Text(title)
                        .font(.interMedium(size: 17))
                        .foregroundColor(viewModel.textColor)
                        .lineLimit(1)
                    
                case .image(let image):
                    image
                    
                case .empty:
                    EmptyView()
                }
                
                Spacer()
                
                HStack(alignment: .center, spacing: 18) {
                    
                    ForEach(0..<rightPlaceholdersCount, id: \.self) { _ in
                        Spacer().frame(width: 20, height: 20)
                    }
                    
                    ForEach(viewModel.rightButtons) { button in
                        
                        switch button {
                        case let bageButtonViewModel as NavigationBarView.ViewModel.BageButtonViewModel:
                            Button(action: bageButtonViewModel.action) {
                                
                                ZStack(alignment: .topTrailing) {
                                    
                                    bageButtonViewModel.icon
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(viewModel.foreground)
                                    
                                    if let count = bageButtonViewModel.bageCount {
                                        
                                        Text(count)
                                            .foregroundColor(.black)
                                            .frame(width: 20, height: 20)
                                            .font(.interMedium(size: 17))
                                            .background(Circle()
                                                .stroke(viewModel.backgroundColor, lineWidth: 2)
                                                .background(Circle().fill(Color(hex: "FF9900")))
                                            )
                                            .offset(x: 9, y: -4)
                                    }
                                }
                            }
                            
                        case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                            Button(action: buttonViewModel.action){
                                
                                buttonViewModel.icon
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(viewModel.foreground)
                            }
                        default:
                            EmptyView()
                        }
                    }
                }
            }
            .frame(height: 48)
            .padding(.horizontal, 14)
            .background(viewModel.backgroundColor.edgesIgnoringSafeArea(.top))
            
        case .hidden:
            
            HStack(alignment: .center, spacing: 4) {
                
                ForEach(viewModel.leftButtons) { button in
                    
                    switch button {
                    case let backButtonViewModel as NavigationBarView.ViewModel.BackButtonViewModel:
                        
                        Button {
                            
                            mode.wrappedValue.dismiss()
                            backButtonViewModel.action()
                            
                        } label: {
                            
                            ZStack {
                                
                                Circle()
                                    .frame(width: 40, height: 40)
                                    .foregroundColor(.white)
                                
                                backButtonViewModel.icon
                                    .resizable()
                                    .renderingMode(.template)
                                    .scaledToFit()
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(viewModel.foreground)
                            }
                        }
                        
                    case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                        
                        Button(action: buttonViewModel.action){
                            
                            ZStack {
                                
                                Circle()
                                    .frame(width: 36, height: 36)
                                    .foregroundColor(.white)
                                
                                buttonViewModel.icon
                                    .resizable()
                                    .renderingMode(.template)
                                    .scaledToFit()
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(viewModel.foreground)
                            }
                        }
                    default:
                        EmptyView()
                    }
                }
            }
            .padding(.leading, 8)
            .padding(.top, 12)
            
        case .withLogo:
            HStack(spacing: 0) {
                
                Image("LogoSmall")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 36)
                    .offset(y: 0)
                
                Spacer()
                
                HStack(alignment: .center, spacing: 18) {
                    
                    ForEach(viewModel.rightButtons) { button in
                        
                        switch button {
                        case let bageButtonViewModel as NavigationBarView.ViewModel.BageButtonViewModel:
                            Button(action: bageButtonViewModel.action) {
                                
                                ZStack(alignment: .topTrailing) {
                                    
                                    bageButtonViewModel.icon
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(viewModel.foreground)
                                    
                                    if let count = bageButtonViewModel.bageCount {
                                        
                                        Text(count)
                                            .foregroundColor(.black)
                                            .frame(width: 20, height: 20)
                                            .font(.interMedium(size: 17))
                                            .background(Circle()
                                                .stroke(viewModel.backgroundColor, lineWidth: 2)
                                                .background(Circle().fill(Color(hex: "FF9900")))
                                            )
                                            .offset(x: 9, y: -4)
                                    }
                                }
                            }
                            
                        case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                            Button(action: buttonViewModel.action){
                                
                                buttonViewModel.icon
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 20, height: 20)
                                    .foregroundColor(viewModel.foreground)
                            }
                            
                        case let textButton as NavigationBarView.ViewModel.TextButtonViewModel:
                            Button(action: textButton.action) {
                                
                                HStack(spacing: 8) {
                                    
                                    Text(textButton.text)
                                        .font(.interSemiBold(size: 13))
                                    
                                    textButton.icon
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 24, height: 24)
                                }
                                .foregroundColor(viewModel.foreground)
                            }
                            
                        default:
                            EmptyView()
                        }
                    }
                }
            }
            .frame(height: 48)
            .padding(.horizontal, 14)
            .background(viewModel.backgroundColor.edgesIgnoringSafeArea(.top))
        }
    }
    
    struct PickerView: View {
        
        @ObservedObject var viewModel: NavigationBarView.ViewModel.TitlePicker
        
        var body: some View {
            
            Picker("", selection: $viewModel.selected) {
                
                ForEach(0 ..< viewModel.items.count, id: \.self) { index in
                    
                    Text(viewModel.items[index])
                        .tag(index)
                }
            }
            .pickerStyle(SegmentedPickerStyle())
        }
    }

}

struct NavigationBarViewModifier: ViewModifier {
    
    let viewModel: NavigationBarView.ViewModel
    
    func body(content: Content) -> some View {
        
        switch viewModel.stase {
            
        case .normal, .withLogo:
            
            VStack(spacing: 0) {
                
                NavigationBarView(viewModel: viewModel)
                content
            }
            .navigationBarHidden(true)
            
        case .hidden:
            
            ZStack(alignment: .topLeading) {
                
                content
                NavigationBarView(viewModel: viewModel)

            }
            .navigationBarHidden(true)
        }
    }
}

extension View {
    
    func navigationBar(with viewModel: NavigationBarView.ViewModel) -> some View {
        
        self.modifier(NavigationBarViewModifier(viewModel: viewModel))
    }
}

extension NavigationBarView.ViewModel {
    
    static let sample0 = NavigationBarView.ViewModel(
        title: .text("Личные данные0"),
        leftButtons: [
            NavigationBarView.ViewModel.BackButtonViewModel(icon: .icChevronLeft, action: { })
        ]
    )
    
    static let sample1 = NavigationBarView.ViewModel(
        title: .picker(.init(selected: 0)),
        leftButtons: [
            NavigationBarView.ViewModel.BackButtonViewModel(icon: .icChevronLeft, action: { })
        ],
        rightButtons: [
//            .init(icon: .icChevronLeft, action: { }),
            .init(icon: .icXmark, action: { })
        ],
        textColor: .black,
        foreground: .lamaGreenDark,
        backgroundColor: .white,
        stase: .normal
    )
    
    static let sample2 = NavigationBarView.ViewModel(
        title: .text("2"),
        leftButtons: [
            NavigationBarView.ViewModel.BackButtonViewModel(icon: .icChevronLeft, action: { })
        ],
        stase: .hidden
    )
    static let sample3 = NavigationBarView.ViewModel(
        title: .empty,
//        rightButtons: [NavigationBarView.ViewModel.TextButtonViewModel(icon: Image("tabProfile"), text: "Профиль", action: {  })],
        stase: .withLogo
    )
    
}

struct NavigationBarView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        Group {
            
            ScrollView {
                VStack(spacing: 10) {
                    Color.green
                        .frame(height: 180)
                    Color.orange
                        .frame(height: 180)
                }
            }
            .navigationBar(with: .sample0)
            
            ScrollView {
                VStack(spacing: 10) {
                    Color.orange
                        .frame(height: 180)
                    Color.purple
                        .frame(height: 180)
                }
            }
            .navigationBar(with: .sample1)
            
            ScrollView {
                VStack(spacing: 10) {
                    Color.green
                        .frame(height: 180)
                    Color.cyan
                        .frame(height: 180)
                }
            }
            .navigationBar(with: .sample2)
            
            ScrollView {
                VStack(spacing: 10) {
                    Color.green
                        .frame(height: 180)
                    Color.cyan
                        .frame(height: 180)
                }
            }
            .navigationBar(with: .sample3)

        }
    }
}
