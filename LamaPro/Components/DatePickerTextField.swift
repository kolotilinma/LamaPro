//
//  DatePickerTextField.swift
//  LamaPro
//
//  Created by user on 20.03.2023.
//

import Foundation
import SwiftUI

struct DatePickerTextField: UIViewRepresentable {

    
    
    
    private let textField = UITextField()
    private let datePicker = UIDatePicker()
    private let helper = Helper()
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyy"
        return formatter
    }()
    
    public var placeHolder: String
    @Binding var currentValue: String
    
    func makeUIView(context: Context) -> UITextField {
        self.datePicker.datePickerMode = .date
        self.datePicker.preferredDatePickerStyle = .wheels
        self.datePicker.addTarget(self.helper,
                                  action: #selector(self.helper.dataValueChanged),
                                  for: .valueChanged)
        self.textField.placeholder = self.placeHolder
        self.textField.inputView = self.datePicker
        
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Выбрать", style: .plain, target: self.helper, action: #selector(self.helper.doneButtonAction))
        toolBar.setItems([flexibleSpace, doneButton], animated: true)
        self.textField.inputAccessoryView = toolBar
        self.helper.dateChanged = {
            
            self.currentValue = datePicker.date.formatted(date: .numeric, time: .omitted)
        }
        
        
        self.helper.doneButtonTapped = {
//            if self.currentValue == nil {
                self.currentValue = self.datePicker.date.formatted(date: .numeric, time: .standard)
//            }
            self.textField.resignFirstResponder()
        }
        return self.textField
    }
    
    func updateUIView(_ uiView: UITextField, context: Context) {
            uiView.text = currentValue
    }
    
    
    func makeCoordinator() -> Coordinator {
        Coordinator()
    }
    
    
    class Helper {
        public var dateChanged: (()-> Void)?
        public var doneButtonTapped: (()-> Void)?
        
        @objc func dataValueChanged() {
            self.dateChanged?()
        }
        
        @objc func doneButtonAction() {
            self.doneButtonTapped?()
        }
    }
    
    class Coordinator {
        
    }
}

