//
//  ToCartButtonViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 20.03.2023.
//

import SwiftUI
import Combine

extension ToCartButtonView {
    
    class ViewModel: ObservableObject {
        
        @Published var itemsCount: Int
        @Published var itemsString: String = ""
        
        private let maxCount: Int
        private var bindings = Set<AnyCancellable>()
        
        internal init(itemsCount: Int, maxCount: Int) {
            
            self.itemsCount = itemsCount
            self.itemsString = String(itemsCount)
            self.maxCount = maxCount
            bind()
        }
        
        func removeItem() {
            
            if itemsCount > 0 {
                itemsCount -= 1
                itemsString = String(itemsCount)
            }
            UIApplication.shared.endEditing()
        }
        
        func addItem() {
            if itemsCount < maxCount {
                itemsCount += 1
                itemsString = String(itemsCount)
            }
            UIApplication.shared.endEditing()
        }
        
        func bind() {

            $itemsCount
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] itemsCount in
                    
                    self.itemsString = String(itemsCount)
                }
                .store(in: &bindings)
        }
        
        func updateCount(itemsString: String) {
            
            if let count = Int(itemsString) {
                
                if count < maxCount {
                    
                    itemsCount = count
                    
                } else {
                    self.itemsString = String(maxCount)
                    itemsCount = maxCount
                }
                
            } else {
                
                itemsCount = 0
            }
        }
    }
}


struct ToCartButtonView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    private let height: CGFloat = 30
    
    var body: some View {
        
        HStack {
            
            Button(action: viewModel.removeItem) {
                
                Image(systemName: "minus")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 16, height: 16)
                    .foregroundColor(.lamaWhite)
            }
            .background(Circle()
                .frame(width: height, height: height)
                .foregroundColor(.lamaGreen)
            )
            .frame(width: height, height: height)
            .cornerRadius(height/2)
            
            TextField("", text: $viewModel.itemsString, onEditingChanged: { editing in
                if editing {
//                    self.viewModel.itemsString = ""
                } else {
                    viewModel.updateCount(itemsString: viewModel.itemsString)
                    print(editing, "text: \(viewModel.itemsString)")
                }
            })
            .font(.interRegular(size: 12))
            .foregroundColor(.lamaBlack)
            .keyboardType(.numberPad)
            .multilineTextAlignment(.center)
//            .toolbar {
//                ToolbarItemGroup(placement: .keyboard) {
//                    Spacer()
//
//                    Button("Готово") {
//                        viewModel.updateCount(itemsString: viewModel.itemsString)
//                        UIApplication.shared.endEditing()
//                    }
//                }
//            }
            
            Button(action: viewModel.addItem) {
                
                Image(systemName: "plus")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 16, height: 16)
                    .foregroundColor(.lamaWhite)
                
            }
            .background(Circle()
                .frame(width: height, height: height)
                .foregroundColor(.lamaGreen)
            )
            .frame(width: height, height: height)
            .cornerRadius(height/2)
            
        }
        .background(Color.lamaGrayLight.cornerRadius(height/2))
    }
}

struct ToCartButtonViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ToCartButtonView(viewModel: .init(itemsCount: 4, maxCount: 10))
            .padding(.horizontal, 80)
    }
}
