//
//  UIApplication+Extension.swift
//  LamaPro
//
//  Created by Михаил on 13.02.2023.
//

import UIKit

extension UIApplication {
    
    public func endEditing() {
        
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
