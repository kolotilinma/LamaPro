//
//  ResignKeyboardOnDragGesture.swift
//  LamaPro
//
//  Created by Михаил on 11.04.2023.
//

import SwiftUI

struct ResignKeyboardOnDragGesture: ViewModifier {
    
    var gesture = DragGesture().onChanged { _ in
        
        UIApplication.shared.endEditing()
    }
    
    func body(content: Content) -> some View {
        
        content.gesture(gesture)
    }
}

extension View {
    
    func resignKeyboardOnDragGesture() -> some View {
        
        return modifier(ResignKeyboardOnDragGesture())
    }
}
