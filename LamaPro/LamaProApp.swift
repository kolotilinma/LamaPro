//
//  LamaProApp.swift
//  LamaPro
//
//  Created by Михаил on 21.12.2022.
//

import SwiftUI

@main
struct LamaProApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var model: Model {
        
        let enviroment = ServerAgent.Environment.test
        
        let serverAgent = ServerAgent(enviroment: enviroment)
        
        let keychainAgent = ValetKeychainAgent(valetName: "com.kolotilinma.LamaPro.valet")
        
        let localContext = LocalAgent.Context(cacheFolderName: "cache", encoder: JSONEncoder(), decoder: JSONDecoder(), fileManager: FileManager.default)
        let localAgent = LocalAgent(context: localContext)
        
        // settings agent
        let settingsAgent = UserDefaultsSettingsAgent()
        
        let model = Model(serverAgent: serverAgent, keychainAgent: keychainAgent, localAgent: localAgent, settingsAgent: settingsAgent)
        
        return model
    }
    
    var mainTabBarViewModel: MainTabBarViewModel {
        
        return MainTabBarViewModel(model: model)
    }
        
    var body: some Scene {
        WindowGroup {
            MainTabBarView(viewModel: mainTabBarViewModel)
        }
    }
}
