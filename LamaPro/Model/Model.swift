//
//  Model.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation
import Combine

class Model {
    
    // interface
    let action: PassthroughSubject<Action, Never> = .init()
    
    var spinner: Cover?
    
    //MARK: - User
    let auth: CurrentValueSubject<SessionState, Never>
    let user: CurrentValueSubject<UserData?, Never> = .init(nil)
    
    //MARK: - Catalog
    let catalogSections: CurrentValueSubject<[CatSection], Never> = .init([])
    let catalogElements: CurrentValueSubject<[CatElement], Never> = .init([])
    let catalogStatus: CurrentValueSubject<CatalogStatus, Never> = .init(.loading)
    let catalogOffers: CurrentValueSubject<[CatOffer], Never> = .init([])
    
    //MARK: - Favorites
    let favoritesElements: CurrentValueSubject<FavoritesElements, Never> = .init(.init(elementsId: []))
    
    //MARK: - Cart
    let cartElements: CurrentValueSubject<CartData, Never> = .init(.init(items: []))
    
    
    //MARK: - Orders
    let myOrders: CurrentValueSubject<[OrderData], Never> = .init([])
    
    
    // private
    private var bindings: Set<AnyCancellable>
    private let queue = DispatchQueue(label: "com.kolotilinma.LamaPro.model", qos: .userInitiated, attributes: .concurrent)
    
    // services
    //MARK: - Services
    internal let serverAgent: ServerAgentProtocol
    internal let keychainAgent: KeychainAgentProtocol
    internal let localAgent: LocalAgentProtocol
    internal let settingsAgent: SettingsAgentProtocol
    
    //MARK: Init
    init(serverAgent: ServerAgentProtocol, keychainAgent: KeychainAgentProtocol, localAgent: LocalAgentProtocol, settingsAgent: SettingsAgentProtocol) {
        
        self.bindings = []
        
        // location agent
        self.serverAgent = serverAgent
        self.keychainAgent = keychainAgent
        self.localAgent = localAgent
        self.settingsAgent = settingsAgent
        self.auth = .init(.inactive)
        
        handleAppFirstLaunch()
        bind()
        loadCacheData()
        
        if let launchedBefore: Bool = try? self.settingsAgent.load(type: .general(.launchedBefore)), launchedBefore == true {
            
            tryLoginWithStored()
            
        }
        Task {
            await fetchCatalogData()
        }
    }
    
    internal var token: String? {
        
        guard case .active(let credentials) = auth.value else {
            return nil
        }
        
        return credentials.token
    }
    
    internal var host: String {
        
        return serverAgent.baseURL
    }
    
    var settingsLaunchedBefore: Bool {
        
        if let launchedBefore: Bool = try? settingsAgent.load(type: .general(.launchedBefore)) {
            
            return launchedBefore
            
        } else {
            
            return false
        }
    }
    
    private func bind() {
        
        action
            .receive(on: queue)
            .sink { [unowned self] action in
                
                switch action {
                    
                    //MARK: - Favorites Actions
                case let payload as ModelAction.Favorites.Update:
                    LoggerAgent.shared.log(category: .model, message: "received ModelAction.Favorites.Update")
                    handleFavoritesUpdateRequest(payload)
                    
                    //MARK: - ModelAction Cart
                case let payload as ModelAction.Cart.Update:
                    LoggerAgent.shared.log(category: .model, message: "received ModelAction.Cart.Update")
                    handleCartUpdateRequest(payload)
                    
                    //MARK: - ModelAction Cover
                case let payload as ModelAction.CoverAction.Spinner.Show:
                    LoggerAgent.shared.log(category: .model, message: "received ModelAction.CoverAction.Spinner.Show")
                    handlePresentSpinnerRequest(payload)
                    
                case let payload as ModelAction.CoverAction.Spinner.Hide:
                    LoggerAgent.shared.log(category: .model, message: "received ModelAction.CoverAction.Spinner.Hide")
                    handleHideSpinnerRequest(payload)
                    
                case let payload as ModelAction.UpdateAction.UserData:
                    LoggerAgent.shared.log(category: .model, message: "received ModelAction.UpdateAction.UserData")

                    updateCustomerData(name: payload.user.name,
                                       secondName: payload.user.secondName,
                                       lastName: payload.user.lastName,
                                       phone: payload.user.phone,
                                       email: payload.user.email)
                    
                
                default:
                    break
                }
            }
            .store(in: &bindings)
        
        favoritesElements
            .dropFirst()
            .receive(on: queue)
            .sink { [unowned self] favorites in
                
                try? localAgent.store(favorites)
            }
            .store(in: &bindings)
        
        cartElements
            .dropFirst()
            .receive(on: queue)
            .sink { [unowned self] cart in
                
                try? localAgent.store(cart)
            }
            .store(in: &bindings)
    }
    
    func updateCustomerData(name: String?, secondName: String?, lastName: String?, phone: String?, email: String?) {
        Task {
            if try await handleAuthChangeUserData(name:name, secondName:secondName, lastName:lastName, phone:phone, email:email) {
                try await  handleAuthGetProfile()
            } else {
                // сброс ошибки
            }
        }
    }
    
    
    func tryLoginWithStored() {
        
        Task {
            do {
                let login: String = try self.keychainAgent.load(type: .login)
                let pass: String = try self.keychainAgent.load(type: .password)
                action.send(ModelAction.CoverAction.Spinner.Show())
                
                try await handleAuthLogin(login: login, pass: pass)
                action.send(ModelAction.CoverAction.Spinner.Hide())
            } catch {
                action.send(ModelAction.CoverAction.Spinner.Hide())
            }
        }
    }
    
    func loadCacheData() {

        if let catalog = localAgent.load(type: FullCatalog.self) {
            
            self.catalogElements.value = catalog.catalog
            self.catalogSections.value = catalog.section
            self.catalogOffers.value = catalog.offers
        }
        
        if let element = localAgent.load(type: FavoritesElements.self) {
            
            self.favoritesElements.value = element
        }
        
        if let cart = localAgent.load(type: CartData.self) {
            
            self.cartElements.value = cart
        }
    }
    
}

extension Model {
    
    func handleAppFirstLaunch() {
        
        if settingsLaunchedBefore == false {
            
            // app just installed, remove previos keychan data that may remain from previous install
            
            clearKeychainData()
            
            do {
                
                try settingsAgent.store(true, type: .general(.launchedBefore))
                
            } catch {
                
                //TODO: set logger
            }
            handleAuthLogout()
        }
    }
    
    func clearKeychainData() {
        
        do {
            
            try keychainAgent.clear(type: .login)
            try keychainAgent.clear(type: .password)
            
        } catch {
            
            //TODO: set logger
        }
    }
}


extension Model {
    
    static var emptyMock: Model {
        
        let enviroment = ServerAgent.Environment.test

        let serverAgent = ServerAgent(enviroment: enviroment)
        
        let keychainAgent = ValetKeychainAgent(valetName: "com.kolotilinma.LamaPro.valet")
        
        let localContext = LocalAgent.Context(cacheFolderName: "cache", encoder: JSONEncoder(), decoder: JSONDecoder(), fileManager: FileManager.default)
        let localAgent = LocalAgent(context: localContext)
        
        
        // settings agent
        let settingsAgent = UserDefaultsSettingsAgent()
        
        let model = Model(serverAgent: serverAgent, keychainAgent: keychainAgent, localAgent: localAgent, settingsAgent: settingsAgent)
        
        return model
    }
}
