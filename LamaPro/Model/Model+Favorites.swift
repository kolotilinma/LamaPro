//
//  Model+Favorites.swift
//  LamaPro
//
//  Created by Михаил on 20.03.2023.
//

import Foundation

extension ModelAction {
    
    enum Favorites {
        
        struct Update: Action {
            
            let id: String
        }
    }
}

extension Model {
    
    func handleFavoritesUpdateRequest(_ payload: ModelAction.Favorites.Update) {
        
        LoggerAgent.shared.log(category: .model, message: "handleFavoritesUpdateRequest")
        
        if let index = favoritesElements.value.elementsId.firstIndex(where: { $0 == payload.id }) {
            
            favoritesElements.value.elementsId.remove(at: index)
        } else {
            
            favoritesElements.value.elementsId.append(payload.id)
        }
    }
}
