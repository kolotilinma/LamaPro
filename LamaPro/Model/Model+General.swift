//
//  Model+General.swift
//  LamaPro
//
//  Created by Михаил on 08.04.2023.
//

import Foundation

extension Model {
    
    func convertErrorToString(_ error: Error) -> String {
        
        switch error {
            
        case let serverError as ServerAgentError:
            switch serverError {
            
            case .curruptedData(let error):
                return error.localizedDescription
                
            case .serverStatus(errorMessage: let errorMessage):
                return errorMessage.errors.first?.message ?? ""
                
            default :
                return "Что то пошло не так"
            }
            
        default:
            return error.localizedDescription
        }
        
    }
    
}
