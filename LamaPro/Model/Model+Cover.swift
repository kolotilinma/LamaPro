//
//  Model+Cover.swift
//  LamaPro
//
//  Created by Михаил on 23.01.2023.
//

import UIKit
import SwiftUI

extension ModelAction {
    
    enum CoverAction {
        
        public enum Spinner {
            
            public struct Show: Action {
               
                let viewModel: SpinnerView.ViewModel
                
                public init(viewModel: SpinnerView.ViewModel = .init()) {
                    
                    self.viewModel = viewModel
                }
            }
            
            public struct Hide: Action {
                
                public init() { }
            }
        }
    }
}

extension Model {
    
    func handlePresentSpinnerRequest(_ payload: ModelAction.CoverAction.Spinner.Show) {
        
        runOnMainQueue {
            guard let windowScenes = UIApplication.shared.connectedScenes.first as? UIWindowScene else {
                return
            }
            
            let window = UIWindow(windowScene: windowScenes)
            
            window.backgroundColor = .clear
            window.windowLevel = .alert + 1
            
            let spinnerView = SpinnerView(viewModel: payload.viewModel)
            let spinnerController = UIHostingController(rootView: spinnerView)
            spinnerController.view.backgroundColor = .clear
            spinnerController.view.frame = UIScreen.main.bounds
            
            window.rootViewController = spinnerController
            window.makeKeyAndVisible()
            
            self.spinner = Cover(type: .spinner, controller: spinnerController, window: window)
        }
    }
    
    func handleHideSpinnerRequest(_ payload: ModelAction.CoverAction.Spinner.Hide) {
        
        runOnMainQueue {
            
            guard let cover = spinner else {
                return
            }
            
            cover.window.isHidden = true
            self.spinner = nil
        }
    }
    
}
