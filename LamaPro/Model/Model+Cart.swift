//
//  Model+Cart.swift
//  LamaPro
//
//  Created by Михаил on 25.03.2023.
//

import Foundation

extension ModelAction {
    
    enum Cart {
        
        struct Update: Action {
            
            let id: String
            let count: Int
        }
    }
}

extension Model {
    
    func handleCartUpdateRequest(_ payload: ModelAction.Cart.Update) {
        LoggerAgent.shared.log(category: .model, message: "handleCartUpdateRequest")
        
        if let index = cartElements.value.items.firstIndex(where: { $0.elementId == payload.id }) {
            
            if payload.count > 0 {
                // обновляем количество
                cartElements.value.items[index].count = payload.count
                
            } else {
                // удаляем
                cartElements.value.items.remove(at: index)
            }
            
        } else {
            // создаем новый
            if payload.count > 0 {
                cartElements.value.items.append(CartData.Item(elementId: payload.id, count: payload.count))
            }
        }
    }
    
    @discardableResult
    func handleOrderCart(items: [CartData.Item],
                         name: String,
                         secondName: String?,
                         lastName: String,
                         phone: String,
                         email: String,
                         address: String,
                         userType: UserTypeData,
                         delivery: DeliveryType,
                         company: String? = nil,
                         inn: String? = nil,
                         kpp: String? = nil
    ) async throws -> Int {
        LoggerAgent.shared.log(category: .model, message: "handleCatalogOrderCart")
        
        let login: String = try self.keychainAgent.load(type: .login)
        let pass: String = try self.keychainAgent.load(type: .password)
        
        let command = ServerCommands.Order.OrderCart(body: .init(login: login, pass: pass, name: name, secondName: secondName, lastName: lastName, phone: phone, email: email, address: address, userType: userType, delivery: delivery, company: company, inn: inn, kpp: kpp, list: items.map({.init(productId: $0.elementId, quantity: $0.count)})))
        
        let result = try await serverAgent.executeCommand(command: command)
        
        return result.id
    }
    
    
    func handlePreOrder(name: String, article: String, phone: String) async throws {
        LoggerAgent.shared.log(category: .model, message: "handlePreOrder")
        
        let login: String = try self.keychainAgent.load(type: .login)
        let pass: String = try self.keychainAgent.load(type: .password)
        
        let command = ServerCommands.Order.PreOrders(login: login, pass: pass, name: name, article: article, phone: phone)
        
        _ = try await serverAgent.executeCommand(command: command)
        
    }
    
    @discardableResult
    func handleGetOrderList() async throws -> [OrderData] {
        LoggerAgent.shared.log(category: .model, message: "handleGetOrderList")
        
        let login: String = try self.keychainAgent.load(type: .login)
        let pass: String = try self.keychainAgent.load(type: .password)
        
        let command = ServerCommands.Order.GetOrders(login: login, pass: pass)
        
        let result = try await serverAgent.executeCommand(command: command)
        self.myOrders.value = result
        try? localAgent.store(result)
        
        return result
    }
    
}
