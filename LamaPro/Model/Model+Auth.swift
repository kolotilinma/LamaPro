//
//  Model+Auth.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

extension Model {
    
    @discardableResult
    func handleAuthLogin(login: String, pass: String) async throws -> SessionCredentials {
        LoggerAgent.shared.log(category: .model, message: "handleAuthLogin")
        
        let command = ServerCommands.AuthController.Login(login: login, pass: pass)
        
        let credentials = try await serverAgent.executeCommand(command: command)
        auth.value = .active(credentials: credentials)
        try keychainAgent.store(login, type: .login)
        try keychainAgent.store(pass, type: .password)
        try await handleAuthGetProfile()
        
        return credentials
    }
    
    @discardableResult
    func handleAuthRegister(name: String, email: String, password: String, mailing: Bool, politics: Bool) async throws -> SessionCredentials {
        LoggerAgent.shared.log(category: .model, message: "handleAuthRegister")
        
        let command = ServerCommands.AuthController.Register(name: name, lastName: " ", email: email, password: password, phone: " ", mailing: mailing, politics: politics)
        
        _ = try await serverAgent.executeCommand(command: command)
        let credentials = try await handleAuthLogin(login: email, pass: password)
        
        return credentials
    }
    
    @discardableResult
    func handleAuthGetProfile() async throws -> UserData {
        LoggerAgent.shared.log(category: .model, message: "handleAuthGetProfile")
        
        let command = ServerCommands.AuthController.GetProfile(token: token)
        
        let user = try await serverAgent.executeCommand(command: command)
        self.user.value = user
        
        return user
    }
    
    func handleAuthLogout() {
        LoggerAgent.shared.log(category: .model, message: "handleAuthLogout")
        
        auth.value = .inactive
        myOrders.value = []
        clearKeychainData()
        action.send(TabBarAction.CloseAll())
    }
    
    func handleAuthChangePassword(newPassword: String) async throws -> Bool {
        LoggerAgent.shared.log(category: .model, message: "handleAuthChangePassword")
        
        let login: String = try self.keychainAgent.load(type: .login)
        let pass: String = try self.keychainAgent.load(type: .password)
        
        let command = ServerCommands.AuthController.UpdateProfile(token: token, body: .init(login: login, pass: pass, new_pass: newPassword))
        
        let result = try await serverAgent.executeCommand(command: command).success
        if result == true {
            try keychainAgent.store(newPassword, type: .password)
        }
        return result
    }
    
    func handleAuthChangeUserData(name: String? = nil, secondName: String? = nil, lastName: String? = nil, phone: String? = nil, email: String? = nil) async throws -> Bool {
        LoggerAgent.shared.log(category: .model, message: "handleAuthChangeUserData")
        
        let login: String = try self.keychainAgent.load(type: .login)
        let pass: String = try self.keychainAgent.load(type: .password)
        
        let command = ServerCommands.AuthController.UpdateProfile(token: token, body: .init(login: login, pass: pass, name: name, second_name: secondName, last_name: lastName, phone: phone, email: email))
        
        return try await serverAgent.executeCommand(command: command).success
    }
}

