//
//  ModelAction.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

enum ModelAction {

    struct LoggedIn: Action {}
    
    //TODO: remove after refactoring
    struct LoggedOut: Action {}
    
    struct PresentAlert: Action {
        
        let message: String
    }
    
    struct NetworkErrorAlert: Action {
        
        let message: String
    }
    
    enum App {
    
        struct Launched: Action {}
        
        struct Activated: Action {}
        
        struct Inactivated: Action {}
    }
    
    enum UpdateAction {
        struct UserData: Action {
            let user: (name: String?, secondName: String?, lastName: String?, phone: String?, email: String?)
        }
        
    }
}
