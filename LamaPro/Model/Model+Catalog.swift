//
//  Model+Catalog.swift
//  LamaPro
//
//  Created by Михаил on 22.02.2023.
//

import Foundation

extension Model {
    
    private enum FetchResult {
        
        case section([CatSection])
        case catalog([CatElement])
        case offers([CatOffer])
    }
    
    private func handleCatalogGetSections() async throws -> [CatSection] {
        LoggerAgent.shared.log(category: .model, message: "handleCatalogGetSections")
        
        let command = ServerCommands.CatalogController.GetSections()

        let result = try await serverAgent.executeCommand(command: command)
        if catalogSections.value != result {
            catalogSections.value = result
        }
        return result
    }
    
    private func handleCatalogGetElements() async throws -> [CatElement] {
        LoggerAgent.shared.log(category: .model, message: "handleCatalogGetElements")
        
        let command = ServerCommands.CatalogController.GetElements()
        
        let result = try await serverAgent.executeCommand(command: command)
        let sorted = result.sorted(by: { $0.quantity > $1.quantity })
        if catalogElements.value != sorted {
            catalogElements.value = sorted
        }
        return sorted
    }
    
    private func handleCatalogGetOffers() async throws -> [CatOffer]  {
        LoggerAgent.shared.log(category: .model, message: "handleCatalogGetOffers")
        
        let command = ServerCommands.CatalogController.GetOffers()
        
        let result = try await serverAgent.executeCommand(command: command)
        catalogOffers.value = result
        
        return result
    }
    
    func fetchCatalogData() async {
        LoggerAgent.shared.log(category: .model, message: "fetchCatalogData")
        catalogStatus.value = .loading
        
        let result = await withThrowingTaskGroup(of: FetchResult.self) { group -> FullCatalog in
            
            group.addTask {
                return .section(try await self.handleCatalogGetSections())
            }
            group.addTask {
                return .catalog(try await self.handleCatalogGetElements())
            }
            group.addTask {
                return .offers(try await self.handleCatalogGetOffers())
            }
            
            var cat: FullCatalog = .init(section: [], catalog: [], offers: [])
            
            do {
                
                for try await value in group {
                    switch value {
                        
                    case .section(let section):
                        cat.section = section
                    case .catalog(let catalog):
                        cat.catalog = catalog
                    case .offers(let offers):
                        cat.offers = offers
                    }
                }
                
            } catch {
                if let error = error as? ServerAgentError {
                    
                    catalogStatus.value = .error(error)
                }
            }
            return cat
        }
        
        catalogStatus.value = .done
        
        try? self.localAgent.store(result)
    }
    
}
