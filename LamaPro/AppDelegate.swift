//
//  AppDelegate.swift
//  LamaPro
//
//  Created by Михаил on 22.02.2023.
//

import SwiftUI
import SDWebImageSwiftUI
import SDWebImageSVGNativeCoder
import SDWebImageSVGCoder

class AppDelegate: NSObject, UIApplicationDelegate {
  
    static var shared: AppDelegate { return UIApplication.shared.delegate as! AppDelegate }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
//        SDImageCodersManager.shared.addCoder(SDImageWebPCoder.shared)
        SDImageCodersManager.shared.addCoder(SDImageSVGNativeCoder.shared)
        SDImageCodersManager.shared.addCoder(SDImageSVGCoder.shared)
        
        return true
    }
    
    
}
