//
//  ContentView.swift
//  LamaPro
//
//  Created by Михаил on 21.12.2022.
//

import SwiftUI
import Combine

class ContentViewModel: ObservableObject {
    
    @Published var state: AuthState
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model) {
        
        self.model = model
        self.state = .auth(AuthentificationViewModel(model: model,backAction: {
            
        }))
        
        bind()
    }
    
    enum AuthState {
        
        case tabbar(MainTabBarViewModel)
        case auth(AuthentificationViewModel)
    }
    
    func bind() {
        
        model.auth
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] auth in
                withAnimation() {
                    
                    switch auth {
                        
                    case .inactive:
                        state = .auth(AuthentificationViewModel(model: model, backAction: {
                            
                        }))
                        
                    case .active(credentials: _):
                        state = .tabbar(MainTabBarViewModel(model: model))
                        
                    case .expired:
                        break
                        
                    case .failed(_):
                        break
                    }
                }
            }
            .store(in: &bindings)
    }
}

struct ContentView: View {
    
    @ObservedObject var viewModel: ContentViewModel
    
    var body: some View {
        
        NavigationView {
            
            switch viewModel.state {
                
            case .auth(let authentification):
                AuthentificationView(viewModel: authentification)
                
            case .tabbar(let mainTabBar):
                MainTabBarView(viewModel: mainTabBar)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: .init(model: .emptyMock))
    }
}
