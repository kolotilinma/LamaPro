//
//  CatalogStatus.swift
//  LamaPro
//
//  Created by Михаил on 26.03.2023.
//

import Foundation

enum CatalogStatus {
    
    case loading
    case done
    case error(ServerAgentError)
}
