//
//  OrderStatus.swift
//  LamaPro
//
//  Created by Михаил on 30.03.2023.
//

import SwiftUI

enum OrderStatus: String, Codable, Unknownable {
    
    case new = "N"
    case confirmed = "C"
    case orderPicking = "DA"
    case delivery = "DF"
    case waitingToArrive = "DG"
    case awaitingProcessing = "DN"
    case transferredToDelivery = "DS"
    case waitingForPickup = "DT"
    case completed = "F"
    case canceled = "GG"
    case shipped = "LO"
    case inWork = "LR"
    case paid = "P"
    case unknown
    
    var statusDescription: String {
        switch self {
        case .new:
            return "Принят, проверяется менеджером"
            
        case .confirmed:
            return "Подтвержден, готов к оплате"
            
        case .orderPicking:
            return "Комплектация заказа"
            
        case .delivery:
            return "Доставка"
            
        case .waitingToArrive:
            return "Ожидаем приход товара"
            
        case .awaitingProcessing:
            return "Ожидает обработки"
            
        case .transferredToDelivery:
            return "Передан в службу доставки"
            
        case .waitingForPickup:
            return "Ожидаем забора транспортной компанией"
            
        case .completed:
            return "Выполнен"
            
        case .canceled:
            return "Отказ со стороны клиента"
            
        case .shipped:
            return "Отгружен"
            
        case .inWork:
            return "В работе"
            
        case .paid:
            return "Оплачен, формируется к отправке"
            
        case .unknown:
            return ""
            
        }
    }
    
    var statusImage: Image {
        switch self {
            
        case .paid, .completed, .confirmed:
            return Image("checkmark")
        
        case .new, .waitingToArrive, .awaitingProcessing, .waitingForPickup, .transferredToDelivery, .shipped, .orderPicking, .delivery, .inWork:
            return Image("waitYellow")
            
        case .canceled, .unknown:
            return Image("crossmark")
        }
    }
}
