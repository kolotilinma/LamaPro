//
//  CartData.swift
//  LamaPro
//
//  Created by Михаил on 20.03.2023.
//

import Foundation

struct CartData: Codable {
    
    var items: [Item]
    
    struct Item: Codable {
        
        let elementId: String
        var count: Int
    }
    
}

struct OrderCartDataReqest: Codable {
    
    let login: String
    let pass: String
    var type: String = "order"
    let address: String
    let phone: String
    let email: String
    let fio: String
    let name: String
    let surname: String
    let patronymic: String?
    let user_type: UserTypeData
    let delivery: DeliveryType
    let list: [CartItem]
    let company: String?
//    let company_adr: String?
    let inn: String?
    let kpp: String?
    
    internal init(login: String, pass: String, name: String, secondName: String?, lastName: String, phone: String, email: String, address: String, userType: UserTypeData, delivery: DeliveryType, company: String?, inn: String?, kpp: String?, list: [CartItem]) {
        
        self.login = login
        self.pass = pass
        self.address = address
        self.user_type = userType
        self.delivery = delivery
        self.name = name
        self.surname = lastName
        self.patronymic = secondName
        self.fio = "\(lastName) \(name) \(secondName ?? "")"
        self.phone = phone
        self.email = email
        self.company = company
        self.inn = inn
        self.kpp = kpp
        self.list = list
    }
    
    struct CartItem: Codable {
        
        let productId: String
        let quantity: Int
    }
}
