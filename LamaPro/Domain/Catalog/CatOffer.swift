//
//  CatOffer.swift
//  LamaPro
//
//  Created by Михаил on 05.03.2023.
//

import Foundation

struct CatOffer: Codable, Identifiable {
    
    let id: String
    let iblockId: String?
    let iblockSectionId: String?
    let name: String
    let previewPicture: String
    let detailPicture: String
    let detailText: String?
    let activeFrom: String? // Bool
    let sort: String
    let detailTextType: TextType?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "ID"
        case iblockId = "IBLOCK_ID"
        case iblockSectionId = "IBLOCK_SECTION_ID"
        case name = "NAME"
        case previewPicture = "PREVIEW_PICTURE"
        case detailPicture = "DETAIL_PICTURE"
        case detailText = "DETAIL_TEXT"
        case activeFrom = "ACTIVE_FROM"
        case sort = "SORT"
        case detailTextType = "DETAIL_TEXT_TYPE"
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.iblockId = try container.decodeIfPresent(String.self, forKey: .iblockId)
        self.iblockSectionId = try container.decodeIfPresent(String.self, forKey: .iblockSectionId)
        
        let nameText = try container.decode(String.self, forKey: .name)
        self.name = nameText.replacingOccurrences(of: "&quot;", with: "\"")
        
        self.previewPicture = try container.decode(String.self, forKey: .previewPicture)
        self.detailPicture = try container.decode(String.self, forKey: .detailPicture)
        self.detailText = try container.decodeIfPresent(String.self, forKey: .detailText)
        self.activeFrom = try container.decodeIfPresent(String.self, forKey: .activeFrom)
        self.sort = try container.decode(String.self, forKey: .sort)
        self.detailTextType = try container.decodeIfPresent(TextType.self, forKey: .detailTextType)
    }
    
}
