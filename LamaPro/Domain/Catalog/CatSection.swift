//
//  CatSection.swift
//  LamaPro
//
//  Created by Михаил on 22.02.2023.
//

import Foundation

struct CatSection: Codable, Identifiable, Equatable {
    
    let id: String
    let iblockSectionId: String?
    let active: String? // Bool
    let sort: String
    let name: String
    let picture: String?
    let depthLevel: String?
    let description: String?
    let descriptionType: String?
    let searchableContent: String?
    let code: String?
    let detailPicture: String?
    let externalId: String?
    let previewPicture: String
    
    enum CodingKeys: String, CodingKey {
        
        case id = "ID"
        case iblockSectionId = "IBLOCK_SECTION_ID"
        case active = "ACTIVE"
        case sort = "SORT"
        case name = "NAME"
        case picture = "PICTURE"
        case depthLevel = "DEPTH_LEVEL"
        case description = "DESCRIPTION"
        case descriptionType = "DESCRIPTION_TYPE"
        case searchableContent = "SEARCHABLE_CONTENT"
        case code = "CODE"
        case detailPicture = "DETAIL_PICTURE"
        case externalId = "EXTERNAL_ID"
        case previewPicture = "PREVIEW_PICTURE"
    }
}
