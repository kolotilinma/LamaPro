//
//  CatElement.swift
//  LamaPro
//
//  Created by Михаил on 26.02.2023.
//

import Foundation

struct CatElement: Codable, Identifiable {
    
    let id: String
    let iblockId: String?
    let iblockSectionId: String?
    let name: String
    let previewPicture: String
    let detailPicture: String
    let detailText: String?
    let activeFrom: String? // Bool
    let sort: String
    let detailTextType: TextType?
    let previewText: String?
    let article: String
    let manufacturer: String?
    let minPartia: String?
    let isNew: String?
    let price: PriceData?
    let quantity: Int
    let weight: Int
    
    enum CodingKeys: String, CodingKey {
        
        case id = "ID"
        case iblockId = "IBLOCK_ID"
        case iblockSectionId = "IBLOCK_SECTION_ID"
        case name = "NAME"
        case previewPicture = "PREVIEW_PICTURE"
        case detailPicture = "DETAIL_PICTURE"
        case detailText = "DETAIL_TEXT"
        case activeFrom = "ACTIVE_FROM"
        case sort = "SORT"
        case detailTextType = "DETAIL_TEXT_TYPE"
        case previewText = "PREVIEW_TEXT"
        case article = "ARTICLE"
        case manufacturer = "MANUFACTURER"
        case minPartia = "MIN_PARTIA"
        case price = "PRICE"
        case isNew = "ISNEW"
        case quantity = "QUANTITY2"
        case weight = "weight"
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.iblockId = try container.decodeIfPresent(String.self, forKey: .iblockId)
        self.iblockSectionId = try container.decodeIfPresent(String.self, forKey: .iblockSectionId)
        
        let nameText = try container.decode(String.self, forKey: .name)
        self.name = nameText.replacingOccurrences(of: "&quot;", with: "\"")
        
        self.previewPicture = try container.decode(String.self, forKey: .previewPicture)
        self.detailPicture = try container.decode(String.self, forKey: .detailPicture)
        self.detailText = try container.decodeIfPresent(String.self, forKey: .detailText)
        self.activeFrom = try container.decodeIfPresent(String.self, forKey: .activeFrom)
        self.sort = try container.decode(String.self, forKey: .sort)
        self.detailTextType = try container.decodeIfPresent(TextType.self, forKey: .detailTextType)
        self.previewText = try container.decodeIfPresent(String.self, forKey: .previewText)
        self.article = try container.decode(String.self, forKey: .article)
        self.manufacturer = try container.decodeIfPresent(String.self, forKey: .manufacturer)
        self.minPartia = try container.decodeIfPresent(String.self, forKey: .minPartia)
        self.price = try container.decodeIfPresent(PriceData.self, forKey: .price)
        self.isNew = try container.decodeIfPresent(String.self, forKey: .isNew)
        self.quantity = Int(try container.decodeIfPresent(String.self, forKey: .quantity) ?? "") ?? 0
        self.weight = Int(try container.decodeIfPresent(String.self, forKey: .weight) ?? "") ?? 0
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(iblockId, forKey: .iblockId)
        try container.encode(iblockSectionId, forKey: .iblockSectionId)
        try container.encode(name, forKey: .name)
        try container.encode(previewPicture, forKey: .previewPicture)
        try container.encode(detailPicture, forKey: .detailPicture)
        try container.encode(detailText, forKey: .detailText)
        try container.encode(activeFrom, forKey: .activeFrom)
        try container.encode(sort, forKey: .sort)
        try container.encode(detailTextType, forKey: .detailTextType)
        try container.encode(previewText, forKey: .previewText)
        try container.encode(article, forKey: .article)
        try container.encode(manufacturer, forKey: .manufacturer)
        try container.encode(minPartia, forKey: .minPartia)
        try container.encode(price, forKey: .price)
        try container.encode(isNew, forKey: .isNew)
        try container.encode(String(quantity), forKey: .quantity)
        try container.encode(String(weight), forKey: .weight)
    }
    
    func setupPrice(itemsCount: Int) -> String {
        
        guard let price = self.price else { return "" }
        let formatter = NumberFormatter.formatter(currencyCode: price.currency)
        guard let double = Double(price.price), let priceString = formatter.string(from: NSNumber(value: double * Double(itemsCount))) else { return "" }
        
        return priceString
        
    }
    
    struct PriceData: Codable {
        
        let id: String
        let productId: String
        let price: String
        let currency: String
        let catalogGroupName: String
        let canAccess: String
        let canBuy: String
        
        enum CodingKeys: String, CodingKey {
            
            case id = "ID"
            case productId = "PRODUCT_ID"
            case price = "PRICE"
            case currency = "CURRENCY"
            case catalogGroupName = "CATALOG_GROUP_NAME"
            case canAccess = "CAN_ACCESS"
            case canBuy = "CAN_BUY"
        }
        
        
        var priceString: String? {
            
            let formatter = NumberFormatter.formatter(currencyCode: currency)
            
            guard let double = Double(price) else { return nil }
            
            return formatter.string(from: NSNumber(value: double))
        }
        
    }
}

extension CatElement: Equatable {
    static func == (lhs: CatElement, rhs: CatElement) -> Bool {
        return lhs.id == rhs.id
        && lhs.iblockSectionId == rhs.iblockSectionId
        && lhs.name == rhs.name
        && lhs.detailPicture == rhs.detailPicture
        && lhs.detailText == rhs.detailText
        && lhs.activeFrom == rhs.activeFrom
        && lhs.sort == rhs.sort
        && lhs.previewText == rhs.previewText
        && lhs.article == rhs.article
    }
    
    
    
}

enum TextType: String, Codable, Cachable, Unknownable {
    
    case text
    case html
    
    case unknown
}
