//
//  FullCatalog.swift
//  LamaPro
//
//  Created by Михаил on 30.03.2023.
//

import Foundation

struct FullCatalog: Codable {
    
    var section: [CatSection]
    var catalog: [CatElement]
    var offers: [CatOffer]
}
