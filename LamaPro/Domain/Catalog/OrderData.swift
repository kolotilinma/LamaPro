//
//  OrderData.swift
//  LamaPro
//
//  Created by Михаил on 29.03.2023.
//

import Foundation

struct OrderData: Codable, Identifiable {
    
    let id: String
    let userId: String
    let payed: String
    let status: OrderStatus
    let reserved: String
    let price: String
    let userDescription: String?
    let currency: String
    let orderPrice: Double
    let orderDate: Date
    let basket: [Basket]
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case userId = "user_id"
        case payed = "payed"
        case status = "status_id"
        case reserved = "RESERVED"
        case price = "PRICE"
        case userDescription = "USER_DESCRIPTION"
        case currency = "CURRENCY"
        case orderPrice = "order_price"
        case orderDate = "order_date"
        case basket = "basket"
    }
    
    init(from decoder: Decoder) throws {
        
        let formatter = DateFormatter.dateAndTime
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.userId = try container.decode(String.self, forKey: .userId)
        self.payed = try container.decode(String.self, forKey: .payed)
        self.status = try container.decode(OrderStatus.self, forKey: .status)
        self.reserved = try container.decode(String.self, forKey: .reserved)
        self.price = try container.decode(String.self, forKey: .price)
        self.userDescription = try container.decodeIfPresent(String.self, forKey: .userDescription)
        self.currency = try container.decode(String.self, forKey: .currency)
        self.orderPrice = try container.decode(Double.self, forKey: .orderPrice)
        self.basket = try container.decode([Basket].self, forKey: .basket)
        self.orderDate = formatter.date(from: try container.decode(String.self, forKey: .orderDate)) ?? Date()
    }
    
    func encode(to encoder: Encoder) throws {
        let formatter = DateFormatter.dateAndTime
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(userId, forKey: .userId)
        try container.encode(payed, forKey: .payed)
        try container.encode(status, forKey: .status)
        try container.encode(reserved, forKey: .reserved)
        try container.encode(price, forKey: .price)
        try container.encode(userDescription, forKey: .userDescription)
        try container.encode(currency, forKey: .currency)
        try container.encode(orderPrice, forKey: .orderPrice)
        try container.encode(basket, forKey: .basket)
        try container.encode(formatter.string(from: orderDate), forKey: .orderDate)
    }
    
    struct Basket: Codable, Equatable {
        
        let id: Int
        let name: String
        let quantity: Int
        let price: Double
        let weight: String
    }
    
    var basketDescription: String {
        
//        var attr: AttributedString = ""
        
        var description: String = ""
        
        for element in basket {
            
            if element == basket.last {
                description.append(element.name)
                description.append(" \(element.quantity) шт")
                
            } else {
                description.append(element.name)
                description.append(" \(element.quantity) шт, ")
            }
        }
        
        return description
    }
}
