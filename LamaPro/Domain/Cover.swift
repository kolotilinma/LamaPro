//
//  Cover.swift
//  LamaPro
//
//  Created by Михаил on 23.01.2023.
//

import UIKit

public struct Cover {
    
    let type: Kind
    let controller: UIViewController
    let window: UIWindow
    
    public enum Kind {
        
        case spinner
        case login
        case alert
    }
}
