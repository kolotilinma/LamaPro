//
//  ActionSheetModel.swift
//  LamaPro
//
//  Created by Михаил on 09.04.2023.
//

import SwiftUI

struct ActionSheetModel: Identifiable {
    
    let id = UUID()
    let title: String
    let buttons: [Alert.Button]
    
    init(title: String, buttons: [Alert.Button]) {
        
        self.title = title
        self.buttons = buttons
    }
}
