//
//  KeychainValueType.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

enum KeychainValueType: String {
    
    case pincode
//    case token
    case login
    case password
}
