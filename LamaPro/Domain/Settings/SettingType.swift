//
//  SettingType.swift
//  LamaPro
//
//  Created by Михаил on 07.03.2023.
//

import Foundation

enum SettingType {
    
    case general(General)
    case security(Security)
    case interface(Interface)
        
    enum General: String {
        
        case launchedBefore
    }
    
    enum Security: String {
        
        case sensor
        case push
        case block
    }
    
    enum Interface: String {
        
        case mainSections
        case inactiveProducts
        case tempates
        case productsHidden
        case productsSections
        case productsMoney
    }
}

extension SettingType {
    
    var identifier: String {
        
        switch self {
        case let .general(general):
            return "setting_general_\(general.rawValue)"
           
        case let .security(security):
            return "setting_security_\(security.rawValue)"
            
        case let .interface(interface):
            return "setting_interface_\(interface.rawValue)"
        }
    }
}

extension SettingType: CustomDebugStringConvertible {
    
    var debugDescription: String {
        
        switch self {
        case .general(let value): return "general : \(value.rawValue)"
        case .security(let value): return "security : \(value.rawValue)"
        case .interface(let value): return "interface : \(value.rawValue)"
        }
    }
}
