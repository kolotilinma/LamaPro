//
//  SessionCredentials.swift
//  LamaPro
//
//  Created by Михаил on 12.02.2023.
//

import Foundation

struct SessionCredentials: Codable {
    
    let id: String
    let token: String
    let refreshToken: String
    
    enum CodingKeys: String, CodingKey {
        
        case token, id
        case refreshToken = "refresh_token"
    }
}
