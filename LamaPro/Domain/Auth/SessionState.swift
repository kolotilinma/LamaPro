//
//  SessionState.swift
//  LamaPro
//
//  Created by Михаил on 12.02.2023.
//

import Foundation

enum SessionState {
    
    case inactive
    case active(credentials: SessionCredentials)
    case expired
    case failed(Error)
}

extension SessionState: CustomDebugStringConvertible {
    
    var debugDescription: String {
        
        switch self {
        case .inactive: return "INACTIVE"
        case .active: return "ACTIVE"
        case .expired: return "EXPIRED"
        case .failed(let error): return "FAILED: \(error.localizedDescription)"
        }
    }
}
