//
//  UserTypeData.swift
//  LamaPro
//
//  Created by Михаил on 30.03.2023.
//

import Foundation

enum UserTypeData: String, Codable {
    
    case individual = "1"
    case entity = "2"
    
    var paymentTextDescription: String {
        
        switch self {
            
        case .entity:
            return "Cчёт на оплату"
            
        case .individual:
            return "Оплата картами Visa/MasterCard"
        }
    }
}
