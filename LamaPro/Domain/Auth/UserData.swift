//
//  UserData.swift
//  LamaPro
//
//  Created by Михаил on 13.02.2023.
//

import Foundation

struct UserData: Codable {
    
    var id: String
    var name: String
    var secondName: String?
    var lastName: String
    var phone: String
    var birthday: String?
    var photo: String?
    var gender: String
    var login: String
    var email: String
    var lastLogin: String
    var dateRegister: String
    
    enum CodingKeys: String, CodingKey {
        
        case id = "ID"
        case name = "NAME"
        case secondName = "SECOND_NAME"
        case lastName = "LAST_NAME"
        case phone = "PERSONAL_PHONE"
        case birthday = "PERSONAL_BIRTHDATE"
        case photo = "PERSONAL_PHOTO"
        case gender = "PERSONAL_GENDER"
        case login = "LOGIN"
        case email = "EMAIL"
        case lastLogin = "LAST_LOGIN"
        case dateRegister = "DATE_REGISTER"
        
    }
    
}
