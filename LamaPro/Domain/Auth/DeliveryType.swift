//
//  DeliveryType.swift
//  LamaPro
//
//  Created by Михаил on 09.04.2023.
//

import Foundation

enum DeliveryType: String, Codable {
    
    case pickup = "3"
    case delivery = "108"
    
    var deliveryTextDescription: String {

        switch self {

        case .pickup:
            return "Самовывоз"

        case .delivery:
            return "Доставка транстпортными компаниями"
        }
    }
}
