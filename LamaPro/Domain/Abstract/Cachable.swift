//
//  Cachable.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

protocol Cachable: Codable {}
