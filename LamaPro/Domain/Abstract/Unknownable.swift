//
//  Unknownable.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

protocol Unknownable: Decodable {
    
    static var unknown: Self { get }
}

extension Unknownable where Self: RawRepresentable, Self.RawValue == String {
    
    init(from decoder: Decoder) throws {
        
        do {
            
            self = try Self(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
            
        } catch {
            
            self = .unknown
        }
    }
}
