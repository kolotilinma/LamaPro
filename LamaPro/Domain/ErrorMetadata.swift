//
//  ErrorMetadata.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

struct ErrorMetadata: Codable {
    
    let errors: [Error]
    
    struct Error: Codable {
        
        let code: String
        let key: String
        let message: String
    }
}
