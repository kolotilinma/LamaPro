//
//  ServerAgent.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

class ServerAgent: NSObject, ServerAgentProtocol {
   
    var baseURL: String { enviroment.baseURL }
    private let enviroment: Environment
    
    private let encoder: JSONEncoder
    private let decoder: JSONDecoder
    
    private lazy var session: URLSession = {
        
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = false
        configuration.httpCookieAcceptPolicy = .never

        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    
    init(enviroment: Environment) {
        
        self.enviroment = enviroment
        self.encoder = JSONEncoder()//.serverDate
        self.decoder = JSONDecoder()//.serverDate
    }
    
    func executeCommand<Command>(command: Command, completion: @escaping (Result<Command.Response, ServerAgentError>) -> Void) where Command : ServerCommand {
        
        do {
            
            let request = try request(with: command)
            LoggerAgent.shared.log(category: .network, message: "data request: \(request)")
            session.dataTask(with: request) { [unowned self] data, response, error in
                
                if let error = error {
                    debugPrint("DEBUG request ERROR: \(error)")
                    completion(.failure(.sessionError(error)))
                    return
                }
                
                guard let data = data else {
                    
                    completion(.failure(.emptyResponseData))
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    
                    if response.statusCode == 200 {
                       
                        if let empty = EmptyData() as? Command.Response {
//                            debugPrint("DEBUG Response Answer \(response.url?.absoluteString ?? "") Empty")
                            completion(.success(empty))
                            
                        } else {
                            
                            do {
//                                if let str = String(data: data, encoding: .utf8) {
//                                    print("DEBUG Response Answer \(response.url?.absoluteString ?? "") ", str)
//                                }
                                let response = try decoder.decode(Command.Response.self, from: data)
                                completion(.success(response))
                                
                            } catch {
                                if error is DecodingError {
                                    
                                    LoggerAgent.shared.log(category: .network, message: "DecodingError in \(command.endpoint): \(error)")
                                }
                                completion(.failure(.curruptedData(error)))
                            }
                        }
                    } else if response.statusCode == 204 {
                        
                        if let empty = EmptyData() as? Command.Response {
                            completion(.success(empty))
                        } else {
                            completion(.failure(.emptyResponseData))
                        }
                        
                    } else {
                        
                        do {
                            if let str = String(data: data, encoding: .utf8) {
                                debugPrint("DEBUG Response Error \(response.url?.absoluteString ?? "") ", str)
                            }
                            let response = try decoder.decode(ErrorMetadata.self, from: data)
                            debugPrint("DEBUG request ERROR: \(response.errors.first?.message ?? "")")
                            // TODO: parse server error
                            completion(.failure(.serverStatus(errorMessage: response)))
                            
                        } catch let error {
                            if error is DecodingError {
                                
                                LoggerAgent.shared.log(category: .network, message: "DecodingError in \(command.endpoint): \(error)")
                            }
                            completion(.failure(.curruptedData(error)))
                        }
                        
                    }
                    
                }
                
            }.resume()
            
        } catch {
            debugPrint("DEBUG request ERROR: \(error)")
            completion(.failure(ServerAgentError.requestCreationError(error)))
        }
    }
    
    func executeCommand<Command>(command: Command) async throws -> Command.Response where Command: ServerCommand {
        
//        debugPrint("DEBUG command: \(Command.self)")
        do {
            let request = try request(with: command)
            LoggerAgent.shared.log(category: .network, message: "data request: \(request)")
            let (data, response) = try await session.data(for: request)
            
            guard let response = response as? HTTPURLResponse else { throw ServerAgentError.emptyResponseData }
            
            if response.statusCode == 200 {
                
                if let error = try? decoder.decode(ErrorMetadata.self, from: data) {
                    throw ServerAgentError.serverStatus(errorMessage: error)
                    
                } else {
                    
                    if let empty = EmptyData() as? Command.Response {
                        
                        return empty
                        
                    } else {
                        
                        let response = try decoder.decode(Command.Response.self, from: data)
                        return response
                    }
                }
            } else {
                
                let response = try decoder.decode(ErrorMetadata.self, from: data)
//                debugPrint("DEBUG request ERROR: \(response.message)")
                throw ServerAgentError.serverStatus(errorMessage: response)
            }
        } catch {
            throw error
        }
    }
    
    func request<Command>(with command: Command) throws -> URLRequest where Command : ServerCommand {
        
        guard let url = URL(string: baseURL + command.endpoint) else {
            throw ServerRequestCreationError.unableConstructURL
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = command.method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // token
        if let token = command.token {
            
            request.setValue("\(token)", forHTTPHeaderField: "Token")
        }
        
        // parameters
        if let parameters = command.query, parameters.isEmpty == false {
            
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
            urlComponents?.queryItems = parameters.map{ URLQueryItem(name: $0.name, value: $0.value) }
            
            guard let updatedURL = urlComponents?.url else {
                throw ServerRequestCreationError.unableCounstructURLWithParameters
            }
            
            request.url = updatedURL
        }
        
        // body
        if let payload = command.body {
            
            do {
                
                request.httpBody = try encoder.encode(payload)
                
            } catch {
                
                throw ServerRequestCreationError.unableEncodePayload(error)
            }
        }
        
        return request
    }
    
}

//MARK: - URLSessionTaskDelegate

extension ServerAgent: URLSessionTaskDelegate {
 
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        
        if let error = error {
            
            LoggerAgent.shared.log(level: .error, category: .network, message: "URL Session did become invalid with error: \(error.localizedDescription)")

        } else {

            LoggerAgent.shared.log(level: .error, category: .network, message: "URL Session did become invalid")
       }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        if let error = error {
            
            LoggerAgent.shared.log(level: .error, category: .network, message: "URLSessionTask: \(String(describing: task.originalRequest?.url)) did complete with error: \(error.localizedDescription)")

        } else {

            LoggerAgent.shared.log(level: .error, category: .network, message: "URLSessionTask: \(String(describing: task.originalRequest?.url)) did complete unexpected")
        }
    }
}


//MARK: - Context

extension ServerAgent {
    
    public enum Environment {
        
        case test
        case prod
        
        var baseURL: String {
            
            switch self {
            case .test:
                return "https://lama-pro.ru"
                
            case .prod:
                return "https://lama-pro.ru"
            }
        }
    }
}
