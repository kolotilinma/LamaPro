//
//  ServerCommands+Order.swift
//  LamaPro
//
//  Created by Михаил on 30.03.2023.
//

import Foundation

extension ServerCommands {
    
    enum Order {
        
        struct OrderCart: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/signin"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            typealias Body = OrderCartDataReqest
            
            struct Response: Decodable {
                
                let id: Int
            }
                        
            internal init(body: Body) {
                
                self.body = body
            }
        }
        
        struct GetOrders: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/signin"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let login: String
                let pass: String
                let type: String = "orderList"
            }
            
            typealias Response = [OrderData]
                        
            internal init(login: String, pass: String) {
                
                self.body = Body(login: login, pass: pass)
            }
        }
        
        struct PreOrders: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/signin"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let login: String
                let pass: String
                let type: String = "order2"
                let name: String
                let article: String
                let phone: String
            }
            
            typealias Response = EmptyData
                        
            internal init(login: String, pass: String, name: String, article: String, phone: String ) {
                
                self.body = Body(login: login, pass: pass, name: name, article: article, phone: phone)
            }
        }
        
    }
}
