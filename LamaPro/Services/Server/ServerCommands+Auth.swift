//
//  ServerCommands+Auth.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation

enum ServerCommands {
    
    enum AuthController {
        
        struct Login: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/signin"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let login: String
                let pass: String
            }
            
            typealias Response = SessionCredentials
                        
            internal init(login: String, pass: String) {
                
                body = .init(login: login, pass: pass)
            }
        }
        
        struct Register: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/register"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let name: String
                let last_name: String
                let email: String
                let pass: String
                let phone: String
                let UF_MAILING: String
                let UF_POLITICS: String
            }
            
            typealias Response = SessionCredentials
            
            internal init(name: String, lastName: String, email: String, password: String, phone: String, mailing: Bool, politics: Bool) {
                
                self.body = .init(name: name, last_name: lastName, email: email, pass: password, phone: phone, UF_MAILING: mailing ? "1" : "0" , UF_POLITICS: politics ? "1" : "0")
            }
        }
        
        struct GetProfile: ServerCommand {
            
            let token: String?
            let endpoint = "/mobileapp/api/profile"
            let method: ServerCommandMethod = .post
            let body: Body? = nil
            let query: [ServerCommandQuery]? = nil
            
            typealias Body = EmptyData
            
            typealias Response = UserData
            
            internal init(token: String?) {
                self.token = token
            }
        }
        
        struct UpdateProfile: ServerCommand {
            
            let token: String?
            let endpoint = "/mobileapp/api/signin"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let login: String
                let pass: String
                var type: String = "editProfile"
                var name: String? = nil
                var second_name: String? = nil
                var last_name: String? = nil
                var phone: String? = nil
                var email: String? = nil
                var new_pass: String? = nil
            }
            
            struct Response: Decodable {
                
                let success: Bool
            }
            
            internal init(token: String?, body: Body) {
                
                self.token = token
                self.body = body
            }
        }
    }
    
}
