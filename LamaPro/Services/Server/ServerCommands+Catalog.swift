//
//  ServerCommands+Catalog.swift
//  LamaPro
//
//  Created by Михаил on 22.02.2023.
//

import Foundation

extension ServerCommands {
    
    enum CatalogController {
        
        struct GetSections: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/elements"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let code: String
                let type: String
            }
            
            typealias Response = [CatSection]
                        
            internal init() {
                
                self.body = Body(code: "sections", type: "content")
            }
        }
        
        struct GetElements: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/elements"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let code: String
                let type: String
            }
            
            typealias Response = [CatElement]
                        
            internal init() {
                
                self.body = Body(code: "elements", type: "5")
            }
        }
        
        struct GetOffers: ServerCommand {
            
            let token: String? = nil
            let endpoint = "/mobileapp/api/elements"
            let method: ServerCommandMethod = .post
            let body: Body?
            let query: [ServerCommandQuery]? = nil
            
            struct Body: Encodable {
                
                let code: String
                let type: String
            }
            
            typealias Response = [CatOffer]
                        
            internal init() {
                
                self.body = Body(code: "elements", type: "4")
            }
        }
        
    }
    
}
