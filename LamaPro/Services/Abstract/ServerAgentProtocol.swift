//
//  ServerAgentProtocol.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation
import SwiftUI

protocol ServerAgentProtocol {
    
    var baseURL: String { get }
    
    @available(*, deprecated, message: "No longer used, use 'executeCommand<Command>(command: Command) async throws -> Command.Response'")
    func executeCommand<Command: ServerCommand>(command: Command, completion: @escaping (Result<Command.Response, ServerAgentError>) -> Void)
    
    func executeCommand<Command>(command: Command) async throws -> Command.Response where Command: ServerCommand
    
}

protocol ServerCommand {
    
    associatedtype Body: Encodable
    associatedtype Response: Decodable
    
    var token: String? { get }
    var endpoint: String { get }
    var method: ServerCommandMethod { get }
    var query: [ServerCommandQuery]? { get }
    var body: Body? { get }
}

extension ServerCommand {
    
    var request: URLRequest? { try? ServerAgent(enviroment: .test).request(with: self) }
}

enum ServerAgentError: Error {
    
    case requestCreationError(Error)
    case sessionError(Error)
    case emptyResponseData
    case curruptedData(Error)
    case serverStatus(errorMessage: ErrorMetadata)
}

enum ServerRequestCreationError: Error {
    
    case unableCreateCommand
    case unableConstructURL
    case unableCounstructURLWithParameters
    case unableEncodePayload(Error)
}

enum ServerCommandMethod: String {
    
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case delete = "DELETE"
}

struct ServerCommandQuery {
    
    let name: String
    let value: String
}
