//
//  UIFont+Extension.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import UIKit

public enum LamaFont: Int {
    
    case interBold
    case interMedium
    case interRegular
    case interSemiBold
    
    func fontName() -> String {
        switch self {
            
        case .interBold: return "Inter-Bold"
        case .interMedium: return "Inter-Medium"
        case .interRegular: return "Inter-Regular"
        case .interSemiBold: return "Inter-SemiBold"
        }
    }
}

public extension UIFont {
    
    class func FontWithName(_ name: LamaFont, size: CGFloat) -> UIFont? {
        let font = UIFont(name: name.fontName(), size: size)
        
        return font
    }
}
