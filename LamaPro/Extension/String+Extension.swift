//
//  String+Extension.swift
//  LamaPro
//
//  Created by Михаил on 27.02.2023.
//

import Foundation

extension String {

//    private static let __firstpart = "[A-Z0-9a-z]([A-Z0-9a-z._%+-]{0,30}[A-Z0-9a-z])?"
//    private static let __serverpart = "([A-Z0-9a-z]([A-Z0-9a-z-]{0,30}[A-Z0-9a-z])?\\.){1,5}"
//    private static let __emailRegex = __firstpart + "@" + __serverpart + "[A-Za-z]{2,8}"
//    private static let __emailPredicate = NSPredicate(format: "SELF MATCHES %@", __emailRegex)
//
//    var isValidEmail: Bool {
//        return String.__emailPredicate.evaluate(with: self.trimmingCharacters(in: .whitespaces))
//    }
    
    
    func htmlAttributedString() -> NSAttributedString? {
        
        let htmlTemplate = """
        <!doctype html>
        <html>
          <head>
            <style>
              body {
                font-family: -apple-system;
                font-size: 24px;
              }
            </style>
          </head>
          <body>
            \(self)
          </body>
        </html>
        """
        
        guard let data = htmlTemplate.data(using: .unicode) else {
            return nil
        }
        
        guard let attributedString = try? NSAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil
        ) else {
            return nil
        }
        
        return attributedString
    }
}
