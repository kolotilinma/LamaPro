//
//  DateFormatter+Extensions.swift
//  LamaPro
//
//  Created by Михаил on 03.04.2023.
//

import Foundation

extension DateFormatter {
    
    static func convertFrom(date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "ru_RU")
        
        let calendar = Calendar.autoupdatingCurrent
        
        if calendar.isDateInToday(date) {
            
            dateFormatter.dateFormat = "HH:mm"
            return "Сегодня, " + dateFormatter.string(from: date)
            
        } else if calendar.isDateInYesterday(date) {
            dateFormatter.dateFormat = "HH:mm"
            return "Вчера " + dateFormatter.string(from: date)
            
        } else {
            
            dateFormatter.dateFormat = "dd.MM.yyyy"
            return dateFormatter.string(from: date)
        }
    }
    
    static func convertDateFrom(string: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd-MM-yyyy'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: string) ?? Date()
        dateFormatter.dateFormat = "HH:mm"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
        
    static let serverDateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
        
        return formatter
    }()
    
    static var operation: DateFormatter {
        
        let formatter = DateFormatter()
        formatter.timeStyle = DateFormatter.Style.short//Set time style
        formatter.dateStyle = DateFormatter.Style.long //Set date style
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
  
        return formatter
    }

    static var shortDate: DateFormatter {

        let dateFormatter = DateFormatter()

        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateStyle = DateFormatter.Style.long

        dateFormatter.dateFormat =  "dd.MM.yy"
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "ru_RU")

        return dateFormatter
    }
    
    
    static let minutsAndSecond: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        return formatter
    }()
    
    static let dateWithoutTime: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "dd.MM.yyyy"
        
        return formatter
    }()
    
    
    static let dateAndTime: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
        
        return formatter
    }()
    
    static let dateAndMonth: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "d MMMM"
        
        return formatter
    }()
    
    static let timeAndSecond: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "HH:mm"
        
        return formatter
    }()
    
    static let historyShortDateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM, E"
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let historyFullDateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy"
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let historyDateAndTimeFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .medium
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let detailFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat =  "d MMMM yyyy"
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
    
    static let productPeriod: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d/MM"
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru_RU")
        
        return formatter
    }()
}
