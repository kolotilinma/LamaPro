//
//  Image+Extension.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI

extension Image {
    
    static var icXmark: Image { Image(systemName: "xmark") }
    static var icChevronLeft: Image { Image(systemName: "chevron.left") }
    
    static var ic24chevronRight: Image { Image(#function) }
    static var ic24Search: Image { Image(#function) }
    static var ic16Clear: Image { Image(#function) }
}
