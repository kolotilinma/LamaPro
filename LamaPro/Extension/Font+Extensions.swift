//
//  Font+Extensions.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI

extension Font {
    
    static func interBold(size: CGFloat) -> Font {
        Font.custom("Inter-Bold", size: size)
//        Font(UIFont.boldSystemFont(ofSize: 14))
    }
    static func interMedium(size: CGFloat) -> Font {
        Font.custom("Inter-Medium", size: size)
    }
    static func interRegular(size: CGFloat) -> Font {
        Font.custom("Inter-Regular", size: size)
    }
    static func interSemiBold(size: CGFloat) -> Font {
        Font.custom("Inter-SemiBold", size: size)
    }
}
