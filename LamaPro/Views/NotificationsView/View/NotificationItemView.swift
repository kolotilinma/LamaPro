//
//  NotificationItemView.swift
//  LamaPro
//
//  Created by Михаил on 01.04.2023.
//

import SwiftUI

struct NotificationItemView: View {
    
    @ObservedObject var viewModel: NotificationItemViewModel
    
    var body: some View {
        
        VStack(spacing: 16) {
            
            HStack(alignment: .top) {
                
                viewModel.image
                    .resizable()
                    .frame(width: 15, height: 15)
                
                VStack(alignment: .leading, spacing: 6) {
                    
                    Text(viewModel.title)
                        .font(.interSemiBold(size: 14))
                        .foregroundColor(.lamaBlack)
                        .multilineTextAlignment(.leading)
                    
                    if let cartDescription = viewModel.cartDescription {
                        Text(cartDescription)
                            .font(.interRegular(size: 12))
                            .foregroundColor(.lamaBlack)
                            .multilineTextAlignment(.leading)
                    }
                    Text(viewModel.date)
                        .font(.interRegular(size: 12))
                        .foregroundColor(.lamaGrayDark)
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            
            Divider()
        }
        .padding(.horizontal, 16)
    }
}

struct NotificationItemView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        VStack {
            
            NotificationItemView(viewModel: .init(id: "1", title: "Статус вашего заказа № 258: Проверяется менеджером", cartDescription: "Бинт из мешковины 0,15м*100м,190 г/м2 2 шт, Агроткань застилочная 1,05 м × 100 м, 130 г/м2 Россия 3 шт, Сетка - теневка 35%, 2,08 м × 100 м, 37 г/м2 Чехия 4 шт", date: "25.12.2022", image: Image("checkmark")))
                
            NotificationItemView(viewModel: .init(id: "1", title: "Статус вашего заказа № 258: Проверяется менеджером", cartDescription: "Бинт из мешковины 0,15м*100м,190 г/м2 2 шт, Агроткань застилочная 1,05 м × 100 м, 130 г/м2 Россия 3 шт, Сетка - теневка 35%, 2,08 м × 100 м, 37 г/м2 Чехия 4 шт", date: "25.12.2022", image: Image("waitYellow")))
        }
        .padding(.vertical, 16)
        .previewLayout(.sizeThatFits)
    }
}
