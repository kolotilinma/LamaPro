//
//  NotificationItemViewModel.swift
//  LamaPro
//
//  Created by Михаил on 01.04.2023.
//

import SwiftUI

class NotificationItemViewModel: ObservableObject, Identifiable {
    
    let id: String
    let title: String
    let cartDescription: String?
    let date: String
    let image: Image
    
    internal init(id: String, title: String, cartDescription: String?, date: String, image: Image) {
        
        self.id = id
        self.title = title
        self.cartDescription = cartDescription
        self.date = date
        self.image = image
    }
    
    convenience init(order: OrderData) {
        
        let formatter = DateFormatter.dateWithoutTime
        self.init(id: order.id, title: "Статус вашего заказа № \(order.id): \(order.status.statusDescription)", cartDescription: order.basketDescription, date: formatter.string(from: order.orderDate), image: order.status.statusImage)
    }
    
    
}
