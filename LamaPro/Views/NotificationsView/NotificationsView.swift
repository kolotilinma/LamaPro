//
//  NotificationsView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

struct NotificationsView: View {
    
    @ObservedObject var viewModel: NotificationsViewModel
    
    var body: some View {
        
        ZStack {
            
            switch viewModel.status {
            case.empty:
                VStack {
                    Spacer()
                    
                    Image("EmptyNotification")
                        .resizable()
                        .frame(width: 234, height: 225)
                    
                    Spacer()
                }
            case .normal:
                
                ScrollView {
                    
                    VStack(spacing: 16) {
                        
                        ForEach(viewModel.orders) { order in
                            
                            NotificationItemView(viewModel: order)
                        }
                    }
                    .padding(.vertical, 20)
                }
            }
        }
        .task(viewModel.GetOrderList)
        .refreshable(action: viewModel.GetOrderList)
        .navigationBar(with: viewModel.navigationBar)
    }
}

struct NotificationsView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        NotificationsView(viewModel: .init(model: .emptyMock, status: .empty, navigationBar: .init(title: .text("Уведомления")), orders: [
            
            .init(id: "1", title: "Статус вашего заказа № 258: Проверяется менеджером", cartDescription: "Бинт из мешковины 0,15м*100м,190 г/м2 2 шт, Агроткань застилочная 1,05 м × 100 м, 130 г/м2 Россия 3 шт, Сетка - теневка 35%, 2,08 м × 100 м, 37 г/м2 Чехия 4 шт", date: "25.12.2022", image: Image("checkmark")),
            .init(id: "2", title: "Статус вашего заказа № 259: Проверяется менеджером", cartDescription: "Бинт из мешковины 0,15м*100м,190 г/м2 2 шт, Агроткань застилочная 1,05 м × 100 м, 130 г/м2 Россия 3 шт, Сетка - теневка 35%, 2,08 м × 100 м, 37 г/м2 Чехия 4 шт", date: "25.12.2022", image: Image("waitYellow"))
        ]))
    }
}
