//
//  NotificationsViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation
import Combine

class NotificationsViewModel: ObservableObject {
    
    @Published var navigationBar: NavigationBarView.ViewModel
    
    @Published var orders: [NotificationItemViewModel]
    
    @Published var status: Status
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, status: Status, navigationBar: NavigationBarView.ViewModel, orders: [NotificationItemViewModel]) {
        
        self.model = model
        self.status = status
        self.navigationBar = navigationBar
        self.orders = orders
    }
    
    convenience init(model: Model) {
        
        self.init(model: model, status: .empty, navigationBar: .init(title: .text("Уведомления")), orders: [])
        bind()
    }
    func bind() {
        
        model.myOrders
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] myOrders in
                if myOrders.isEmpty == false {
                    
                    status = .normal
                    orders = myOrders.map { NotificationItemViewModel(order: $0) }
                } else {
                    
                    status = .empty
                    orders = []
                }
            }
            .store(in: &bindings)
    }
    
    
    @Sendable
    func GetOrderList() async {
        
        do {
            try await model.handleGetOrderList()
            
        } catch {
            print(error)
        }
        
    }
    
    enum Status {
        
//        case loading
        case empty
//        case error
        case normal
    }
}
