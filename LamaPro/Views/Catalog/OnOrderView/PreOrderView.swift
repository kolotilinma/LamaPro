//
//  PreOrderView.swift
//  LamaPro
//
//  Created by Константин Савялов on 05.04.2023.
//
import SwiftUI

extension PreOrderView {
    
    final class ViewModel: ObservableObject {
        
        @Published var name: TextFieldView.ViewModel
        @Published var phone: TextFieldView.ViewModel
        
        internal init(name: TextFieldView.ViewModel, phone: TextFieldView.ViewModel) {
            
            self.name = name
            self.phone = phone
        }
        
        
        convenience init(user: UserData) {

            self.init(
                name: .init(text: user.name, placeholder: "Ваше имя"),
                phone: .init(text: user.phone, placeholder: "Номер телефона")
            )
        }
    }
    
}

struct PreOrderView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
            
            TextFieldView(viewModel: viewModel.name)
            
            TextFieldView(viewModel: viewModel.phone)
        }
    }
}

struct PreOrderView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        PreOrderView(viewModel: .init(name: .init(text: "", placeholder: "Ваше имя"),                           phone: .init(text: "", placeholder: "Номер телефона")))
    }
}

