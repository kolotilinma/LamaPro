//
//  OnOrderView.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI

struct OnOrderView: View {
    
    @ObservedObject var viewModel: OnOrderViewModel
    
    var body: some View {
        
        VStack(spacing: 16) {
            
            Text("Оставьте заявку \nна персональный заказ")
                .font(.interBold(size: 20))
                .foregroundColor(.lamaBlack)
                .multilineTextAlignment(.center)
            
            Text("Мы перезвоним в ближайшее время")
                .font(.interRegular(size: 16))
                .foregroundColor(.lamaBlack)
            
            if let perOrderView = viewModel.preOrderView {
                
                PreOrderView(viewModel: perOrderView)
                    .padding(.vertical, 20)
            }
            
            if let confirmButton = viewModel.confirmButton {
                
                ButtonSimpleView(viewModel: confirmButton)
                    .padding(.horizontal, 80)
            }
            
            Spacer()
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {

                if let link = viewModel.link  {

                    switch link {

                    case .success(let success):
                        SuccessView(viewModel: success)
                    }
                }
            }
            
            Button {
                let phone = "+74951222254"
                guard let url = URL(string: "telprompt://\(phone)"),
                      UIApplication.shared.canOpenURL(url) else {
                    return
                }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } label: {
                HStack {
                    Image(uiImage: UIImage(named: "PhoneIcon") ?? UIImage())
                    Text("+7 (495) 122 22 54")
                        .font(.interRegular(size: 16))
                        .foregroundColor(.lamaBlack)
                }
            }
            
        }
        .padding(18)
        .navigationBar(with: viewModel.navigationBar)
    }
}

struct OnOrderView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        NavigationView {
            
            OnOrderView(viewModel: .init(id: "", model: .emptyMock, confirmButton: .init(title: "Оформить заказ", height: 50, action: {}), preOrderView: .init(name: .init(text: "", placeholder: "Ваше имя"),                           phone: .init(text: "", placeholder: "Номер телефона")), navigationBar: .init(title: .text("Под заказ"))))
        }
    }
}
