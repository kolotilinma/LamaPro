//
//  OnOrderViewModel.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI
import Combine

class OnOrderViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigationBar: NavigationBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    let id: String
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    var confirmButton: ButtonSimpleView.ViewModel?
    var preOrderView: PreOrderView.ViewModel?
    
    internal init(id: String, model: Model, confirmButton: ButtonSimpleView.ViewModel?, preOrderView: PreOrderView.ViewModel?, navigationBar: NavigationBarView.ViewModel) {
        
        self.id = id
        self.model = model
        self.navigationBar = navigationBar
        self.confirmButton = confirmButton
        self.preOrderView = preOrderView
    }
    
    convenience init(model: Model, item: CatElement, backAction: @escaping () -> Void) {
        
        self.init(id: item.article, model: model, confirmButton: nil, preOrderView: nil, navigationBar: .init(title: .text("Под заказ")))
        
        self.navigationBar.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        
        self.confirmButton = ButtonSimpleView.ViewModel(title: "Оформить заказ", height: 50) { [weak self] in
            self?.action.send(ConfirmPreOrderAction.OrderTapped())
        }
        
        self.preOrderView = PreOrderView.ViewModel(name: .init(text: "", placeholder: "Ваше имя"),
                                                   phone: .init(text: "", placeholder: "Номер телефона"))
        
        bind()
    }
    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                
                case _ as ConfirmPreOrderAction.OpenConfirm:
                    link = .success(SuccessView.ViewModel(title: "Заказ успешно оформлен!", subTitle: "Скоро наш менеджер свяжется с Вами для уточнения деталей", action: { [weak self] in
                        self?.model.action.send(TabBarAction.CloseAll())
                    }))
                    
                    
                case _ as ConfirmPreOrderAction.OrderTapped:
                    
                    guard let name = preOrderView?.name.textField.text,
                          let phone = preOrderView?.phone.textField.text
                          
                    else {
                        
                        // alert заполните все поля
                        return
                    }
                    
                    Task {
                        do {
                            model.action.send(ModelAction.CoverAction.Spinner.Show())
                            try await model.handlePreOrder(name: name, article: id, phone: phone)
                            model.action.send(ModelAction.CoverAction.Spinner.Hide())
                            self.action.send(ConfirmPreOrderAction.OpenConfirm())
                            
                        } catch {
                            model.action.send(ModelAction.CoverAction.Spinner.Hide())
                        }
                    }
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    enum Link {
        
        case success(SuccessView.ViewModel)
    }
    
    enum ConfirmPreOrderAction {
        
        struct OpenConfirm: Action {}
        struct OrderTapped: Action {}
    }
}
