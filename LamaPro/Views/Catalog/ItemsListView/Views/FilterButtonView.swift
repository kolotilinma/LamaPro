//
//  FilterButtonView.swift
//  LamaPro
//
//  Created by Михаил on 09.04.2023.
//

import SwiftUI
import Combine

extension FilterButtonView {
    
    final class ViewModel: ObservableObject {
        
        
        @Published var actionSheetModel: ActionSheetModel?
        
        @Published var sortButton: SortButton?
        @Published var filterButton: FilterButton?
        
        let sortType: CurrentValueSubject<SortType, Never>
        
        private var bindings = Set<AnyCancellable>()
        
        internal init(sortType: SortType = .initial, filterButton: FilterButton?) {
            self.sortType = .init(sortType)
            self.sortButton = nil
            self.filterButton = filterButton
            
            bind()
        }
        
        func bind() {
            
            sortType
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] sortType in
                   
                    self.sortButton = .init(title: sortType.textDescription, image: sortType.icon, action: { [weak self] in
                        
                        self?.actionSheetModel = self?.createActionSheet()
                    })
                    
                }
                .store(in: &bindings)
        }
        
        func createActionSheet() -> ActionSheetModel {
            
            var buttons: [Alert.Button] = []
            
            buttons.append(Alert.Button.default(Text("Цене (убыванию)")) { [weak self] in
                self?.sortType.value = .priceDown
            })
            buttons.append(Alert.Button.default(Text("Цене (возростанию)")) { [weak self] in
                self?.sortType.value = .priceUp
            })
            
            buttons.append(Alert.Button.default(Text("Названию (убыванию)")) { [weak self] in
                self?.sortType.value = .nameDown
            })
            buttons.append(Alert.Button.default(Text("Названию (возростанию)")) { [weak self] in
                self?.sortType.value = .nameUp
            })
            
            buttons.append(Alert.Button.cancel(Text("Отмена")))
            
            return ActionSheetModel(title: "", buttons: buttons)
        }
        
        enum SortType {
            
            case initial
            case nameDown
            case nameUp
            case priceDown
            case priceUp
            
            var textDescription: String {
                
                switch self {
                    
                case .initial:
                    return "Сортировать по"
                    
                case .nameDown, .nameUp:
                    return "Сортировать по названию"
                    
                case .priceDown, .priceUp:
                    return "Сортировать по цене"
                }
            }
            
            var icon: Image? {
                switch self {
                    
                case .initial:
                    return nil
                    
                case .nameDown, .priceDown:
                    return Image("arrowDownSmall")
                    
                case .nameUp, .priceUp:
                    return Image("arrowUpSmall")
                }
            }
        }
        
        struct SortButton {
            
            let title: String
            let image: Image?
            
            let action: () -> Void
        }
        
        struct FilterButton {
            
            var image: Image = Image("ic24setting")
            let action: () -> Void
        }
    }
    
}

struct FilterButtonView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        HStack {
            
            if let sortButton = viewModel.sortButton {
                
                Button(action: sortButton.action) {
                    
                    HStack(spacing: 10) {
                        
                        Text(sortButton.title)
                            .font(.interSemiBold(size: 13))
                            .foregroundColor(.lamaBlack)
                        
                        if let icon = sortButton.image {
                            icon
                        }
//                        Image("arrowDownSmall")
                        
                    }
                    .padding(.vertical, 8)
                    .padding(.horizontal, 14)
                }
                .background(Color.lamaGrayLight)
                .cornerRadius(30)
            }
            Spacer()
            
            if let filterButton = viewModel.filterButton {
                
                Button(action: filterButton.action) {
                    filterButton.image
                }
            }
        }
        .actionSheet(item: $viewModel.actionSheetModel, content: { actionSheet in
            ActionSheet(title: Text(actionSheet.title), buttons: actionSheet.buttons)
        })
    }
}

struct FilterButtonView_Previews: PreviewProvider {
    static var previews: some View {
        FilterButtonView(viewModel: .init(sortType: .initial, filterButton: .init(action: {})))
            .padding()
    }
}
