//
//  ProductItemView.swift
//  LamaPro
//
//  Created by Михаил on 25.02.2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProductItemView: View {
    
    @ObservedObject var viewModel: ProductItemViewModel
    @State var tapped: Bool = false
    @State private var buttonTypeId = 0
    
    var body: some View {
        
        VStack(spacing: 0) {
            
            ZStack {
                
                Color.lamaWhite
                
                WebImage(url: viewModel.imageUrl)
                    .resizable()
                    .indicator(.activity)
                    .transition(.fade)
                    .scaledToFit()
                    .frame(alignment: .bottom)
                    .onTapGesture {
                        simulateTap()
                    }
            }
            .overlay(alignment: .topTrailing) {
                
                Button(action: viewModel.favoriteTapped) {
                    
                    ZStack {
                        
                        Circle()
                            .foregroundColor(.lamaGrayLight)
                            .frame(width: 24, height: 24)
                            .padding(8)
                        
                        Image("heartFav")
                            .renderingMode(.template)
                            .foregroundColor(.lamaGreenDark.opacity(viewModel.isFavorite ? 1 : 0.2))
                            .offset(y: 1)
                    }
                }
                .frame(width: 24, height: 24)
                .cornerRadius(12)
                .padding(8)
            }
            
            Rectangle()
                .foregroundColor(.lamaWhite)
                .overlay (alignment: .bottom) {
                    
                    ZStack {
                        
                        VStack(alignment: .leading, spacing: 8) {
                            
                            VStack(alignment: .leading, spacing: 4) {
                                
                                Text(viewModel.title)
                                    .font(.interBold(size: 12))
                                    .foregroundColor(.lamaBlack)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .lineLimit(2)
                                
                                Text(viewModel.code)
                                    .font(.interRegular(size: 10))
                                    .foregroundColor(.lamaGrayDark)
                                    .lineLimit(1)
                            }
                            
                            Spacer()
                                .frame(minHeight: 0)
                            
                        }
                        .onTapGesture {
                            simulateTap()
                        }
                        
                        VStack(alignment: .leading, spacing: 8) {
                            
                            VStack(alignment: .leading, spacing: 8) {
                                
                                Spacer()
                                    .frame(minHeight: 0)
                                
                                Text(viewModel.price)
                                    .font(.interBold(size: 14))
                                    .foregroundColor(.lamaBlack)
                                    .lineLimit(1)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                
                            }
                            .onTapGesture {
                                simulateTap()
                            }
                            
                            VStack {
                                
                                switch viewModel.buttonState {
                                    
                                case .initial:
                                    EmptyView()
                                        .frame(height: 30)
                                        .onAppear {
                                            buttonTypeId = 0
                                        }
                                        .animation(.spring(), value: buttonTypeId)
                                    
                                case .addButton(let addButton):
                                    ButtonSimpleView(viewModel: addButton)
                                        .onAppear {
                                            buttonTypeId = 1
                                        }
                                        .animation(.spring(), value: buttonTypeId)
                                    
                                case .countButton(let count):
                                    ToCartButtonView(viewModel: count)
                                        .onAppear {
                                            buttonTypeId = 2
                                        }
                                        .animation(.spring(), value: buttonTypeId)
                                }
                            }
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                            .padding(.horizontal, 10)
                            .padding(.bottom, 10)
                        }
                    }
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .padding(.horizontal, 10)
                    .padding(.top, 10)
                }
        }
        .cornerRadius(10, corners: .allCorners)
        .frame(width: 180, height: 300)
        .padding(.horizontal, 4)
        .padding(.vertical, 8)
        .shadow(color: .black.opacity(0.1), radius: 5)
        .blur(radius: tapped ? 1 : 0)
        .scaleEffect(tapped ? 0.9 : 1, anchor: .center)
        .animation(.spring(), value: tapped)
//        .onTapGesture {
//
//            simulateTap()
//        }
    }
    
    
    private func simulateTap() {
        
        tapped.toggle()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            
            tapped.toggle()
            viewModel.itemTapped()
        }
    }
}

struct ProductItemView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ProductItemView(viewModel: .init(model: .emptyMock, id: "1", title: "Бур садовый Бур садовый Бур садовый Бур садовый", code: "Артикул: Lt001272", price: "5 954.70 ₽", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/c1f/d1ubgz30h8crwjogrzdjr6nb9z86p1ls.jpg"), buttonState: .addButton(.init(title: "В корзину", backgroundColor: .lamaGreen,  action: {}))))
            .previewLayout(.sizeThatFits)
            .padding()
        //                    .frame(width: 180, height: 300)
        //            .previewLayout(.fixed(width: 140, height: 200))
    }
}
