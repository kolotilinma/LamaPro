//
//  ItemsListView.swift
//  LamaPro
//
//  Created by Михаил on 25.02.2023.
//

import SwiftUI

struct ItemsListView: View {
    
    @ObservedObject var viewModel: ItemsListViewModel
    
    var columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
    
    var body: some View {
        
        ZStack {
            
            ScrollView {
                
                VStack {
                    
                    SearchBarView(viewModel: viewModel.searchBar)
                    
                    FilterButtonView(viewModel: viewModel.filterBar)
                }
                .padding(.horizontal, 16)
                .padding(.top, 16)
                
                LazyVGrid(columns: columns) {
                    
                    ForEach(viewModel.items) { item in
                        
                        ProductItemView(viewModel: item)
                    }
                }
                .padding(.horizontal, 16)
            }
            .resignKeyboardOnDragGesture()
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                
                if let link = viewModel.link  {
                    
                    switch link {
                        
                    case .search(let search):
                        SearchItemsView(viewModel: search)
                        
                    case .onOrder(let onOrder):
                        OnOrderView(viewModel: onOrder)
                        
                    case .itemDetail(let model):
                        ProductDetailView(viewModel: model)
                        
                    case .filter(let filter):
                        FilterView(viewModel: filter)
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigationBar)
    }
}

struct ItemsListView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ItemsListView(viewModel: .init(model: .emptyMock, id: UUID().uuidString, items: [
            .init(model: .emptyMock, id: "1", title: "Бур садовый", code: "Артикул: Lt001272", price: "5 954.70 ₽", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/c1f/d1ubgz30h8crwjogrzdjr6nb9z86p1ls.jpg"), buttonState: .addButton(.init(title: "В корзину", action: { }))),
            .init(model: .emptyMock, id: "2", title: "Бур садовый", code: "Артикул: Lt001272", price: "5 954.70 ₽", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/e65/inn73l25j5w0mht8mxbpggo091sg047x.jpg"), buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark, action: {}))),
            .init(model: .emptyMock, id: "3", title: "Бур садовый", code: "Артикул: Lt001272", price: "5 954.70 ₽", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/16c/jqc2j2hzcvmgomq8yfnny97th9kf0z4p.jpg"), buttonState: .countButton(.init(itemsCount: 1, maxCount: 5)))
        ], navigationBar: .init(title: .text("Агроткань застилочная"))))
    }
}
