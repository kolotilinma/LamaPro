//
//  ItemsListViewModel.swift
//  LamaPro
//
//  Created by Михаил on 25.02.2023.
//

import SwiftUI
import Combine

class ItemsListViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigationBar: NavigationBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    let searchBar: SearchBarView.ViewModel
    let filterBar: FilterButtonView.ViewModel
    @Published var items: [ProductItemViewModel]
    
    private let filteredData: CurrentValueSubject<[CatElement], Never> = .init([])
    private let itemsData: CurrentValueSubject<[CatElement], Never> = .init([])
    
    let id: String
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, id: String, items: [ProductItemViewModel], navigationBar: NavigationBarView.ViewModel) {
        
        self.navigationBar = navigationBar
        self.searchBar = .init()
        self.filterBar = .init(filterButton: nil)
        self.id = id
        self.items = items
        self.model = model
        self.filterBar.filterButton = .init(action: { [weak self] in
            self?.action.send(ItemsListViewModelAction.OpenFilter())
        })
    }
    
    convenience init(model: Model, item: CatSection, backAction: @escaping () -> Void) {
        
        let filtered = model.catalogElements.value.filter({ $0.iblockSectionId == item.id })
        
        self.init(model: model, id: item.id, items: [], navigationBar: .init(title: .text(item.name)))
        self.navigationBar.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        self.itemsData.value = filtered
        bind()
    }
    
    convenience init(model: Model, newItems: [CatElement], backAction: @escaping () -> Void) {
        
        self.init(model: model, id: "New", items: [], navigationBar: .init(title: .text("Новинки")))
        self.navigationBar.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        self.itemsData.value = newItems
        bind()
    }
    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case _ as ItemsListViewModelAction.OpenFilter:
                    let filter = FilterViewModel(items: itemsData.value, backAction: { [weak self] in
                        self?.link = nil
                    })
                    link = .filter(filter)
                    bind(filter)
                    
                case let payload as ItemsListViewModelAction.OpenOnOrder:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                    link = .onOrder(OnOrderViewModel(model: model, item: item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                case let payload as ItemsListViewModelAction.OpenItemDetail:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                    
                    link = .itemDetail(ProductDetailViewModel(model: model, item: item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        itemsData
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] items in
                filteredData.value = items
            }
            .store(in: &bindings)
        
        filteredData
            .combineLatest(filterBar.sortType)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] items, sortType in
                withAnimation {
                    
                    switch sortType {
                        
                    case .initial:
                        let itemsModel: [ProductItemViewModel] = items.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .nameDown:
                        
                        let filtered = items.sorted(by: { $0.name < $1.name })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .nameUp:
                        
                        let filtered = items.sorted(by: { $0.name > $1.name })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .priceDown:
                        let filtered = items.sorted(by: { Double($0.price?.price ?? "") ?? 0 > Double($1.price?.price ?? "") ?? 0 })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .priceUp:
                        let filtered = items.sorted(by: { Double($0.price?.price ?? "") ?? 0 < Double($1.price?.price ?? "") ?? 0 })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                    }
                }
            }
            .store(in: &bindings)
        
        searchBar.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as SearchBarViewAction.SearchText:
                    link = .search(SearchItemsViewModel(model, text: payload.text, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    func bind(_ items: [ProductItemViewModel]) {
        for item in items {
            
            item.action
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] action in
                    switch action {
                     
                    case let payload as ProductItemViewModelAction.OnOrderTapped:
                        self.action.send(ItemsListViewModelAction.OpenOnOrder(id: payload.id))
                        
                    case let payload as ProductItemViewModelAction.ItmemTapped:
                        self.action.send(ItemsListViewModelAction.OpenItemDetail(id: payload.id))
                        
                        
                    case let payload as ProductItemViewModelAction.FavoriteItmemTapped:
                        
                        self.model.action.send(ModelAction.Favorites.Update(id: payload.id))
                        
                    default: break
                    }
                }
                .store(in: &bindings)
            
        }
    }
    
    func bind(_ filter: FilterViewModel) {
        
        filter.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case let payload as FilterViewModelAction.ShowFiltered:
                    filteredData.value = payload.filtered
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    enum Link {
        
        case search(SearchItemsViewModel)
        case itemDetail(ProductDetailViewModel)
        case onOrder(OnOrderViewModel)
        case filter(FilterViewModel)
    }
    
}

enum ItemsListViewModelAction {
    
    struct OpenItemDetail: Action {
        let id: String
    }
    
    struct OpenOnOrder: Action {
        let id: String
    }
    
    struct OpenFilter: Action { }
}
