//
//  CatalogView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

struct CatalogView: View {
    
    @ObservedObject var viewModel: CatalogViewModel
    
    var body: some View {
        
        ZStack {
            
            ScrollView {
                
                switch viewModel.catalogState {
                    
                case .done:
                    
                    LazyVStack {
                        
                        SearchBarView(viewModel: viewModel.searchBar)
                            .padding(.horizontal, 16)
                            .padding(.top, 16)
                        
                        switch viewModel.searchState {
                            
                        case.normal:
                            
                            ForEach(viewModel.items) { item in
                                
                                CatalogCellItemView(viewModel: item)
                            }
                            
                        case .isSearch(let search):
                            
                            SearchItemsView(viewModel: search)
                        }
                    }
                    
                case .loading:
                    VStack {
                        
                        Text("Подождите, идет загрузка каталога...")
                            .font(.interBold(size: 13))
                            .foregroundColor(.lamaBlack)
                        
                        AnimatedHUDView()
                            .frame(width: 40, height: 40)
                        
                    }
                    .padding(.top, UIScreen.main.bounds.height/2 - 120)
                }
            }.refreshable(action: viewModel.refreshCatalog)
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                
                if let link = viewModel.link  {
                    
                    switch link {
                        
                    case .search(let search):
                        SearchItemsView(viewModel: search)
                        
                    case .itemsList(let model):
                        ItemsListView(viewModel: model)
                        
                    case .catalogSections(let model):
                        CatalogSectionsListView(viewModel: model)
                        
                    case .itemDetail(let model):
                        ProductDetailView(viewModel: model)
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigation)
    }
}

struct CatalogView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CatalogView(viewModel: .init(model: .emptyMock, items: [
            .init(id: "1", title: "Агротекстиль", action: { _ in }),
            .init(id: "2", title: "Вспомогательное оборудование", action: { _ in }),
            .init(id: "3", title: "Газонные травы и сидераты", action: { _ in })
        ], navigation: .init(title: .text("Каталог")), catalogState: .done, searchState: .normal))
    }
}
