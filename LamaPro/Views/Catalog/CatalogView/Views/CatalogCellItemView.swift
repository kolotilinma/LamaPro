//
//  CatalogCellItemView.swift
//  LamaPro
//
//  Created by Михаил on 22.02.2023.
//

import SwiftUI
import Kingfisher
import SVGView
import SDWebImageSwiftUI

struct CatalogCellItemView: View {
    
    @ObservedObject var viewModel: CatalogCellItemViewModel
    
    var body: some View {
        
        VStack() {
            
            Button {
                
                viewModel.action(viewModel.id)
                
            } label: {
                
                HStack(alignment: .center, spacing: 5) {
                    
                    if let url = viewModel.iconUrl {
                        
                        WebImage(url: url)
                            .resizable()
                            .indicator(.activity)
                            .transition(.fade)
                            .scaledToFit()
                            .frame(width: 30, height: 30, alignment: .center)
                    }
                    
                    Text(viewModel.title)
                        .font(.interSemiBold(size: 13))
                        .foregroundColor(.lamaBlack)
                        .multilineTextAlignment(.leading)
                    
                    
                    Spacer()
                    
                    Image.ic24chevronRight
                        .resizable()
                        .frame(width: 24, height: 24)
                    
                }
                .padding(.vertical, 4)
                .background(Color.white)
                .frame(maxWidth: .infinity)
            }
            .frame(maxWidth: .infinity)
            .background(Color.white)
            
            Divider()
        }
        .padding(.horizontal, 16)
    }
}

struct CatalogCellItemView_Previews: PreviewProvider {
    static var previews: some View {
        CatalogCellItemView(viewModel: .init(id: "1", title: "Вспомогательное оборудование", action: { _ in }))
            .previewLayout(.sizeThatFits)
//            .padding()
    }
}
