//
//  CatalogCellItemViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.02.2023.
//

import Foundation

class CatalogCellItemViewModel: ObservableObject, Identifiable {
    
    let id: String
    let title: String
    let iconUrl: URL?
    
    let action: (CatalogCellItemViewModel.ID) -> Void
    
    internal init(id: String, title: String, stringUrl: String? = nil, action: @escaping (CatalogCellItemViewModel.ID) -> Void) {
        
        self.id = id
        self.title = title
        
        if let iconUrl = stringUrl {
            
            self.iconUrl = URL(string: iconUrl)
            
        } else {
            
            self.iconUrl = nil
        }
        self.action = action
    }
    
    convenience init(host: String, catalog: CatSection, action: @escaping (CatalogCellItemViewModel.ID) -> Void) {
        
        self.init(id: catalog.id, title: catalog.name, stringUrl: host + catalog.previewPicture, action: action)
    }
    
}
