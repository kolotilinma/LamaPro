//
//  CatalogViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI
import Combine

class CatalogViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigation: NavigationBarView.ViewModel
    let searchBar: SearchBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    @Published var catalogState: CatalogState
    @Published var searchState: SearchState
    @Published var items: [CatalogCellItemViewModel]
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    
    internal init(model: Model, items: [CatalogCellItemViewModel], navigation: NavigationBarView.ViewModel, catalogState: CatalogState = .loading, searchState: SearchState = .normal) {
        
        self.model = model
        self.items = items
        self.navigation = navigation
        self.searchBar = .init()
        self.searchState = searchState
        self.catalogState = catalogState
    }
    
    convenience init(model: Model) {
        
        self.init(model: model, items: [], navigation: .init(title: .text("Каталог")))
        
        bind()
    }
    
    func bind() {
        
        model.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as TabBarAction.CloseAll:
                    link = nil
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case let payload as CatalogViewModelAction.ItemTapped:
                    
                    if model.catalogSections.value.first(where: { $0.iblockSectionId == payload.item.id }) != nil {
                        
                        link = .catalogSections(CatalogSectionsListViewModel(model: model, item: payload.item, backAction: { [weak self] in
                            self?.link = nil
                        }))
                        
                    } else {
                        
                        link = .itemsList(ItemsListViewModel(model: model, item: payload.item, backAction: { [weak self] in
                            self?.link = nil
                        }))
                    }
                    
                case let payload as CatalogViewModelAction.OpenItemDetail:
                    link = .itemDetail(ProductDetailViewModel(model: model, item: payload.item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        model.catalogSections
            .combineLatest(model.catalogElements)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] sections, elements in
                
                guard sections.isEmpty == false, elements.isEmpty == false else { return }
                
                let filtered = sections.filter({ $0.iblockSectionId == nil })
                items = filtered.map({ CatalogCellItemViewModel(host: model.host, catalog: $0, action: { [weak self] id in
                    
                    guard let item = filtered.first(where: { $0.id == id }) else { return }
                    self?.action.send(CatalogViewModelAction.ItemTapped(item: item))
                    
                }) })
                catalogState = .done
            }
            .store(in: &bindings)
        
        searchBar.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as SearchBarViewAction.SearchText:
                    link = .search(SearchItemsViewModel(model, text: payload.text, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
        
//        searchBar.$searchText
//            .removeDuplicates()
//            .receive(on: DispatchQueue.main)
//            .sink { [unowned self] text in
//                let isEditing = searchBar.isEditing
//
//                withAnimation {
//
//                    if isEditing, text != "" {
//
//                        let filtered = model.catalogElements.value.filter({ $0.name.lowercased().contains(text.lowercased()) || $0.article.lowercased().contains(text.lowercased()) })
//                        let searchModel = SearchItemsViewModel(model, items: filtered)
//                        bind(searchModel)
//                        searchState = .isSearch(searchModel)
//
//                    } else {
//
//                        searchState = .normal
//                    }
//                }
//
//            }
//            .store(in: &bindings)
        
    }
    
    func bind(_ searchModel: SearchItemsViewModel) {
        searchModel.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                
                case let payload as SearchItemsViewModelAction.OnOrderTapped:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id }) else { return }
                    self.action.send( CatalogViewModelAction.OpenItemDetail(item: item))
                    
                case let payload as SearchItemsViewModelAction.OpenItemDetail:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id }) else { return }
                    self.action.send( CatalogViewModelAction.OpenItemDetail(item: item))
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    @Sendable func refreshCatalog() async {
        
        await model.fetchCatalogData()
    }
    
    enum CatalogState {
        
        case loading
        case done
    }
    
    enum SearchState {
        
        case isSearch(SearchItemsViewModel)
        case normal
    }
    
    enum Link {
        
        case catalogSections(CatalogSectionsListViewModel)
        case itemsList(ItemsListViewModel)
        case itemDetail(ProductDetailViewModel)
        case search(SearchItemsViewModel)
    }
    
}

enum CatalogViewModelAction {
    
    struct ItemTapped: Action {
        
        let item: CatSection
    }
    
    struct OpenItemDetail: Action {
        
        let item: CatElement
    }
    
}
