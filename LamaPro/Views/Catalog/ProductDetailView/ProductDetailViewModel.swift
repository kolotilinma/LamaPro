//
//  ProductDetailViewModel.swift
//  LamaPro
//
//  Created by Михаил on 26.02.2023.
//

import SwiftUI
import Combine

class ProductDetailViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigationBar: NavigationBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    let title: String
    let article: String
    let manufacturer: String?
    let minPartia: String?
    let description: String
    let imageUrl: URL?
    
    @Published var price: String?
    @Published var countButton: ToCartButtonView.ViewModel
    @Published var cartButton: ButtonSimpleView.ViewModel?
    
    let id: String
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, id: String, title: String, article: String, manufacturer: String?, minPartia: String?, description: String, price: String, imageUrl: URL?, navigationBar: NavigationBarView.ViewModel, countButton: ToCartButtonView.ViewModel, cartButton: ButtonSimpleView.ViewModel?) {
        
        self.model = model
        self.id = id
        self.title = title
        self.article = article
        self.manufacturer = manufacturer
        self.minPartia = minPartia
        self.description = description
        self.imageUrl = imageUrl
        self.price = price
        self.navigationBar = navigationBar
        self.countButton = countButton
        self.cartButton = cartButton
    }
    
    convenience init(model: Model, item: CatElement, backAction: @escaping () -> Void) {
        
        let count = model.cartElements.value.items.first(where: { $0.elementId == item.id })?.count ?? 1
        
        let countButton = ToCartButtonView.ViewModel(itemsCount: count, maxCount: item.quantity)
        
        self.init(model: model, id: item.id, title: item.name, article: "Артикул: \(item.article)", manufacturer: item.manufacturer, minPartia: item.minPartia, description: item.detailText?.htmlAttributedString()?.string ?? "", price: item.price?.priceString ?? "", imageUrl: URL(string: model.host + (item.detailPicture)), navigationBar: .init(title: .text(item.name)), countButton: countButton, cartButton: nil)
        
        self.navigationBar.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        bind(countButton)
        bind()
    }
    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case let payload as ProductDetailViewModelAction.OpenOnOrder:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                    link = .onOrder(OnOrderViewModel(model: model, item: item, backAction: {[weak self] in
                        self?.link = nil
                    }))
                    
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        model.cartElements
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] cartElements in
                
                guard let item = model.catalogElements.value.first(where: { $0.id == id }) else { return }
                
                if let count = cartElements.items.first(where: { $0.elementId == id })?.count {
                    
                    self.cartButton = createButton(item: item, count: count)
                    
                } else {
                    
                    self.cartButton = createButton(item: item, count: 0)
                }
                
            }
            .store(in: &bindings)
        
        
    }
    
    func bind(_ countButton: ToCartButtonView.ViewModel) {
        
        countButton.$itemsCount
            .dropFirst()
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] itemsCount in
                guard let item = model.catalogElements.value.first(where: { $0.id == id }) else { return }
                
                model.action.send(ModelAction.Cart.Update(id: item.id, count: itemsCount))
                
                if itemsCount == 0 {
                    
                    price = item.setupPrice(itemsCount: 1)

                } else {
                    
                    price = item.setupPrice(itemsCount: itemsCount)
                }
            }
            .store(in: &bindings)
    }
    
    func createButton(item: CatElement, count: Int) -> ButtonSimpleView.ViewModel {
        
        
        if item.quantity == 0 {
            
            return ButtonSimpleView.ViewModel(title: "Под заказ", backgroundColor: .lamaGreenDark, action: { [weak self] in
                
                self?.action.send(ProductDetailViewModelAction.OpenOnOrder(id: item.id))
            })
            
        } else {
            
            if count != 0 {
                
                return ButtonSimpleView.ViewModel(title: "В корзине", icon: Image(systemName: "checkmark"), backgroundColor: .lamaGreen, action: { [weak self] in
                    
                    guard let count = self?.countButton.itemsCount else { return }
                    self?.model.action.send(ModelAction.Cart.Update(id: item.id, count: count))
                    
                })
                
            } else {
                
                return ButtonSimpleView.ViewModel(title: "В корзину", backgroundColor: .lamaGreen, action: { [weak self] in
                    
                    guard let count = self?.countButton.itemsCount else { return }
                    self?.model.action.send(ModelAction.Cart.Update(id: item.id, count: count))
                })
            }
        }
        
    }
    
    enum Link {
        
        case onOrder(OnOrderViewModel)
    }
}

enum ProductDetailViewModelAction {
    
    struct OpenOnOrder: Action {
        let id: String
    }
}
