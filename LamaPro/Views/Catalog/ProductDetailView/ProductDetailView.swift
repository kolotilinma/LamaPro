//
//  ProductDetailView.swift
//  LamaPro
//
//  Created by Михаил on 26.02.2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProductDetailView: View {
    
    @ObservedObject var viewModel: ProductDetailViewModel
    
    var body: some View {
        
        ZStack {
            
            ScrollView {
                
                VStack(alignment: .leading, spacing: 20) {
                    
                    HStack {
                        
                        Spacer()
                        
                        WebImage(url: viewModel.imageUrl)
                            .resizable()
                            .indicator(.activity)
                            .transition(.fade)
                            .scaledToFit()
                            .frame(height: 230, alignment: .center)
                        
                        Spacer()
                    }
                    
                    VStack(alignment: .leading, spacing: 8) {
                        
                        Text(viewModel.title)
                            .font(.interBold(size: 20))
                            .foregroundColor(.lamaBlack)
                        
                        Text(viewModel.article)
                            .font(.interRegular(size: 13))
                            .foregroundColor(.lamaGrayDark)
                    }
                    
                    VStack(alignment: .leading, spacing: 10) {
                        
                        if let manufacturer = viewModel.manufacturer {
                            
                            HStack(spacing: 6) {
                                
                                Circle()
                                    .frame(width: 8)
                                    .foregroundColor(.lamaGreen)
                                
                                Text("Производитель:")
                                    .font(.interBold(size: 13))
                                    .foregroundColor(.lamaBlack)
                                
                                Text(manufacturer)
                                    .font(.interRegular(size: 13))
                                    .foregroundColor(.lamaBlack)
                                
                                Spacer()
                            }
                        }
                        
                        if let minPartia = viewModel.minPartia {
                            
                            HStack(spacing: 6) {
                                
                                Circle()
                                    .frame(width: 8)
                                    .foregroundColor(.lamaGreen)
                                
                                Text("Минимальная партия:")
                                    .font(.interBold(size: 13))
                                    .foregroundColor(.lamaBlack)
                                
                                Text(minPartia)
                                    .font(.interRegular(size: 13))
                                    .foregroundColor(.lamaBlack)
                            }
                        }
                       
                    }
                    
                    VStack(alignment: .leading, spacing: 8) {
                        
                        Text("Описание товара")
                            .font(.interBold(size: 15))
                            .foregroundColor(.lamaBlack)
                        
                        Text(viewModel.description)
                            .font(.interRegular(size: 13))
                            .foregroundColor(.lamaBlack)
                    }
                    .multilineTextAlignment(.leading)
                }
                .padding(.horizontal, 16)
                
            }
            .padding(.bottom, 136)
            .resignKeyboardOnDragGesture()
            
            VStack {
                
                Spacer()
                
                VStack(spacing: 14) {
                    
                    VStack(spacing: 19) {
                        
                        HStack {
                            
                            Text("Цена")
                                .font(.interBold(size: 15))
                                .foregroundColor(.lamaBlack)
                            
                            Spacer()
                            
                            if let price = viewModel.price {
                                
                                Text(price)
                                    .font(.interBold(size: 15))
                                    .foregroundColor(.lamaBlack)
                            }
                        }
                        
                        Divider()
                        
                    }
                    
                    HStack {
                        
//                        if let count = viewModel.countButton {
                            
                            ToCartButtonView(viewModel: viewModel.countButton)
//                        }
                        
                        if let button = viewModel.cartButton {
                            
                            ButtonSimpleView(viewModel: button)
                        }
                    }
                    
                }
                .padding(.horizontal, 16)
                .padding(.vertical, 19)
                .background(Color.lamaWhite)
            }
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                
                if let link = viewModel.link  {
                    
                    switch link {
                        
                    case .onOrder(let onOrder):
                        OnOrderView(viewModel: onOrder)
                     
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigationBar)
    }
}

struct ProductDetailView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ProductDetailView(viewModel: .init(
            model: .emptyMock,
            id: UUID().uuidString,
            title: "Агроткань застилочная",
//            title: "Агроткань застилочная 1,05 м × 100 м, 130 г/м2 Чехия",
            article: "Артикул: Lt001272",
            manufacturer: "company JUTA a.s.",
            minPartia: "1",
//            description: "Практичный, прочный, многолетний материал из полипропиленовых волокон для использования в растениеводстве, садоводстве, под открытым небом, в теплицах и для защиты от грязи.Не пропускает солнечный свет, опрепятствует прорастанию сорняков; снижает испарение; пропускает воду и воздух.Устойчив к плесени, бактериям и УФ – излучению.",
            description: "Практичный, прочный.",
            price: "7 243.20 ₽",
            imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/c1f/d1ubgz30h8crwjogrzdjr6nb9z86p1ls.jpg"),
            navigationBar: .init(title: .text("Агроткань застилочная")), countButton: .init(itemsCount: 2, maxCount: 7),
            cartButton: .init(title: "В корзину", backgroundColor: .lamaGreen, action: {})
        ))
    }
}
