//
//  CatalogSectionsListViewModel.swift
//  LamaPro
//
//  Created by Михаил on 25.02.2023.
//

import SwiftUI
import Combine

class CatalogSectionsListViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigationBar: NavigationBarView.ViewModel
    let searchBar: SearchBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    let sections: [CatalogSectionViewModel]
    
    let id: String
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, id: String, sections: [CatalogSectionViewModel], navigationBar: NavigationBarView.ViewModel) {
        
        self.model = model
        self.id = id
        self.sections = sections
        self.navigationBar = navigationBar
        self.searchBar = .init()
    }
    
    convenience init(model: Model, item: CatSection, backAction: @escaping () -> Void) {
        
        let filtered = model.catalogSections.value.filter({ $0.iblockSectionId == item.id })
        let sections = filtered.map { CatalogSectionViewModel(model: model, item: $0) }
        
        self.init(model: model, id: item.id, sections: sections, navigationBar: .init(title: .text(item.name)))
        self.navigationBar.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        bind()
//        let sections = createSections(from: item)
        bind(sections)
//        self.sections = sections
    }
    
    func bind() {
        
        searchBar.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as SearchBarViewAction.SearchText:
                    link = .search(SearchItemsViewModel(model, text: payload.text, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    func bind(_ sections: [CatalogSectionViewModel]) {
        
        for section in sections {
            
            section.action
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] action in
                    switch action {
                     
                    case let payload as CatalogSectionViewModelAction.OpenOnOrder:
                        guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                        link = .onOrder(OnOrderViewModel(model: model, item: item, backAction: {[weak self] in
                            self?.link = nil
                        }))
                        
                        
                    case let payload as CatalogSectionViewModelAction.OpenItemDetail:
                        guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                        
                        link = .itemDetail(ProductDetailViewModel(model: model, item: item, backAction: { [weak self] in
                            self?.link = nil
                        }))
                        
                    case let payload as CatalogSectionViewModelAction.OpenAllElements:
                        
                        guard let item = model.catalogSections.value.first(where: { $0.id == payload.id}) else { return }
                        
                        if model.catalogSections.value.first(where: { $0.iblockSectionId == payload.id }) != nil {
                            
                            link = .catalogSections(CatalogSectionsListViewModel(model: model, item: item, backAction: { [weak self] in
                                self?.link = nil
                            }))
                            
                        } else {
                            
                            link = .itemsList(ItemsListViewModel(model: model, item: item, backAction: { [weak self] in
                                self?.link = nil
                            }))
                        }
                        
                    default: break
                    }
                }
                .store(in: &bindings)
        }
        
    }
    
//    func createSections(from item: CatSection) -> [CatalogSectionViewModel] {
//
//        let filtered = model.catalogSections.value.filter({ $0.iblockSectionId == item.id })
//        return filtered.map { CatalogSectionViewModel(model: model, item: $0) }
//    }
    
    enum Link {
        
        case search(SearchItemsViewModel)
        case itemDetail(ProductDetailViewModel)
        case catalogSections(CatalogSectionsListViewModel)
        case itemsList(ItemsListViewModel)
        case onOrder(OnOrderViewModel)
    }
    
}
