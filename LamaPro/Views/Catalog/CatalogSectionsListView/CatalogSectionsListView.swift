//
//  CatalogSectionsListView.swift
//  LamaPro
//
//  Created by Михаил on 25.02.2023.
//

import SwiftUI

struct CatalogSectionsListView: View {
    
    @ObservedObject var viewModel: CatalogSectionsListViewModel
    
    var body: some View {
        
        ZStack {
            
            ScrollView {
                
                SearchBarView(viewModel: viewModel.searchBar)
                    .padding(.horizontal, 16)
                    .padding(.top, 16)
                
                VStack(spacing: 16) {
                    
                    ForEach(viewModel.sections) { section in
                        
                        CatalogSectionView(viewModel: section)
                    }
                }
            }
            .resignKeyboardOnDragGesture()
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                
                if let link = viewModel.link  {
                    
                    switch link {
                        
                    case .search(let search):
                        SearchItemsView(viewModel: search)
                        
                    case .onOrder(let onOrder):
                        OnOrderView(viewModel: onOrder)
                        
                    case .itemDetail(let model):
                        ProductDetailView(viewModel: model)
                        
                    case .itemsList(let model):
                        ItemsListView(viewModel: model)
                        
                    case .catalogSections(let model):
                        CatalogSectionsListView(viewModel: model)
                        
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigationBar)
        
    }
}

struct CatalogSectionsListView_Previews: PreviewProvider {
    static var previews: some View {
        CatalogSectionsListView(viewModel: .init(
            model: .emptyMock, id: UUID().uuidString,
            sections: [
                .init(model: .emptyMock, id: "1", items: [
                    .init(model: .emptyMock, id: "1", title: "Бур садовый", code: "Артикул: Lt001111", price: "5 554.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
                    .init(model: .emptyMock, id: "2", title: "Бур дачный", code: "Артикул: Lt001222", price: "5 154.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
                    .init(model: .emptyMock, id: "3", title: "Бур дворовый", code: "Артикул: Lt001333", price: "5 954.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {})))
                ], title: "Агроткань застилочная"),
                .init(model: .emptyMock, id: "2", items: [
                    .init(model: .emptyMock, id: "1", title: "Бур садовый", code: "Артикул: Lt001111", price: "5 554.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
                    .init(model: .emptyMock, id: "2", title: "Бур дачный", code: "Артикул: Lt001222", price: "5 154.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
                    .init(model: .emptyMock, id: "3", title: "Бур дворовый", code: "Артикул: Lt001333", price: "5 954.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {})))
                ], title: "Мешковина"),
                .init(model: .emptyMock, id: "3", items: [
                    .init(model: .emptyMock, id: "1", title: "Бур садовый", code: "Артикул: Lt001111", price: "5 554.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
                    .init(model: .emptyMock, id: "2", title: "Бур дачный", code: "Артикул: Lt001222", price: "5 154.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
                    .init(model: .emptyMock, id: "3", title: "Бур дворовый", code: "Артикул: Lt001333", price: "5 954.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {})))
                ], title: "Сетка для затенения")
            ],
            navigationBar: .init(title: .text("Агротекстиль"))))
    }
}
