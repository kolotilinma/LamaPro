//
//  CatalogSectionView.swift
//  LamaPro
//
//  Created by Михаил on 25.02.2023.
//

import SwiftUI

struct CatalogSectionView: View {
    
    @ObservedObject var viewModel: CatalogSectionViewModel
    
//    let screen = UIScreen.main.bounds
    
    var body: some View {
        
        VStack(spacing: 8) {
            
            HStack(alignment: .top) {
                
                Text(viewModel.title)
                    .font(.interBold(size: 20))
                    .foregroundColor(.lamaBlack)
                
                Spacer()
                
                Button(action: viewModel.openAllElements) {
                   
                    HStack(spacing: 0) {
                        
                        Text("Больше")
                            .font(.interSemiBold(size: 13))
                            .foregroundColor(.lamaBlack)
                        
                        Image.ic24chevronRight
                            .resizable()
                            .frame(width: 24, height: 24)
                    }
                }
            }
            .padding(.leading, 16)
            .padding(.trailing, 7)
            
            ScrollView(.horizontal, showsIndicators: false) {
                
                HStack(spacing: 8) {
                    
                    ForEach(viewModel.items) { item in
                        
                        ProductItemView(viewModel: item)
//                            .frame(width: 140)
//                            .padding(.horizontal, 4)
//                            .padding(.vertical, 8)
                    }
                    
                }
                .padding(.horizontal, 6)
            }
        }
    }
}

struct CatalogSectionView_Previews: PreviewProvider {
    static var previews: some View {
        CatalogSectionView(viewModel: .init(model: .emptyMock, id: "1", items: [
            .init(model: .emptyMock, id: "1", title: "Бур садовый", code: "Артикул: Lt001111", price: "5 554.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
            .init(model: .emptyMock, id: "2", title: "Бур дачный", code: "Артикул: Lt001222", price: "5 154.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
            .init(model: .emptyMock, id: "3", title: "Бур дворовый", code: "Артикул: Lt001333", price: "5 954.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {})))
        ], title: "Агроткань застилочная"))
    }
}
