//
//  CatalogSectionViewModel.swift
//  LamaPro
//
//  Created by Михаил on 25.02.2023.
//

import Foundation
import Combine

class CatalogSectionViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    let items: [ProductItemViewModel]
    
    let id: String
    let title: String
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, id: String, items: [ProductItemViewModel], title: String) {
        
        self.model = model
        self.id = id
        self.items = items
        self.title = title
    }
    
    convenience init(model: Model, item: CatSection) {
        
        let filtered = model.catalogElements.value.filter({ $0.iblockSectionId == item.id })//.prefix(5)
        
        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
        
        self.init(model: model, id: item.id, items: itemsModel, title: item.name)

//        self.items = itemsModel
        bind(itemsModel)
    }
    
    func bind(_ items: [ProductItemViewModel]) {
        for item in items {
            
            item.action
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] action in
                    switch action {
                     
                    case let payload as ProductItemViewModelAction.OnOrderTapped:
                        self.action.send(CatalogSectionViewModelAction.OpenOnOrder(id: payload.id))
                        
                    case let payload as ProductItemViewModelAction.ItmemTapped:
                        self.action.send(CatalogSectionViewModelAction.OpenItemDetail(id: payload.id))
                        
                        
                    case let payload as ProductItemViewModelAction.FavoriteItmemTapped:
                        model.action.send(ModelAction.Favorites.Update(id: payload.id))
                        
                    default: break
                    }
                }
                .store(in: &bindings)
            
        }
    }
    
    func createItems(from item: CatSection) -> [ProductItemViewModel] {
        
        let filtered = model.catalogElements.value.filter({ $0.iblockSectionId == item.id })//.prefix(5)
        
        return filtered.map { ProductItemViewModel(model: model, item: $0) }
    }
    
    func openAllElements() {
        
        action.send(CatalogSectionViewModelAction.OpenAllElements(id: id))
    }
    
}

enum CatalogSectionViewModelAction {
    
    struct OpenAllElements: Action {
        let id: String
    }
    
    struct OpenItemDetail: Action {
        let id: String
    }
    
    struct OpenOnOrder: Action {
        let id: String
    }
}
