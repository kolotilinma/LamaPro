//
//  SearchItemsViewModel.swift
//  LamaPro
//
//  Created by Михаил on 07.03.2023.
//

import SwiftUI
import Combine

class SearchItemsViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    let searchBar: SearchBarView.ViewModel
    let filterBar: FilterButtonView.ViewModel
    
    @Published var navigation: NavigationBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    @Published var textResult: String
    @Published var items: [ProductItemViewModel]
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    private let searchText: CurrentValueSubject<String, Never> = .init("")
    private let filteredData: CurrentValueSubject<[CatElement], Never> = .init([])
    private let itemsData: CurrentValueSubject<[CatElement], Never> = .init([])
    
    internal init(model: Model, textResult: String, items: [ProductItemViewModel], navigation: NavigationBarView.ViewModel) {
        
        self.items = items
        self.model = model
        self.textResult = textResult
        self.navigation = navigation
        self.searchBar = .init()
        self.filterBar = .init(filterButton: nil)
        self.filterBar.filterButton = .init(action: { [weak self] in
            self?.action.send(SearchItemsViewModelAction.OpenFilter())
        })
    }
    
    convenience init(_ model: Model, text: String, backAction: @escaping () -> Void) {
        
        self.init(model: model, textResult: "Результаты поиска\nпо запросу «\(text)»", items: [], navigation: .init(title: .text("Поиск")))
        self.navigation.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        self.searchText.value = text
        bind()
    }

    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                   
                case _ as SearchItemsViewModelAction.OpenFilter:
                    
                    let filter = FilterViewModel(items: itemsData.value, backAction: { [weak self] in
                        self?.link = nil
                    })
                    link = .filter(filter)
                    bind(filter)
                    
                case let payload as SearchItemsViewModelAction.OnOrderTapped:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                    link = .onOrder(OnOrderViewModel(model: model, item: item, backAction: {[weak self] in
                        self?.link = nil
                    }))
                    
                case let payload as SearchItemsViewModelAction.OpenItemDetail:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                    
                    link = .itemDetail(ProductDetailViewModel(model: model, item: item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        searchBar.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as SearchBarViewAction.SearchText:
                    
                    self.searchText.value = payload.text
                   
                default: break
                }
            }
            .store(in: &bindings)
        itemsData
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] items in
                filteredData.value = items
            }
            .store(in: &bindings)
        
        filteredData
            .combineLatest(filterBar.sortType)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] items, sortType in
                withAnimation {
                    
                    switch sortType {
                        
                    case .initial:
                        let itemsModel: [ProductItemViewModel] = items.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .nameDown:
                        
                        let filtered = items.sorted(by: { $0.name < $1.name })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .nameUp:
                        
                        let filtered = items.sorted(by: { $0.name > $1.name })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .priceDown:
                        let filtered = items.sorted(by: { Double($0.price?.price ?? "") ?? 0 > Double($1.price?.price ?? "") ?? 0 })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                        
                    case .priceUp:
                        let filtered = items.sorted(by: { Double($0.price?.price ?? "") ?? 0 < Double($1.price?.price ?? "") ?? 0 })
                        
                        let itemsModel: [ProductItemViewModel] = filtered.map { ProductItemViewModel(model: model, item: $0) }
                        self.items = itemsModel
                        bind(itemsModel)
                    }
                }
            }
            .store(in: &bindings)
        
        searchText
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] searchText in
                
                self.textResult = "Результаты поиска\nпо запросу «\(searchText)»"
                let filtered = model.catalogElements.value.filter({ $0.name.lowercased().contains(searchText.lowercased()) || $0.article.lowercased().contains(searchText.lowercased()) })
                itemsData.value = filtered
            }
            .store(in: &bindings)
    }
    
    func bind(_ items: [ProductItemViewModel]) {
        for item in items {
            
            item.action
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] action in
                    switch action {
                     
                    case let payload as ProductItemViewModelAction.OnOrderTapped:
                        self.action.send(SearchItemsViewModelAction.OnOrderTapped(id: payload.id))
                        
                    case let payload as ProductItemViewModelAction.ItmemTapped:
                        self.action.send(SearchItemsViewModelAction.OpenItemDetail(id: payload.id))
                        
                        
                    case let payload as ProductItemViewModelAction.FavoriteItmemTapped:
                        model.action.send(ModelAction.Favorites.Update(id: payload.id))
                        
                    default: break
                    }
                }
                .store(in: &bindings)
            
        }
    }
    
    func bind(_ filter: FilterViewModel) {
        
        filter.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case let payload as FilterViewModelAction.ShowFiltered:
                    filteredData.value = payload.filtered
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    enum Link {
        
        case onOrder(OnOrderViewModel)
        case itemDetail(ProductDetailViewModel)
        case filter(FilterViewModel)
    }
    
}

enum SearchItemsViewModelAction {
    
    struct OnOrderTapped: Action {
        let id: String
    }
    
    struct OpenItemDetail: Action {
        let id: String
    }
    
    struct OpenFilter: Action { }
}
