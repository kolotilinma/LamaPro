//
//  SearchItemsView.swift
//  LamaPro
//
//  Created by Михаил on 07.03.2023.
//

import SwiftUI

struct SearchItemsView: View {
    
    @ObservedObject var viewModel: SearchItemsViewModel
    
    var columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
    
    var body: some View {
        
        ZStack {
            
            ScrollView {
                
                VStack(alignment: .leading) {
                    
                    VStack {
                        
                        SearchBarView(viewModel: viewModel.searchBar)
                        
                        FilterButtonView(viewModel: viewModel.filterBar)
                    }
                    
                    Text(viewModel.textResult)
                        .font(.interBold(size: 20))
                        .foregroundColor(.lamaBlack)
                        .multilineTextAlignment(.leading)
                    
                    if viewModel.items.count == 0 {
                        
                        VStack(spacing: 25) {
                            
                            Image("EmptySearch")
                                .resizable()
                                .scaledToFill()
                                .padding(.horizontal, 40)
                            
                            Text("Сожалеем, но ничего не найдено")
                                .font(.interBold(size: 20))
                                .foregroundColor(.lamaBlack)
                                .multilineTextAlignment(.leading)
                            
                            Text("Попробуйте ввести другой запрос")
                                .font(.interRegular(size: 16))
                                .foregroundColor(.lamaBlack)
                                .multilineTextAlignment(.leading)
                        }
                        
                    } else {
                        
                        LazyVGrid(columns: columns) {
                            
                            ForEach(viewModel.items) { item in
                                
                                ProductItemView(viewModel: item)
                            }
                        }
                    }
                }
                .padding(.horizontal, 16)
                .padding(.top, 16)
                
            }
            .resignKeyboardOnDragGesture()
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                
                if let link = viewModel.link  {
                    
                    switch link {
                    
                    case .filter(let filter):
                        FilterView(viewModel: filter)
                        
                    case .onOrder(let onOrder):
                        OnOrderView(viewModel: onOrder)
                        
                    case .itemDetail(let model):
                        ProductDetailView(viewModel: model)
                        
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigation)
    }
}

struct SearchItemsView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        SearchItemsView(viewModel: .init(model: .emptyMock, textResult: "Результаты поиска\nпо запросу «Бур»", items: [
            .init(model: .emptyMock, id: "1", title: "Example", code: "123123", price: "1231.22", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
            .init(model: .emptyMock, id: "2", title: "Example2", code: "35453", price: "3561.22", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
            .init(model: .emptyMock, id: "3", title: "Example3", code: "465736", price: "2561.22", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {})))
        ], navigation: .init(title: .text("Поиск"))))
    }
}
