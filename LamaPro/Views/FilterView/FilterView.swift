//
//  FilterView.swift
//  LamaPro
//
//  Created by Михаил on 10.04.2023.
//

import SwiftUI
import Sliders

struct FilterView: View {
    
    @ObservedObject var viewModel: FilterViewModel
    
    var body: some View {
        
        ScrollView {
            
            VStack(spacing: 30) {
                
                HStack {
                    
                    Text("Цена")
                        .font(.interBold(size: 17))
                        .foregroundColor(.lamaBlack)
                    
                    Spacer()
                }
                
                RangeSlider(range: $viewModel.currentRange, in: viewModel.rangeFrom)
                    .rangeSliderStyle(HorizontalRangeSliderStyle(track: HorizontalRangeTrack(
                        view: Capsule().foregroundColor(.lamaGreenDark)
                    )
                        .background(Capsule().foregroundColor(Color.lamaGreenDark.opacity(0.25)))
                        .frame(height: 4)))
                    .frame(height: 30)
                
                HStack {
                    
                    HStack(spacing: 0) {
                        Text("От")
                            .font(.interSemiBold(size: 13))
                            .foregroundColor(Color(hex: "#A7A9AC"))
                        
                        TextField("", text: $viewModel.minText, onEditingChanged: { editing in
                            if editing == false {
                                
                                viewModel.setNewMinValue(newMin: viewModel.minText)
                            }
                        })
                        .keyboardType(.numberPad)
                        .font(.interRegular(size: 17))
                        .padding(.horizontal, 4)
                        
                        Text("₽")
                            .font(.interSemiBold(size: 13))
                            .foregroundColor(Color(hex: "#A7A9AC"))
                    }
                    .padding(10)
                    .background(Color.lamaGrayLight)
                    .cornerRadius(30)
                    
                    Spacer()
                    
                    HStack(spacing: 0) {
                        
                        Text("От")
                            .font(.interSemiBold(size: 13))
                            .foregroundColor(Color(hex: "#A7A9AC"))
                        
                        TextField("", text: $viewModel.maxText, onEditingChanged: { editing in
                            if editing == false {
                                
                                viewModel.setNewMaxValue(newMax: viewModel.maxText)
                            }
                        })
                        .keyboardType(.numberPad)
                        .font(.interRegular(size: 17))
                        .padding(.horizontal, 4)
                        
                        Text("₽")
                            .font(.interSemiBold(size: 13))
                            .foregroundColor(Color(hex: "#A7A9AC"))
                    }
                    .padding(10)
                    .background(Color.lamaGrayLight)
                    .cornerRadius(30)
                    
                }
                //            .padding(30)
                
                
                if let button = viewModel.confirmButton {
                    
                    ButtonSimpleView(viewModel: button)
                        .padding()
                }
                
            }
            .padding(.horizontal, 16)
            .padding(.top, 16)
        }
        .resignKeyboardOnDragGesture()
        .background(Color.lamaWhite.onTapGesture {
            UIApplication.shared.endEditing()
        })
        .navigationBar(with: viewModel.navigation)
    }
}

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        FilterView(viewModel: .init(minPrice: 2000, maxPrice: 30000,confirmButton: .init(title: "Показать", height: 50, action: {}), navigation: .init(title: .text("Фильтры")), backAction: {}))
            .previewLayout(.sizeThatFits)
    }
}
