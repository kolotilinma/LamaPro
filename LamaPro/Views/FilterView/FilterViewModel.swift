//
//  FilterViewModel.swift
//  LamaPro
//
//  Created by Михаил on 10.04.2023.
//

import SwiftUI
import Combine

final class FilterViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigation: NavigationBarView.ViewModel
    @Published var confirmButton: ButtonSimpleView.ViewModel?

    var rangeFrom: ClosedRange<Double>
    @Published var currentRange: ClosedRange<Double>
    
    @Published var minText: String = ""
    @Published var maxText: String = ""
    
    let backAction: () -> Void
    
    private let itemsData: CurrentValueSubject<[CatElement], Never> = .init([])
    private var bindings = Set<AnyCancellable>()
    
    internal init(minPrice: Double, maxPrice: Double, confirmButton: ButtonSimpleView.ViewModel?, navigation: NavigationBarView.ViewModel, backAction: @escaping () -> Void) {
        
        self.navigation = navigation
        self.confirmButton = confirmButton
        self.rangeFrom = minPrice...maxPrice
        self.currentRange = minPrice...maxPrice
        self.backAction = backAction
    }
    
    convenience init(items: [CatElement], backAction: @escaping () -> Void) {
        
        self.init(minPrice: 0, maxPrice: 0, confirmButton: nil, navigation: .init(title: .text("Фильтры")), backAction: backAction)
        self.navigation.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: self.backAction)]
        itemsData.value = items
        bind()
    }

    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                   
                case _ as FilterViewModelAction.FindItems:
                    
                    let filtered = itemsData.value.filter {
                        Double($0.price?.price ?? "") ?? 0 >= currentRange.lowerBound
                        && Double($0.price?.price ?? "") ?? 0 <= currentRange.upperBound
                    }
                    self.action.send(FilterViewModelAction.ShowFiltered(filtered: filtered))
                    backAction()
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        itemsData
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] itemsData in
                let min = Double(itemsData.min(by: { Double($0.price?.price ?? "") ?? 0 < Double($1.price?.price ?? "") ?? 0 })?.price?.price ?? "") ?? 0
                let max = Double(itemsData.max(by: { Double($0.price?.price ?? "") ?? 0 < Double($1.price?.price ?? "") ?? 0 })?.price?.price ?? "") ?? 0
                rangeFrom = min...max
                currentRange = min...max
                
                guard itemsData.isEmpty == false else { return }
                
                confirmButton = .init(title: "Показать", backgroundColor: .lamaGreen, height: 50, withBackAction: true, action: { [weak self] in
                    self?.action.send(FilterViewModelAction.FindItems())
                })
            }
            .store(in: &bindings)
        
        $currentRange
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] currentRange in
                self.minText = String(Int(currentRange.lowerBound))
                self.maxText = String(Int(currentRange.upperBound))
            }
            .store(in: &bindings)

    }
    
    func setNewMinValue(newMin: String) {
        
        guard let double = Double(minText) else {
            currentRange = rangeFrom.lowerBound...currentRange.upperBound
            return
        }
        
        if double < rangeFrom.lowerBound {
            currentRange = rangeFrom.lowerBound...currentRange.upperBound
            
        } else if double > currentRange.lowerBound && double < currentRange.upperBound {
            
            currentRange = double...currentRange.upperBound
            
        } else if double > currentRange.upperBound && double < rangeFrom.upperBound {
            
            currentRange = double...double
        } else {
            
            currentRange = rangeFrom.upperBound...rangeFrom.upperBound
        }
    }
    
    func setNewMaxValue(newMax: String) {
        
        guard let double = Double(maxText) else {
            currentRange = currentRange.lowerBound...rangeFrom.upperBound
            return
        }
        if double > rangeFrom.upperBound {
            currentRange = currentRange.lowerBound...rangeFrom.upperBound
            
        } else if double > currentRange.lowerBound && double < rangeFrom.upperBound {
            
            currentRange = currentRange.lowerBound...double
            
        } else if double < currentRange.lowerBound && double > rangeFrom.lowerBound {
            
            currentRange = double...double
        } else {
            
            currentRange = rangeFrom.lowerBound...rangeFrom.lowerBound
        }
    }
    
}

enum FilterViewModelAction {
    
    struct FindItems: Action { }
    
    struct ShowFiltered: Action {
        
        let filtered: [CatElement]
    }
}
