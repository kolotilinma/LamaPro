//
//  AuthentificationView.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI

struct AuthentificationView: View {
    
    @ObservedObject var viewModel: AuthentificationViewModel
    
    var body: some View {
        
        VStack {
            
            if let state = viewModel.state {
                switch state {
                    
                case .login(let login):
                    LoginView(viewModel: login)
                    
                case .register(let registration):
                    RegistrationView(viewModel: registration)
                    
                }
            }
        }
        .navigationBar(with: viewModel.navigation)
    }
}

struct AuthentificationView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        AuthentificationView(viewModel: .init(model: .emptyMock, navigation: .sample0))
    }
}
