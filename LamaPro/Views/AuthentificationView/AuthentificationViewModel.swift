//
//  AuthentificationViewModel.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI
import Combine

class AuthentificationViewModel: ObservableObject {
    
    @Published var navigation: NavigationBarView.ViewModel
    @Published var state: AuthState?
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, navigation: NavigationBarView.ViewModel) {
        
        self.model = model
        self.navigation = navigation
    }
    
    convenience init(model: Model, backAction: @escaping () -> Void) {
        
        let picker = NavigationBarView.ViewModel.TitlePicker(selected: 0)
        self.init(model: model, navigation: .init(title: .picker(picker)))
        
        navigation.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        bind(picker)
    }
    
    func bind(_ picker: NavigationBarView.ViewModel.TitlePicker) {
        picker.$selected
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] selected in
                
                if selected == 0 {
                    
                    let login = LoginViewModel(model: model)
                    state = .login(login)
                    bind(login)
                    
                } else {
                    
                    let register = RegistrationViewModel(model: model)
                    state = .register(register)
                    bind(register)
                }
            }
            .store(in: &bindings)
    }
    
    func bind(_ login: LoginViewModel) {
        login.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case let payload as LoginViewModelAction.SwitchTo:
                    guard case .picker(let picker) = navigation.title else { return }
                    picker.selected = payload.index
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    func bind(_ register: RegistrationViewModel) {
        register.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case let payload as LoginViewModelAction.SwitchTo:
                    guard case .picker(let picker) = navigation.title else { return }
                    picker.selected = payload.index
                    
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    enum AuthState {
        
        case login(LoginViewModel)
        case register(RegistrationViewModel)
    }
    
}
