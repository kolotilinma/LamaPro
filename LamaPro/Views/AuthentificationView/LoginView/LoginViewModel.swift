//
//  LoginViewModel.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI
import Combine

class LoginViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var alert: Alert.ViewModel?
    
    @Published var loginButton: ButtonSimpleView.ViewModel?
    @Published var login = ""
    @Published var password = ""
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    init(model: Model, loginButton: ButtonSimpleView.ViewModel?) {
        
        self.model = model
//        self.login = .init(masks: [], regExp: "")
//        self.password = .init(masks: [], regExp: "")
//        self.login = .init(placeholder: "Ваш E-mail")
//        self.password = .init(placeholder: "Введите пароль", isSecureText: true)
        self.loginButton = loginButton
    }
    
    convenience init(model: Model) {
        
        self.init(model: model, loginButton: nil)
        
        self.loginButton = .init(title: "Войти в аккаунт", backgroundColor: .lamaGreen, height: 50, action: { [weak self] in
            
            self?.action.send(LoginViewModelAction.LoginAction())
        })
        
        bind()
    }
    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case _ as LoginViewModelAction.LoginAction:
                           
                    if login.isValidEmail {
                        
                        Task {
                            do {
                                model.action.send(ModelAction.CoverAction.Spinner.Show())
                                try await model.handleAuthLogin(login: login, pass: password)
                                model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                model.action.send(TabBarAction.CloseAll())
                            } catch {
                                await MainActor.run(body: {
                                    model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                    alert = .init(title: "Ошибка", message: model.convertErrorToString(error), primary: .init(type: .default, title: "Ок", action: { [weak self] in
                                        self?.alert = nil
                                    }))
                                })
                            }
                        }
                    } else {
                        
                        alert = .init(title: "Ошибка", message: "Введите корректные данные", primary: .init(type: .default, title: "Ок", action: { [weak self] in
                            self?.alert = nil
                        }))
                    }
                    
                    
                default: break
                }
            }
            .store(in: &bindings)
        
    }
    
    
    func switchToRegister() {
        action.send(LoginViewModelAction.SwitchTo(index: 1))
    }
    
    func hideKeyboard() {
        UIApplication.shared.endEditing()
    }
}

enum LoginViewModelAction {
    
    struct SwitchTo: Action {
        let index: Int
    }
    
    struct LoginAction: Action {}
    struct Register: Action {
        
    }
}
