//
//  LoginView.swift
//  LamaPro
//
//  Created by Михаил on 03.01.2023.
//

import SwiftUI

struct LoginView: View {
    
    @ObservedObject var viewModel: LoginViewModel
    
    var body: some View {
        
        ScrollView {
            
            ZStack {
                
                VStack {
                    
                    Image("Logo")
                        .resizable()
                        .scaledToFill()
                        .frame(height: 256, alignment: .bottom)
                        .clipped()
                    
                    Spacer()
                }
                
                VStack {
                    
                    Spacer()
                        .frame(height: 236)
                    
                    VStack {
                        
                        VStack(spacing: 16) {
                            
                            Image("LogoSmall")
                            
                            Text("Заполните поля, чтобы войти")
                                .font(.interRegular(size: 16))
                            
                            VStack {
                                
                                TextField("Ваш E-mail", text: $viewModel.login)
                                    .frame(height: 48)
                                
                                Divider()
                            }
                            
                            VStack {
                                SecureField("Введите пароль", text: $viewModel.password)
                                    .frame(height: 48)
                                
                                Divider()
                            }
                            
                            //                        TextFieldView(viewModel: viewModel.login)
                            //                            .frame(height: 48)
                            //
                            //                        TextFieldView(viewModel: viewModel.password)
                            //                            .frame(height: 48)
                            
                            if let button = viewModel.loginButton {
                                
                                ButtonSimpleView(viewModel: button)
                                    .padding(.horizontal, 90)
                            }
                            
                            Spacer()
                            
                            Button(action: {
                                let phone = "+74951222254"
                                guard let url = URL(string: "telprompt://\(phone)"),
                                      UIApplication.shared.canOpenURL(url) else {
                                    return
                                }
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            }, label: {
                                
                                HStack(spacing: 8) {
                                    
                                    Image(systemName: "phone.fill")
                                        .resizable()
                                        .scaledToFill()
                                        .frame(width: 16, height: 16)
                                    
                                    Text("+7 (495) 122-22-54")
                                        .font(.interRegular(size: 13))
                                }
                                .foregroundColor(Color.lamaBlack)
                            })
                            
                            HStack(spacing: 4) {
                                
                                Text("У вас еще нат аккаунта?")
                                    .font(.interRegular(size: 12))
                                
                                Button(action: viewModel.switchToRegister) {
                                    
                                    Text("Зарегистрируйтесь")
                                        .font(.interBold(size: 12))
                                        .foregroundColor(.lamaGreen)
                                }
                            }
                        }
                        .padding(.top, 34)
                        .padding(.horizontal, 16)
                    }
                    .frame(maxWidth: .infinity)
                    .background(Color.white.onTapGesture {
                        viewModel.hideKeyboard()
                    })
                    .cornerRadius(20, corners: [.topRight, .topLeft])
                }
            }
        }
        .alert(item: $viewModel.alert, content: { alertViewModel in
            Alert(with: alertViewModel)
        })
    }
}

struct LoginView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        LoginView(viewModel: .init(model: .emptyMock, loginButton: .init(title: "Войти в аккаунт", backgroundColor: .lamaGreen, action: {})))
    }
}
