//
//  RegistrationView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

struct RegistrationView: View {
    
    @ObservedObject var viewModel: RegistrationViewModel
    
    var body: some View {
        
        ScrollView {
            
            VStack {
                
                Image("LogoSmall")
                
                Text("Заполните поля, чтобы создать профиль")
                    .font(.interRegular(size: 16))
                
                VStack {
                    
                    VStack {
                        
                        TextField("Ваше имя", text: $viewModel.name)
                            .frame(height: 48)
                        
                        Divider()
                    }
                    
                    VStack {
                        
                        TextField("Ваш E-mail", text: $viewModel.email)
                            .frame(height: 48)
                        
                        Divider()
                    }
                    
                    VStack {
                        
                        TextField("Придумайте пароль", text: $viewModel.password)
                            .frame(height: 48)
                        
                        Divider()
                    }
                    
                    VStack {
                        
                        TextField("Повторите пароль", text: $viewModel.repeatPassword)
                            .frame(height: 48)
                        
                        Divider()
                    }
                    
                    Toggle(isOn: $viewModel.rules) {
                        
                        Text("Даю согласие на обработку моих персональных данных в соответствии с правилами")
                            .font(.interRegular(size: 15))
                    }
                    
                    Toggle(isOn: $viewModel.subscription) {
                        
                        Text("Хочу первым получать новости о скидках и новинках")
                            .font(.interRegular(size: 15))
                    }
                }
                
                if let button = viewModel.registerButton {
                    
                    ButtonSimpleView(viewModel: button)
                        .padding(.horizontal, 50)
                }
                
                Spacer()
                    .frame(height: 40)
                
                Button(action: {
                    let phone = "+74951222254"
                    guard let url = URL(string: "telprompt://\(phone)"),
                          UIApplication.shared.canOpenURL(url) else {
                        return
                    }
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    
                }, label: {
                    
                    HStack(spacing: 8) {
                        
                        Image(systemName: "phone.fill")
                            .resizable()
                            .scaledToFill()
                            .frame(width: 16, height: 16)
                        
                        Text("+7 (495) 122-22-54")
                            .font(.interRegular(size: 13))
                    }
                    .foregroundColor(Color.lamaBlack)
                })
                
                HStack(spacing: 4) {
                    
                    Text("Уже зарегистрированы?")
                        .font(.interRegular(size: 12))
                    
                    Button(action: {
                        viewModel.switchToLogin()
                    }, label: {
                        
                        Text("Войдите")
                            .font(.interBold(size: 12))
                            .foregroundColor(.lamaGreen)
                    })
                }
            }
            .padding(16)
            .background(Color.white.onTapGesture {
                viewModel.hideKeyboard()
            })
        }
        .alert(item: $viewModel.alert, content: { alertViewModel in
            Alert(with: alertViewModel)
        })
    }
}

struct RegistrationView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        RegistrationView(viewModel: .init(model: .emptyMock, registerButton: .init(title: "Регистрация", backgroundColor: .lamaGreen, action: {})))
    }
}
