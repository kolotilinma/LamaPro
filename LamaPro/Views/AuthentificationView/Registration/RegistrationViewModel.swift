//
//  RegistrationViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI
import Combine

class RegistrationViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var alert: Alert.ViewModel?
    
    @Published var name = ""
    @Published var email = ""
    @Published var password = ""
    @Published var repeatPassword = ""
    @Published var rules = false
    @Published var subscription = false
    
    @Published var registerButton: ButtonSimpleView.ViewModel?
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    init(model: Model,registerButton: ButtonSimpleView.ViewModel?) {
        
        self.model = model
        self.registerButton = registerButton
    }
    
    convenience init(model: Model) {
        self.init(model: model, registerButton: nil)
        
        bind()
    }
    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case _ as LoginViewModelAction.Register:
                    if email.isValidEmail, name.count > 2 {
                        
                        Task {
                            do {
                                model.action.send(ModelAction.CoverAction.Spinner.Show())
                                try await model.handleAuthRegister(name: name, email: email, password: password, mailing: subscription, politics: rules)
                                model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                model.action.send(TabBarAction.CloseAll())
                            } catch {
                                await MainActor.run(body: {
                                    model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                    alert = .init(title: "Ошибка", message: model.convertErrorToString(error), primary: .init(type: .default, title: "Ок", action: { [weak self] in
                                        self?.alert = nil
                                    }))
                                })
                            }
                        }
                    } else {
                        
                        alert = .init(title: "Ошибка", message: "Введите корректные данные", primary: .init(type: .default, title: "Ок", action: { [weak self] in
                            self?.alert = nil
                        }))
                    }
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        $password
            .combineLatest($repeatPassword, $rules)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] password, repeatPassword, rules in
                withAnimation {
                    
                    if  name != "", email != "", password != "", repeatPassword != "", password == repeatPassword, rules {
                        
                        self.registerButton = .init(title: "Зарегистрироваться", backgroundColor: .lamaGreen, height: 50, action: { [weak self] in
                            
                            self?.action.send(LoginViewModelAction.Register())
                        })
                        
                    } else {
                        
                        self.registerButton = .init(title: "Зарегистрироваться", backgroundColor: .lamaGreen, disabled: true, height: 50, action: { })
                        
                    }
                }
            }
            .store(in: &bindings)
    }
    
    func hideKeyboard() {
        UIApplication.shared.endEditing()
    }
    
    func switchToLogin() {
        action.send(LoginViewModelAction.SwitchTo(index: 0))
    }
    
}
