//
//  ConfirmCartViewModel.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI
import Combine

class ConfirmCartViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigationBar: NavigationBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    @Published var alert: Alert.ViewModel?
    
    @Published var payerType: PayerTypeView.ViewModel
    @Published var deliveryView: DeliveryView.ViewModel
    @Published var paymentView: PaymentView.ViewModel?
    @Published var buyerView: BuyerView.ViewModel?
    @Published var company: OrganizationView.ViewModel?
    
    @Published var orderButton: ButtonSimpleView.ViewModel?
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    init(model: Model, navigationBar: NavigationBarView.ViewModel, payerType: PayerTypeView.ViewModel, deliveryView: DeliveryView.ViewModel, paymentView: PaymentView.ViewModel?, buyerView: BuyerView.ViewModel?, orderButton: ButtonSimpleView.ViewModel? = nil) {
        
        self.model = model
        self.navigationBar = navigationBar
        self.payerType = payerType
        self.deliveryView = deliveryView
        self.paymentView = paymentView
        self.buyerView = buyerView
        self.orderButton = orderButton
    }
    
    convenience init(model: Model, backAction: @escaping () -> Void) {
        
        self.init(model: model, navigationBar: .init(title: .text("Оформление заказа")), payerType: .init(), deliveryView: .init(type: nil), paymentView: nil, buyerView: nil)
        self.navigationBar.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        
        bind()
    }
    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as ConfirmCartViewModelAction.OpenSuccess:
                    link = .success(SuccessView.ViewModel(title: "Заказ успешно оформлен!", subTitle: "Скоро наш менеджер свяжется с Вами для уточнения деталей", action: { [weak self] in
                        self?.model.action.send(TabBarAction.CloseAll())
                    }))
                    
                case let payload as ConfirmCartViewModelAction.OrderTapped:
                    
                    guard deliveryView.isValid == true else {
                        alert = .init(title: "Ошибка", message: "Введите данные о доставке", primary: .init(type: .default, title: "Ок", action: { [weak self] in
                            self?.alert = nil
                        }))
                        return
                    }
                    let address = deliveryView.addres.textField.text ?? "МО, г. Раменское, ш. 4-й км Донинского, стр. За"
                    
                    guard let name = buyerView?.name.textField.text,
                          let lastName = buyerView?.lastName.textField.text,
                          let phone = buyerView?.phone.textField.text,
                          let email = buyerView?.email.textField.text,
                          let type = deliveryView.type
                    else {
                        alert = .init(title: "Ошибка", message: "Заполните все поля", primary: .init(type: .default, title: "Ок", action: { [weak self] in
                            self?.alert = nil
                        }))
                        return
                    }
                    let secondName = buyerView?.secondName.textField.text
                    
                    let items: [CartData.Item] = payload.elements.compactMap { catElement in
                        
                        guard let count = model.cartElements.value.items.first(where: { $0.elementId == catElement.id })?.count else { return nil }
                        return CartData.Item(elementId: catElement.id, count: count)
                    }
                    
                    switch payerType.type {
                        
                    case .individual:
                        Task {
                            do {
                                model.action.send(ModelAction.CoverAction.Spinner.Show())
                                try await model.handleOrderCart(items: items, name: name, secondName: secondName, lastName: lastName, phone: phone, email: email, address: address, userType: payerType.type, delivery: type)
                                model.cartElements.value.items = []
                                model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                self.action.send(ConfirmCartViewModelAction.OpenSuccess())
                                
                            } catch {
                                model.action.send(ModelAction.CoverAction.Spinner.Hide())
                            }
                        }
                        
                    case .entity:
                    
                        guard let companyName = company?.company.textField.text,
                              let inn = company?.inn.textField.text,
                              let kpp = company?.kpp.textField.text
                        else {
                            alert = .init(title: "Ошибка", message: "Заполните реквизиты организации", primary: .init(type: .default, title: "Ок", action: { [weak self] in
                                self?.alert = nil
                            }))
                            return
                        }
                        
                        Task {
                            do {
                                model.action.send(ModelAction.CoverAction.Spinner.Show())
                                try await model.handleOrderCart(items: items, name: name, secondName: secondName, lastName: lastName, phone: phone, email: email, address: address, userType: payerType.type, delivery: type, company: companyName, inn: inn, kpp: kpp)
                                model.cartElements.value.items = []
                                model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                self.action.send(ConfirmCartViewModelAction.OpenSuccess())
                                
                            } catch {
                                model.action.send(ModelAction.CoverAction.Spinner.Hide())
                            }
                        }
                    }
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        model.cartElements
            .combineLatest(model.catalogStatus)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] cartElements, catalogStatus in
                
                guard case .done = catalogStatus, cartElements.items.isEmpty == false else { return }
                let cartArr: [String] = cartElements.items.map { $0.elementId }
                let elements = model.catalogElements.value.filter({ cartArr.contains($0.id)})
                
                self.orderButton = .init(title: "Оформить заказ", backgroundColor: .lamaGreen, height: 50, action: { [weak self] in
                    
                    self?.action.send(ConfirmCartViewModelAction.OrderTapped(elements: elements))
                })
                
            }
            .store(in: &bindings)
        
        payerType.$type
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] type in
                withAnimation {
                    
                    paymentView = .init(type: type)
                    switch type {
                        
                    case .individual:
                        company = nil
                        
                    case .entity:
                        company = .init()
                    }
                }
            }
            .store(in: &bindings)
        
        model.user
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] user in
                guard let user = user else { return }
                
                withAnimation {
                    
                    buyerView = .init(user: user)
                }
            }
            .store(in: &bindings)
    }
    
    
    enum Link {
        
        case success(SuccessView.ViewModel)
    }
    
    enum ConfirmCartViewModelAction {
        
        struct OrderTapped: Action {
            let elements: [CatElement]
        }
        
        struct OpenSuccess: Action {
            
//            let item: CatElement
        }
    }

}

