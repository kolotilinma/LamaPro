//
//  PayerTypeViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI

extension PayerTypeView {
    
    final class ViewModel: ObservableObject {
        
        @Published var type: UserTypeData
        
        @Published var individualButton: SelectebleButtonView.ViewModel?
        @Published var entityButton: SelectebleButtonView.ViewModel?
        
        internal init(type: UserTypeData = .individual, individualButton: SelectebleButtonView.ViewModel? = nil, entityButton: SelectebleButtonView.ViewModel? = nil) {
            
            self.type = type
            
            self.individualButton = .init(title: "Физическое лицо", selected: true, action: { [weak self] in
                
                self?.type = .individual
                self?.individualButton?.selected = true
                self?.entityButton?.selected = false
            })
            
            self.entityButton = .init(title: "Юридическое лицо", selected: false, action: { [weak self] in
                
                self?.type = .entity
                self?.individualButton?.selected = false
                self?.entityButton?.selected = true
            })
        }
    }
}

struct PayerTypeView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        VStack(spacing: 20) {
            
            Text("Тип плательщика")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            VStack(spacing: 15) {
                
                if let individual = viewModel.individualButton {
                    
                    SelectebleButtonView(viewModel: individual)
                }
                
                Divider()
                
                if let entity = viewModel.entityButton {
                    
                    SelectebleButtonView(viewModel: entity)
                }
            }
        }
    }
}

struct PayerTypeViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        PayerTypeView(viewModel: .init())
            .padding()
            .previewLayout(.sizeThatFits)
    }
}



