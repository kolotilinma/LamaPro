//
//  PaymentViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 28.03.2023.
//

import SwiftUI

extension PaymentView {
    
    final class ViewModel: ObservableObject {
        
        @Published var text: String
        
        internal init(text: String) {
            
            self.text = text
        }
        
        convenience init(type: UserTypeData) {
            
            self.init(text: type.paymentTextDescription)
        }
    }
}

struct PaymentView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
            
            Text("Оплата")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Text(viewModel.text)
                .font(.interRegular(size: 17))
                .foregroundColor(.lamaBlack)
        }
    }
}

struct PaymentViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        PaymentView(viewModel: .init(text: "Оплата картами Visa/MasterCard"))
    }
}
