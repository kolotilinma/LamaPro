//
//  DeliveryViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 28.03.2023.
//

import SwiftUI
import Combine

extension DeliveryView {
    
    final class ViewModel: ObservableObject {
        
        @Published var type: DeliveryType?
        @Published var addres: TextFieldView.ViewModel
        
        @Published var deliveryButton: SelectebleButtonView.ViewModel?
        @Published var pickupButton: SelectebleButtonView.ViewModel?
        
        private var bindings = Set<AnyCancellable>()
        
        internal init(type: DeliveryType?) {
            
            self.type = type
            self.addres = .init(placeholder: "Введите адрес")
            
            self.deliveryButton = .init(title: "Доставка", selected: false, action: { [weak self] in
                
                self?.type = .delivery
                self?.deliveryButton?.selected = true
                self?.pickupButton?.selected = false
            })
            
            self.pickupButton = .init(title: "Самовывоз", selected: false, action: { [weak self] in
                
                self?.type = .pickup
                self?.deliveryButton?.selected = false
                self?.pickupButton?.selected = true
            })
            
            bind()
        }
        
        func bind() {
            
            addres.textField.$text
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] text in
                    guard text != nil else { return }
                    
                    self.type = .delivery
                    self.deliveryButton?.selected = true
                    self.pickupButton?.selected = false
                    
                }
                .store(in: &bindings)
            
        }
        
        var isValid: Bool {
            
            if let type = type {
                switch type {
                    
                case .pickup:
                    return true
                    
                case .delivery:
                    
                    if let addres = addres.textField.text {
                        return true
                        
                    } else {
                        return false
                    }
                }
                
            } else {
                return false
            }
        }
    }
}


struct DeliveryView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
            
            Text("Доставка")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            if let delivery = viewModel.deliveryButton {
                
                SelectebleButtonView(viewModel: delivery)
            }
            
            TextFieldView(viewModel: viewModel.addres)
            
            if let pickup = viewModel.pickupButton {
                
                SelectebleButtonView(viewModel: pickup)
            }
            
            Text("МО, г. Раменское, ш. 4-й км Донинского, стр. За")
                .font(.interRegular(size: 13))
                .foregroundColor(.lamaBlack)
        }
    }
}

struct DeliveryViewComponent_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryView(viewModel: .init(type: nil))
    }
}
