//
//  OrganizationViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 09.04.2023.
//

import SwiftUI

extension OrganizationView {
    
    final class ViewModel: ObservableObject {
        
        @Published var company: TextFieldView.ViewModel
        @Published var inn: TextFieldView.ViewModel
        @Published var kpp: TextFieldView.ViewModel
        
        internal init(company: TextFieldView.ViewModel, inn: TextFieldView.ViewModel, kpp: TextFieldView.ViewModel) {
            
            self.company = company
            self.inn = inn
            self.kpp = kpp
        }
        
        
        convenience init() {

            self.init(
                company: .init(text: nil, placeholder: "Наименование организации"),
                inn: .init(text: nil, placeholder: "ИНН", keyboardType: .numberPad),
                kpp: .init(text: nil, placeholder: "КПП", keyboardType: .numberPad)
            )
        }
    }
    
}

struct OrganizationView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
            
            Text("Реквизиты организации")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            
            TextFieldView(viewModel: viewModel.company)
            
            TextFieldView(viewModel: viewModel.inn)
            
            TextFieldView(viewModel: viewModel.kpp)
            
//            TextFieldView(viewModel: viewModel.phone)
        }
    }
}

struct OrganizationViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        OrganizationView(viewModel: .init(company: .init(placeholder: "Наименование организации"), inn: .init(placeholder: "ИНН"), kpp: .init(placeholder: "КПП")))
    }
}

//    let company: String?
//    let company_adr: String?
//    let inn: String?
//    let kpp: String?
