//
//  BuyerViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 28.03.2023.
//

import SwiftUI

extension BuyerView {
    
    final class ViewModel: ObservableObject {
        
        @Published var lastName: TextFieldView.ViewModel
        @Published var name: TextFieldView.ViewModel
        @Published var secondName: TextFieldView.ViewModel
        @Published var email: TextFieldView.ViewModel
        @Published var phone: TextFieldView.ViewModel
        
        internal init(lastName: TextFieldView.ViewModel, name: TextFieldView.ViewModel, secondName: TextFieldView.ViewModel, email: TextFieldView.ViewModel, phone: TextFieldView.ViewModel) {
            
            self.lastName = lastName
            self.name = name
            self.secondName = secondName
            self.email = email
            self.phone = phone
        }
        
        
        convenience init(user: UserData) {

            self.init(
                lastName: .init(text: user.lastName, placeholder: "Фамилия"),
                name: .init(text: user.name, placeholder: "Имя"),
                secondName: .init(text: user.secondName, placeholder: "Отчество"),
                email: .init(text: user.email, placeholder: "E-mail"),
                phone: .init(text: user.phone, placeholder: "Телефон")
            )
        }
    }
    
}

struct BuyerView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
            
            Text("Покупатель")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            TextFieldView(viewModel: viewModel.lastName)
            
            TextFieldView(viewModel: viewModel.name)
            
            TextFieldView(viewModel: viewModel.secondName)
            
            TextFieldView(viewModel: viewModel.email)
            
            TextFieldView(viewModel: viewModel.phone)
        }
    }
}

struct BuyerViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        BuyerView(viewModel: .init(lastName: .init(placeholder: "Фамилия"), name: .init(placeholder: "Имя"), secondName: .init(placeholder: "Отчество"), email: .init(placeholder: "E-mail"), phone: .init(placeholder: "Телефон")))
    }
}
