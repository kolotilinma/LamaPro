//
//  ConfirmCartView.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI

struct ConfirmCartView: View {
    
    @ObservedObject var viewModel: ConfirmCartViewModel
    
    var body: some View {
        
        ZStack {
            
            ScrollView {
                
                VStack(spacing: 32) {
                    
                    PayerTypeView(viewModel: viewModel.payerType)
                    
                    DeliveryView(viewModel: viewModel.deliveryView)
                    
                    if let payment = viewModel.paymentView {
                        
                        PaymentView(viewModel: payment)
                    }
                    
                    if let buyer = viewModel.buyerView {
                        
                        BuyerView(viewModel: buyer)
                    }
                    
                    if let company = viewModel.company {
                        
                        OrganizationView(viewModel: company)
                    }
                    
                    if let orderButton = viewModel.orderButton {
                        
                        ButtonSimpleView(viewModel: orderButton)
                    }
                }
                .padding(16)
            }
            .resignKeyboardOnDragGesture()
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {

                if let link = viewModel.link  {

                    switch link {

                    case .success(let success):
                        SuccessView(viewModel: success)
                    }
                }
            }
            
        }
        .alert(item: $viewModel.alert, content: { alertViewModel in
            Alert(with: alertViewModel)
        })
        .onTapGesture { UIApplication.shared.endEditing() }
        .navigationBar(with: viewModel.navigationBar)
    }
}

struct ConfirmCartView_Previews: PreviewProvider {
    
    static var previews: some View {
        ScrollView {
            ConfirmCartView(viewModel: .init(model: .emptyMock, navigationBar: .init(title: .text("Оформление заказа")), payerType: .init(), deliveryView: .init(type: .delivery), paymentView: .init(type: .individual), buyerView: .init(lastName: .init(placeholder: "Фамилия"), name: .init(placeholder: "Имя"), secondName: .init(placeholder: "Отчество"), email: .init(placeholder: "E-mail"), phone: .init(placeholder: "Телефон")), orderButton: .init(title: "Оформить заказ", backgroundColor: .lamaGreen, action: { })))
        }
    }
}
