//
//  ProfileViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI
import Combine

class ProfileViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigationBar: NavigationBarView.ViewModel
    @Published var alert: Alert.ViewModel?
    
    @Published var header: ProfileHeaderViewModel?
    @Published var userData: ProfilePersonalDataViewModel?
    @Published var logoutButton: ButtonSimpleView.ViewModel?
    @Published var saveDataButton: ButtonSimpleView.ViewModel?
    @Published var savePassButton: ButtonSimpleView.ViewModel?

    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, navigationBar: NavigationBarView.ViewModel, header: ProfileHeaderViewModel?, userData: ProfilePersonalDataViewModel?, logoutButton: ButtonSimpleView.ViewModel? = nil, saveDataButton: ButtonSimpleView.ViewModel? = nil, savePassButton: ButtonSimpleView.ViewModel? = nil) {
        
        self.model = model
        self.navigationBar = navigationBar
        self.header = header
        self.userData = userData
        self.logoutButton = logoutButton
        self.saveDataButton = saveDataButton
        self.savePassButton = savePassButton
    }
    
    convenience init(model: Model, backAction: @escaping () -> Void) {
        self.init(model: model, navigationBar: .init(title: .text("Профиль")), header: nil, userData: nil)
        
        
        self.logoutButton = .init(title: "Выйти из аккаунта", backgroundColor: .lamaGreen, height: 50, action: { [weak self] in
            self?.model.handleAuthLogout()
        })
        
        self.saveDataButton = .init(title: "Сохранить",backgroundColor: .lamaGreenDark, height: 50, action: { [weak self] in
            
            self?.model.action.send(ModelAction.UpdateAction.UserData(user: (name: self?.userData?.name.textField.text,
                                                                             secondName: self?.userData?.secondName.textField.text,
                                                                             lastName: self?.userData?.lastName.textField.text,
                                                                             phone: self?.userData?.phone.textField.text,
                                                                             email: self?.userData?.email.textField.text)))
        })
        
        
        self.savePassButton = .init(title: "Изменить пароль",backgroundColor: .lamaGreenDark, height: 50, action: { [weak self] in
            
            self?.action.send(ProfileViewModelAction.ChangePassword())
            
//            if self?.userData?.password2Send.textField.text == self?.userData?.password2Check.textField.text {
//                if let pass = self?.userData?.password2Send.textField.text {
//                    self?.model.action.send(ModelAction.UpdateAction.Password(password: pass))
//                }
//            } else {
//                //сброс ошибки
//            }
        })
        
        self.navigationBar.leftButtons = [NavigationBarView.ViewModel.BackButtonViewModel(icon: Image(systemName: "chevron.backward"), action: backAction)]
        
        bind()
    }
    
    func bind() {
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as ProfileViewModelAction.ChangePassword:
                    if userData?.password2Send.textField.text == userData?.password2Check.textField.text {
                        guard let pass = userData?.password2Send.textField.text else { return }
                        model.action.send(ModelAction.CoverAction.Spinner.Show())
                        
                        Task {
                            do {
                                
                                let result = try await model.handleAuthChangePassword(newPassword: pass)
                                model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                await MainActor.run {
                                    if result == true {
                                        userData?.password2Send.textField.text = nil
                                        userData?.password2Check.textField.text = nil
                                    }
                                }
                            } catch {
                                await MainActor.run {
                                    model.action.send(ModelAction.CoverAction.Spinner.Hide())
                                    alert = .init(title: "Ошибка", message: model.convertErrorToString(error), primary: .init(type: .default, title: "Ок", action: { [weak self] in
                                        self?.alert = nil
                                    }))
                                }
                            }
                        }
                        
                    } else {
                        
                        alert = .init(title: "Ошибка", message: "Пароли не совпадают", primary: .init(type: .default, title: "Ок", action: { [weak self] in
                            self?.alert = nil
                        }))
                    }
                    
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        model.user
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] user in
                guard let user = user else { return }
                
                header = .init(user: user)
                userData = .init(user: user, profileViewModel: self)
                
            }
            .store(in: &bindings)
    }
}

enum ProfileViewModelAction {
    
    struct ChangePassword: Action { }
}
