//
//  ProfileView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

struct ProfileView: View {
    
    @ObservedObject var viewModel: ProfileViewModel
    
    var body: some View {
        
        ScrollView {
            
            VStack(alignment: .leading) {
                
                if let header = viewModel.header,
                   let userData = viewModel.userData {
                    ProfileHeaderView(viewModel: header)
                    ProfilePersonalDataView(viewModel: userData)
                }
                
                if let button = viewModel.logoutButton {
                    
                    ButtonSimpleView(viewModel: button)
                        .padding(.horizontal, 40)
                        .padding(.bottom, 16)
                    
                }
                
            }
            .padding(.top, 17)
            .padding(.horizontal, 14)
        }
        .resignKeyboardOnDragGesture()
        .alert(item: $viewModel.alert, content: { alertViewModel in
            Alert(with: alertViewModel)
        })
        .navigationBar(with: viewModel.navigationBar)
    }
}

struct ProfileView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        NavigationView {
            
            ProfileView(viewModel: .init(model: .emptyMock, backAction: {}))
        }
    }
}
