//
//  ProfileHeaderViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 13.02.2023.
//

import SwiftUI

extension ProfileViewModel {
    
    class ProfileHeaderViewModel: ObservableObject {
        
        let fullname: String
        let lastAuth: String
        let registerDate: String
        let avatarUrl: URL?
        
        internal init(fullname: String, lastAuth: String, registerDate: String, avatarUrl: URL?) {
            
            self.fullname = fullname
            self.lastAuth = lastAuth
            self.registerDate = registerDate
            self.avatarUrl = avatarUrl
        }
        
        convenience init(user: UserData) {
            
            let fullname = "\(user.name) \(user.lastName)"
            
            self.init(fullname: fullname, lastAuth: user.lastLogin, registerDate: user.dateRegister, avatarUrl: nil)
            
        }
    }
}

extension ProfileView {
    
    struct ProfileHeaderView: View {
        
        @ObservedObject var viewModel: ProfileViewModel.ProfileHeaderViewModel
        
        var body: some View {
            
            VStack(alignment: .leading, spacing: 19) {
                
                HStack(spacing: 15) {
                    
                    Circle()
                        .frame(width: 80, height: 80)
                        .foregroundColor(.lamaGrayLight)
                    
                    VStack(alignment: .leading, spacing: 8) {
                        
                        Text(viewModel.fullname)
                            .font(.interBold(size: 20))
                        
                        VStack(alignment: .leading, spacing: 2) {
                            
                            Text("Последняя авторизация: ")
                                .font(.interBold(size: 10))
                            
                            + Text(viewModel.lastAuth)
                                .font(.interRegular(size: 10))
                            
                            Text("Аккаунт создан: ")
                                .font(.interBold(size: 10))
                            
                            + Text(viewModel.registerDate)
                                .font(.interRegular(size: 10))
                        }
                    }
                    .foregroundColor(.lamaBlack)
                    
                }
                
                HStack(spacing: 15) {
                    
                    Text("Ваша персональная скидка")
                        .font(.interBold(size: 13))
                    
                    Spacer()
                    
                    Text("0 %")
                        .font(.interBold(size: 20))
                }
                .foregroundColor(.lamaGreen)
                
                Divider()
            }
        }
    }
}

struct ProfileHeaderViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ProfileView.ProfileHeaderView(viewModel: .init(fullname: "Денис Иванов", lastAuth: "08.12.2022 18:21:54", registerDate: "25.10.2022",  avatarUrl: nil))
    }
}
