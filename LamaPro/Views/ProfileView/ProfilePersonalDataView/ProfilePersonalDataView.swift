//
//  ProfilePersonalDataView.swift
//  LamaPro
//
//  Created by user on 08.03.2023.
//

import SwiftUI


extension ProfileViewModel {
    
    class ProfilePersonalDataViewModel: ObservableObject {
        
        var profileViewModel: ProfileViewModel?
        
        @Published var password2Check: TextFieldView.ViewModel
        @Published var password2Send: TextFieldView.ViewModel
        @Published var lastName: TextFieldView.ViewModel
        @Published var name: TextFieldView.ViewModel
        @Published var secondName: TextFieldView.ViewModel
        @Published var email: TextFieldView.ViewModel
        @Published var phone: TextFieldView.ViewModel
        
        internal init(password2Check: TextFieldView.ViewModel, password2Send: TextFieldView.ViewModel, lastName: TextFieldView.ViewModel, name: TextFieldView.ViewModel, secondName: TextFieldView.ViewModel, email: TextFieldView.ViewModel, phone: TextFieldView.ViewModel, profileViewModel: ProfileViewModel?) {
            self.password2Check = password2Check
            self.password2Send = password2Send
            self.lastName = lastName
            self.name = name
            self.secondName = secondName
            self.email = email
            self.phone = phone
            self.profileViewModel = profileViewModel
        }
        
        convenience init(user: UserData, profileViewModel: ProfileViewModel?) {
            self.init(
                password2Check: .init(text: nil, placeholder: "Введите пароль", isSecureText: true),
                password2Send: .init(text: nil, placeholder: "Введите пароль", isSecureText: true),
                lastName: .init(text: user.lastName, placeholder: "Фамилия"),
                name: .init(text: user.name, placeholder: "Имя"),
                secondName: .init(text: user.secondName, placeholder: "Отчество"),
                email: .init(text: user.email, placeholder: "E-mail"),
                phone: .init(text: user.phone, placeholder: "Телефон"),
                profileViewModel: profileViewModel
                
            )
        }
    }
}

struct ProfilePersonalDataView: View {
    
    @ObservedObject var viewModel: ProfileViewModel.ProfilePersonalDataViewModel
    
    var body: some View {
        VStack {
            
            //MARK: Раздел "Контактные данные"
            customerDataBlock
                .padding(.bottom, 15)

            if let customerDataViewModel = viewModel.profileViewModel?.saveDataButton {
                ButtonSimpleView(viewModel: customerDataViewModel)
                    .frame(width: UIScreen.main.bounds.width * 0.5)
                    .padding(.bottom, 15)


            }
            
            passwordBlock
                .padding(.bottom, 15)
            
            if let passwordViewModel = viewModel.profileViewModel?.savePassButton {
                ButtonSimpleView(viewModel: passwordViewModel)
                    .frame(width: UIScreen.main.bounds.width * 0.5)
                    .padding(.bottom, 15)

            }
            
            //MARK: Раздел "Футер"
            footerBlock
                .padding(.bottom, 15)

        }
    }
}


struct ProfilePersonalDataView_Previews: PreviewProvider {
    static var previews: some View {
        ProfilePersonalDataView(viewModel: .init(user: UserData(id: "", name: "", lastName: "", phone: "", gender: "", login: "", email: "", lastLogin: "", dateRegister: ""), profileViewModel: .init(model: .emptyMock, backAction: {})))
    }
}


extension ProfilePersonalDataView {
    
    var customerDataBlock: some View {
        VStack {
            Text("Покупатель")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            TextFieldView(viewModel: viewModel.lastName)
            
            TextFieldView(viewModel: viewModel.name)
            
            TextFieldView(viewModel: viewModel.secondName)
            
            TextFieldView(viewModel: viewModel.email)
            
            TextFieldView(viewModel: viewModel.phone)
        }
    }
    
    var passwordBlock: some View {
        VStack {
            Text("Пароль")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            TextFieldView(viewModel: viewModel.password2Check)
            
            TextFieldView(viewModel: viewModel.password2Send)
        }
    }
    
    var footerBlock: some View {
        VStack {
            Text("Контакты")
                .font(.interBold(size: 17))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
            ProfileFooterView()
        }
    }
}

struct DatablockWidthTextfield<Content>: View where Content: View {
    var views: Content
    var header: String
    init(header: String, @ViewBuilder content: () -> Content) {
        self.header = header
        self.views = content()
    }
    
    var body: some View {
        VStack {
            Text(header)
                .font(.interBold(size: 20))
                .foregroundColor(.lamaBlack)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.bottom)
            Group {
                views.foregroundColor(.black)
            }
        }
    }
}
