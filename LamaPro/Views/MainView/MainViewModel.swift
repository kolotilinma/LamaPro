//
//  MainViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI
import Combine

class MainViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigation: NavigationBarView.ViewModel
    let searchBar: SearchBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    @Published var offersItem: OffersSectionViewModel?
    @Published var newItems: MainCatalogSectionViewModel?
    @Published var popularSections: PopularSectionsViewModel?
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, navigation: NavigationBarView.ViewModel) {
        
        self.model = model
        self.navigation = navigation
        self.searchBar = .init()
    }
    
    convenience init(model: Model) {
        
        self.init(model: model, navigation: .init(title: .empty, stase: .withLogo))
        
        self.navigation.rightButtons = [NavigationBarView.ViewModel.TextButtonViewModel(icon: Image("tabProfile"), text: "Профиль", action: { [weak self] in
            
            self?.action.send(MainViewModelAction.ProfileTapped())
        })]
        
        bind()
    }
    
    func bind() {
        
        model.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as TabBarAction.CloseAll:
                    link = nil
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as MainViewModelAction.ProfileTapped:
                    
                    switch model.auth.value {
                        
                    case .active:
                        self.link = .profile(ProfileViewModel(model: model, backAction: { [weak self] in
                            self?.link = nil
                        }))
                        
                    default :
                        self.link = .authentification(AuthentificationViewModel(model: model, backAction: { [weak self] in
                            self?.link = nil
                        }))
                    }
                    
                case let payload as MainViewModelAction.OpenItemDetail:
                    link = .itemDetail(ProductDetailViewModel(model: model, item: payload.item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                case let payload as  MainViewModelAction.OpenItemDetail:
                    link = .onOrder(OnOrderViewModel(model: model, item: payload.item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        searchBar.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as SearchBarViewAction.SearchText:
                    link = .search(SearchItemsViewModel(model, text: payload.text, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
        
//        searchBar.$searchText
//            .removeDuplicates()
//            .receive(on: DispatchQueue.main)
//            .sink { [unowned self] text in
//                let isEditing = searchBar.isEditing
//
//                withAnimation {
//
//                    if isEditing, text != "" {
//
//                        let filtered = model.catalogElements.value.filter({ $0.name.lowercased().contains(text.lowercased()) || $0.article.lowercased().contains(text.lowercased()) })
//                        let searchModel = SearchItemsViewModel(model, items: filtered)
//                        bind(searchModel)
//                        state = .isSearch(searchModel)
//
//                    } else {
//
//                        state = .normal
//                    }
//                }
//
//            }
//            .store(in: &bindings)
        
        
        model.catalogOffers
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] offers in
                offersItem = .init(items: offers.map({ OfferViewModel(host: model.host, item: $0) }))
            }
            .store(in: &bindings)
        
        model.catalogElements
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] elements in
                
                let newItems = elements.filter({ $0.isNew != nil })
                if newItems.isEmpty == false {
                    let newItemsViewModel: MainCatalogSectionViewModel = .init(model: model, newItems: newItems)
                    
                    self.newItems = newItemsViewModel
                    
                    bind(newItemsViewModel)
                }
                let popularSections = PopularSectionsViewModel()
                self.popularSections = popularSections
                bind(popularSections)
                
            }
            .store(in: &bindings)
        
    }
    
    @Sendable func refreshCatalog() async {
        
        await model.fetchCatalogData()
    }
    
    func bind(_ searchModel: SearchItemsViewModel) {
        searchModel.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                
                case let payload as SearchItemsViewModelAction.OnOrderTapped:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id }) else { return }
                    self.action.send( MainViewModelAction.OpenOnOrder(item: item))
                    
                case let payload as SearchItemsViewModelAction.OpenItemDetail:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id }) else { return }
                    self.action.send( MainViewModelAction.OpenItemDetail(item: item))
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    func bind(_ section: MainCatalogSectionViewModel) {
        
        section.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                
                case let payload as MainCatalogSectionViewModelAction.OpenItemDetail:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id }) else { return }
                    self.action.send( MainViewModelAction.OpenItemDetail(item: item))
                    
                case _ as MainCatalogSectionViewModelAction.OpenAllElements:
                    let newItems = model.catalogElements.value.filter({ $0.isNew != nil })
                    
                    link = .itemsList(ItemsListViewModel(model: model, newItems: newItems, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    func bind(_ section: PopularSectionsViewModel) {
        
        section.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                
                case let payload as PopularSectionsViewModelAction.ItemTapped:
                    
                    guard let item = model.catalogSections.value.first(where: { $0.id == payload.sectionId }) else { return }
                    
                    link = .catalogSections(CatalogSectionsListViewModel(model: model, item: item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                    
                default: break
                }
            }
            .store(in: &bindings)
    }
    
    
    enum State {
        
        case isSearch(SearchItemsViewModel)
        case normal
    }
    
    enum Link {
        
        case onOrder(OnOrderViewModel)
        case profile(ProfileViewModel)
        case itemDetail(ProductDetailViewModel)
        case catalogSections(CatalogSectionsListViewModel)
        case itemsList(ItemsListViewModel)
        case authentification(AuthentificationViewModel)
        case search(SearchItemsViewModel)
    }
    
}

enum MainViewModelAction {
    
    struct ProfileTapped: Action { }
    
    struct OpenItemDetail: Action {
        
        let item: CatElement
    }
    
    struct OpenOnOrder: Action {
        
        let item: CatElement
    }
}
