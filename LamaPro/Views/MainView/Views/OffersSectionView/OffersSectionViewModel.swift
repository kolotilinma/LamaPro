//
//  OffersSectionViewModel.swift
//  LamaPro
//
//  Created by Михаил on 05.03.2023.
//

import Foundation

class OffersSectionViewModel: ObservableObject {
    
    @Published var items: [OfferViewModel]
    
    internal init(items: [OfferViewModel]) {
        
        self.items = items
    }
    
}
