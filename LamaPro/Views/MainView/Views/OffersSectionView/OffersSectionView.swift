//
//  OffersSectionView.swift
//  LamaPro
//
//  Created by Михаил on 05.03.2023.
//

import SwiftUI

struct OffersSectionView: View {
    
    @ObservedObject var viewModel: OffersSectionViewModel
    
    var body: some View {
        
        ScrollView(.horizontal, showsIndicators: false) {
            
            HStack(spacing: 8) {
                
                ForEach(viewModel.items) { item in
                    
                    OfferView(viewModel: item)
                        .frame(width: UIScreen.main.bounds.width * 0.9)
                        
                }
                
            }
            .padding(.horizontal, 16)
            
        }
    }
}

struct OffersSectionView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        OffersSectionView(viewModel: .init(items: [
            .init(id: "1", title: "Профессиональный Японский инструмент", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/b49/b49a3f4193eeaf40230461b94e1c8ca8.jpg")),
            .init(id: "2", title: "Профессиональный Японский инструмент", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/b49/b49a3f4193eeaf40230461b94e1c8ca8.jpg"))]))
        .previewLayout(.sizeThatFits)
        
    }
}
