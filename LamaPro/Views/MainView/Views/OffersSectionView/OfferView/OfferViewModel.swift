//
//  OfferViewModel.swift
//  LamaPro
//
//  Created by Михаил on 05.03.2023.
//

import Foundation

class OfferViewModel: ObservableObject, Identifiable {
    
    let id: String
    let title: String
    let imageUrl: URL?
    
    internal init(id: String, title: String, imageUrl: URL? = nil) {
        
        self.id = id
        self.title = title
        self.imageUrl = imageUrl
    }
    
    convenience init(host: String, item: CatOffer) {
        
        self.init(id: item.id, title: item.name, imageUrl: URL(string: host + item.previewPicture))
    }
}
