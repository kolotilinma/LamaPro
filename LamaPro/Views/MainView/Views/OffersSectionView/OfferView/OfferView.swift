//
//  OfferView.swift
//  LamaPro
//
//  Created by Михаил on 05.03.2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct OfferView: View {
    
    @ObservedObject var viewModel: OfferViewModel
    
    var body: some View {
        
        GeometryReader { geometry in
            
            ZStack(alignment: .topLeading) {
                
                
                WebImage(url: viewModel.imageUrl)
                    .resizable()
                    .indicator(.activity)
                    .transition(.fade)
                    .scaledToFit()
                    .frame(width: geometry.size.width)
                
                Text(viewModel.title)
                    .font(.interRegular(size: 13))
                    .multilineTextAlignment(.leading)
                    .foregroundColor(.lamaWhite)
                    .padding(21)
                    .frame(width: geometry.size.width / 1.9)
                
            }
            .cornerRadius(22)
            
        }
        .frame(height: 110)
    }
}

struct OfferView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        OfferView(viewModel: .init(id: "1", title: "Профессиональный Японский инструмент", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/b49/b49a3f4193eeaf40230461b94e1c8ca8.jpg")))
            .previewLayout(.sizeThatFits)
    }
}
