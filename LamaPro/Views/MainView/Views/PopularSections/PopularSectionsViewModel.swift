//
//  PopularSectionsViewModel.swift
//  LamaPro
//
//  Created by Михаил on 20.03.2023.
//

import SwiftUI
import Combine

class PopularSectionsViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var items: [PopularItemView.ViewModel]
    
    internal init(items: [PopularItemView.ViewModel]) {
        
        self.items = items
    }
    
    convenience init() {
        
        self.init(items: [])
        self.items = createItems()
    }
    
    func createItems() -> [PopularItemView.ViewModel] {
        
        return [
            .init(id: "21", title: "Контейнеры", image: Image("контейнеры2"), action: { [weak self] id in
                
                self?.action.send(PopularSectionsViewModelAction.ItemTapped(sectionId: id))
            }),
            .init(id: "16", title: "Агротекстиль", image: Image("Агротекстиль"), action: { [weak self] id in
                
                self?.action.send(PopularSectionsViewModelAction.ItemTapped(sectionId: id))
            }),
            .init(id: "52", title: "Инструмент", image: Image("Инструмент"), action: { [weak self] id in
                
                self?.action.send(PopularSectionsViewModelAction.ItemTapped(sectionId: id))
            }),
            .init(id: "40", title: "Удобрения", image: Image("banner021"), action: { [weak self] id in
                
                self?.action.send(PopularSectionsViewModelAction.ItemTapped(sectionId: id))
            })
        ]
        
    }
}

enum PopularSectionsViewModelAction {
    
    struct ItemTapped: Action {
        
        let sectionId: String
    }
    
}

