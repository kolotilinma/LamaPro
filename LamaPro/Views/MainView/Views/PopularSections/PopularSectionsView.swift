//
//  PopularSectionsView.swift
//  LamaPro
//
//  Created by Михаил on 20.03.2023.
//

import SwiftUI

struct PopularSectionsView: View {
    
    @ObservedObject var viewModel: PopularSectionsViewModel
    
    var columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 12) {
            
            Text("Популярные разделы")
                .font(.interBold(size: 20))
                .foregroundColor(.lamaBlack)
            
            LazyVGrid(columns: columns) {
                
                ForEach(viewModel.items) { item in
                    
                    PopularItemView(viewModel: item)
                }
            }
        }
    }
}

struct PopularSectionsView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        PopularSectionsView(viewModel: .init(items: [
            .init(id: "1", title: "Контейнеры", image: Image("контейнеры2"), action: { _ in }),
            .init(id: "2", title: "Агротекстиль", image: Image("Агротекстиль"), action: { _ in }),
            .init(id: "3", title: "Инструмент", image: Image("Инструмент"), action: { _ in }),
            .init(id: "4", title: "Удобрения", image: Image("banner021"), action: { _ in })
        ]))
        .padding()
    }
}
