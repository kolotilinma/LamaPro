//
//  PopularItemViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 20.03.2023.
//

import SwiftUI

extension PopularItemView {
    
    class ViewModel: ObservableObject, Identifiable {
        
        let id: String
        @Published var title: String
        @Published var image: Image
        
        let action: (ViewModel.ID) -> Void
        
        internal init(id: String, title: String, image: Image, action: @escaping (ViewModel.ID) -> Void) {
            
            self.id = id
            self.title = title
            self.image = image
            self.action = action
        }
        
    }
    
    
}


struct PopularItemView: View {

    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        Button {
            viewModel.action(viewModel.id)
        } label: {
          
            ZStack(alignment: .bottomLeading) {
                
                viewModel.image
                    .resizable()
                    .scaledToFill()
                    
//                    .clipped()
                
                LinearGradient(colors: [.lamaBlack.opacity(0), .lamaBlack.opacity(0.12)], startPoint: .init(x: 0.5, y: 0.5), endPoint: .init(x: 0.5, y: 0.8))
                
                LinearGradient(colors: [.lamaBlack.opacity(0), .lamaBlack.opacity(0.75)], startPoint: .init(x: 0.5, y: 0.5), endPoint: .init(x: 0.5, y: 0.9))
                
                Text(viewModel.title)
                    .font(.interSemiBold(size: 13))
                    .foregroundColor(.lamaWhite)
                    .padding(8)
            }
            
        }
        .cornerRadius(10)
    }
}

struct PopularItemViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        
        PopularItemView(viewModel: .init(id: "1", title: "Агротекстиль", image: Image("контейнеры2"), action: { _ in }))
            .frame(width: 150, height: 88)
    }
}
