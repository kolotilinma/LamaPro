//
//  MainProductItemView.swift
//  LamaPro
//
//  Created by Михаил on 19.03.2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct MainProductItemView: View {
    
    @ObservedObject var viewModel: MainProductItemViewModel
    
    @State var tapped: Bool = false
    @State private var buttonTypeId = 0
    
    var body: some View {
        
        HStack( spacing: 8) {
            
            WebImage(url: viewModel.imageUrl)
                .resizable()
                .indicator(.activity)
                .transition(.fade)
                .scaledToFit()
                .frame(width: 96, height: 96, alignment: .center)
                .onTapGesture {
                    viewModel.itemTapped()
                }
            
            VStack(alignment: .leading, spacing: 8) {
                
                VStack(alignment: .leading, spacing: 8) {
                    
                    Text(viewModel.title)
                        .font(.interBold(size: 10))
                        .foregroundColor(.lamaBlack)
                        .lineLimit(3)
                        .multilineTextAlignment(.leading)
                    
                    Text(viewModel.price)
                        .font(.interBold(size: 13))
                        .foregroundColor(.lamaBlack)
                }
                .onTapGesture {
                    viewModel.itemTapped()
                }
                
                HStack {
                    
                    switch viewModel.buttonState {
                        
                    case .initial:
                        EmptyView()
                            .frame(height: 30)
                            .onAppear {
                                buttonTypeId = 0
                            }
                            .animation(.spring(), value: buttonTypeId)
                        
                    case .addButton(let addButton):
                        ButtonSimpleView(viewModel: addButton)
                            .onAppear {
                                buttonTypeId = 1
                            }
                            .animation(.spring(), value: buttonTypeId)
                        
                    case .countButton(let count):
                        ToCartButtonView(viewModel: count)
                            .onAppear {
                                buttonTypeId = 2
                            }
                            .animation(.spring(), value: buttonTypeId)
                    }
                    
                    Spacer()
                }
                
            }
            .frame(maxWidth: .infinity)
        }
        .cornerRadius(10, corners: [.topLeft, .bottomLeft])
        .frame(width: 234)
    }
}

struct MainProductItemView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        MainProductItemView(viewModel: .init(model: .emptyMock, id: "1", title: "Бур садовый Бур садовый Бур садовый Бур садовый", code: "Артикул: Lt001272", price: "5 954.70 ₽", imageUrl: URL(string: "https://lama-pro.ru/upload/iblock/c1f/d1ubgz30h8crwjogrzdjr6nb9z86p1ls.jpg"), buttonState: .addButton(.init(title: "В корзину", action: { }))))
            .padding()
//            .previewLayout(.fixed(width: 140, height: 200))
    }
}
