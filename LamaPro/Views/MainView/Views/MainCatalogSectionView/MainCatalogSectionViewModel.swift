//
//  MainCatalogSectionViewModel.swift
//  LamaPro
//
//  Created by Михаил on 19.03.2023.
//


import Foundation
import Combine

class MainCatalogSectionViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var items: [MainProductItemViewModel]
    
    let id: String
    let title: String
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, id: String, items: [MainProductItemViewModel], title: String) {
        
        self.model = model
        self.id = id
        self.items = items
        self.title = title
    }
    
    
    convenience init(model: Model, newItems: [CatElement]) {
        
        self.init(model: model, id: UUID().uuidString, items: [], title: "Новинки")
        
        items = createItems(from: newItems)
        
    }
    
    func createItems(from newItems: [CatElement]) -> [MainProductItemViewModel] {
          
        let items = newItems.map { MainProductItemViewModel(model: model, item: $0) }
        bind(items)
        return items
    }
    
    func bind(_ items: [MainProductItemViewModel]) {
        for item in items {
            
            item.action
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] action in
                    switch action {

//                    case let payload as ProductItemViewModelAction.OnOrderTapped:
//                        self.action.send(ItemsListViewModelAction.OpenOnOrder(id: payload.id))
                        
                    case let payload as ProductItemViewModelAction.ItmemTapped:
                        self.action.send(MainCatalogSectionViewModelAction.OpenItemDetail(id: payload.id))
                        
                    default: break
                    }
                }
                .store(in: &bindings)
            
        }
    }
    
    func openAllElements() {
        
        action.send(MainCatalogSectionViewModelAction.OpenAllElements())
    }
    
}

enum MainCatalogSectionViewModelAction {
    
    struct OpenAllElements: Action { }
    
    struct OpenItemDetail: Action {
        let id: String
    }
}
