//
//  MainProductItemViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.03.2023.
//

import SwiftUI
import Combine

class MainProductItemViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    let id: String
    let title: String
    let code: String
    let imageUrl: URL?
    
    @Published var price: String
    @Published var buttonState: ButtonState
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, id: String, title: String, code: String, price: String, imageUrl: URL?, buttonState: ButtonState) {
        
        self.model = model
        self.id = id
        self.title = title
        self.code = code
        self.price = price
        self.imageUrl = imageUrl
        self.buttonState = buttonState
    }
    
    convenience init(model: Model, item: CatElement) {
        
        self.init(model: model, id: item.id, title: item.name, code: "Артикул: \(item.article)", price: item.price?.priceString ?? "", imageUrl: URL(string: model.host + item.previewPicture), buttonState: .initial)
        
        if let count = model.cartElements.value.items.first(where: {$0.elementId == id})?.count {
            
            createButton(item: item, count: count)
            
        } else {
            
            createButton(item: item, count: 0)
        }
        bind()
    }
    
    func bind() {
        
        model.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as ModelAction.Cart.Update:
                    
                    guard let item = model.catalogElements.value.first(where: { $0.id == id }) else { return }
                    
                    if payload.id != id {
                        
                        if let count = model.cartElements.value.items.first(where: {$0.elementId == id})?.count {
                            
                            createButton(item: item, count: count)
                            
                        } else {
                            
                            createButton(item: item, count: 0)
                        }
                        
                    } else {
                        
                        if payload.count == 0 {
                            
                            createButton(item: item, count: 0)
                        }
                    }
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                 
                case _ as ProductItemViewModelAction.AddToCart:
                    
                    guard let item = model.catalogElements.value.first(where: { $0.id == id }) else { return }
                    
                    withAnimation {
                        let countButton = ToCartButtonView.ViewModel(itemsCount: 1, maxCount: item.quantity)
                        self.buttonState = .countButton(countButton)
                        self.bind(countButton)
                    }
                    
                default: break
                }
            }
            .store(in: &bindings)
        
    }
    
    func bind(_ countButton: ToCartButtonView.ViewModel) {
        
        countButton.$itemsCount
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] itemsCount in
                guard let item = model.catalogElements.value.first(where: { $0.id == id }) else { return }
                
                model.action.send(ModelAction.Cart.Update(id: item.id, count: itemsCount))
                
                if itemsCount == 0 {
                    
                    price = item.setupPrice(itemsCount: 1)

                } else {
                    
                    price = item.setupPrice(itemsCount: itemsCount)
                }
            }
            .store(in: &bindings)
    }
    
    func createButton(item: CatElement, count: Int) {
        
        withAnimation {
            
            if item.quantity == 0 {
                
                buttonState = .addButton(ButtonSimpleView.ViewModel(title: "Под заказ", backgroundColor: .lamaGreenDark, action: { [weak self] in
                    
                    self?.OnOrderTapped()
                }))
                
            } else {
                
                if count != 0 {
                    
                    buttonState = .addButton(ButtonSimpleView.ViewModel(title: "В корзине", icon: Image(systemName: "checkmark"), backgroundColor: .lamaGreen, action: { [weak self] in
                        
                        withAnimation {
                            let countButton = ToCartButtonView.ViewModel(itemsCount: count, maxCount: item.quantity)
                            self?.buttonState = .countButton(countButton)
                            self?.bind(countButton)
                        }
                    }))
                    
                } else {
               
                    buttonState = .addButton(ButtonSimpleView.ViewModel(title: "В корзину", backgroundColor: .lamaGreen, action: { [weak self] in
                        self?.action.send(ProductItemViewModelAction.AddToCart(item: item, count: 1))
                        
                    }))
                }
            }
        }
    }
    
    func OnOrderTapped() {
        
        action.send(ProductItemViewModelAction.OnOrderTapped(id: id))
    }
    
    func itemTapped() {
        
        action.send(ProductItemViewModelAction.ItmemTapped(id: id))
    }
    
    
    enum ButtonState {
        
        case initial
        case addButton(ButtonSimpleView.ViewModel)
        case countButton(ToCartButtonView.ViewModel)
        
    }
}
