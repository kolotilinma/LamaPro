//
//  MainCatalogSectionView.swift
//  LamaPro
//
//  Created by Михаил on 19.03.2023.
//

import SwiftUI

struct MainCatalogSectionView: View {
    
    @ObservedObject var viewModel: MainCatalogSectionViewModel
    
    let screen = UIScreen.main.bounds
    
    var body: some View {
        
        VStack(spacing: 12) {
            
            HStack(alignment: .top) {
                
                Text(viewModel.title)
                    .font(.interBold(size: 20))
                    .foregroundColor(.lamaBlack)
                
                Spacer()
                
                Button(action: viewModel.openAllElements) {
                    
                    HStack(spacing: 0) {
                        
                        Text("Больше")
                            .font(.interSemiBold(size: 13))
                            .foregroundColor(.lamaBlack)
                        
                        Image.ic24chevronRight
                            .resizable()
                            .frame(width: 24, height: 24)
                    }
                }
                
            }
            .padding(.leading, 16)
            .padding(.trailing, 7)
            
            ScrollView(.horizontal, showsIndicators: false) {
                
                HStack(spacing: 8) {
                    
                    ForEach(viewModel.items) { item in
                        
                        MainProductItemView(viewModel: item)
                            
                    }
                    
                }
                .padding(.horizontal, 16)
            }
            
            
        }
    }
}

struct MainCatalogSectionView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        MainCatalogSectionView(viewModel: .init(model: .emptyMock, id: "1", items: [
            .init(model: .emptyMock, id: "1", title: "Бур садовый", code: "Артикул: Lt001111", price: "5 554.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "В корзину", action: { }))),
            .init(model: .emptyMock, id: "2", title: "Бур дачный", code: "Артикул: Lt001222", price: "5 154.70 ₽", imageUrl: nil, buttonState: .addButton(.init(title: "В корзину", action: { }))),
            .init(model: .emptyMock, id: "3", title: "Бур дворовый", code: "Артикул: Lt001333", price: "5 954.70 ₽", imageUrl: nil, buttonState: .countButton(.init(itemsCount: 2, maxCount: 5)))
        ], title: "Агроткань застилочная"))
    }
}
