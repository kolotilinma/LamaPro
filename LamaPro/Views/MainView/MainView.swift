//
//  MainView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

struct MainView: View {
    
    @ObservedObject var viewModel: MainViewModel
    
    private let screen = UIScreen.main.bounds
    
    var body: some View {
        
        ZStack {
            
            ScrollView {
                
                VStack(spacing: 10) {
                    
                    SearchBarView(viewModel: viewModel.searchBar)
                        .padding(.horizontal, 16)
                        .padding(.top, 16)
                    
                    VStack(spacing: 8) {
                        
                        CarouselView(itemHeight: (screen.width - 32)/2.17, views: [
                            Image("banner6")
                                .resizable()
                                .scaledToFit()
                                .cornerRadius(10),
                            
                            Image("banner03")
                                .resizable()
                                .scaledToFit()
                                .cornerRadius(10),
                            
                            Image("banner04")
                                .resizable()
                                .scaledToFit()
                                .cornerRadius(10)
                            
                        ])
                        
                        if let newItemsCatalog = viewModel.newItems {
                            
                            MainCatalogSectionView(viewModel: newItemsCatalog)
                        }
                        if let popularSections = viewModel.popularSections {
                            
                            PopularSectionsView(viewModel: popularSections)
                                .padding(.horizontal, 16)
                        }
                    }
                }
            }
            .refreshable(action: viewModel.refreshCatalog)
            .onTapGesture {
                viewModel.searchBar.clear()
                viewModel.searchBar.stopEditing()
            }
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                
                if let link = viewModel.link  {
                    
                    switch link {
                        
                    case .search(let search):
                        SearchItemsView(viewModel: search)
                        
                    case .itemDetail(let model):
                        ProductDetailView(viewModel: model)
                        
                    case .onOrder(let onOrder):
                        OnOrderView(viewModel: onOrder)
                        
                    case .itemsList(let model):
                        ItemsListView(viewModel: model)
                        
                    case .catalogSections(let catalogSections):
                        CatalogSectionsListView(viewModel: catalogSections)
                        
                    case .profile(let profile):
                        ProfileView(viewModel: profile)
                        
                    case .authentification(let model):
                        AuthentificationView(viewModel: model)
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigation)
    }
}

struct MainView_Previews: PreviewProvider {
    
    static var previews: some View {
        NavigationView {
            MainView(viewModel: .init(model: .emptyMock))
        }
    }
}
