//
//  FavoritesView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

struct FavoritesView: View {
    
    @ObservedObject var viewModel: FavoritesViewModel

    var columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
    
    var body: some View {
        
        ZStack {
            
            switch viewModel.status {
            case.empty:
                VStack {
                    Spacer()
                    
                    Image("EmptyFav")
                    
                    Spacer()
                }
            case .normal:
                
                ScrollView {
                    
                    LazyVGrid(columns: columns) {
                        
                        ForEach(viewModel.items) { item in
                            
                            ProductItemView(viewModel: item)

                        }
                    }
                    .padding([.leading, .trailing], 4)
                    .padding([.top, .bottom], 8)
                }
                .resignKeyboardOnDragGesture()
                
            case .loading:
                VStack {
                    Spacer()
                    
                    Text("Подождите, идет загрузка каталога...")
                        .font(.interBold(size: 13))
                        .foregroundColor(.lamaBlack)
                    
                    AnimatedHUDView()
                        .frame(width: 40, height: 40)
                    
                    Spacer()
                }
            case .error:
                VStack {
                    Spacer()
                    
                    Text("Ошибка")
                        .font(.interBold(size: 13))
                        .foregroundColor(.lamaBlack)
                    
                    Spacer()
                }
            }
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {

                if let link = viewModel.link  {

                    switch link {

                    case .itemDetail(let model):
                        ProductDetailView(viewModel: model)
                    default:
                        EmptyView()
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigation)
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView(viewModel: FavoritesViewModel(model: .emptyMock, navigation: .init(title: .text("Избранное")), searchBar: .init(), status: .normal, items: [
            .init(model: .emptyMock, id: "1", title: "Example", code: "123123", price: "1231.22 р", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
            .init(model: .emptyMock, id: "2", title: "Example2", code: "35453", price: "3561.22 р", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {}))),
            .init(model: .emptyMock, id: "3", title: "Example3", code: "465736", price: "2561.22 р", imageUrl: nil, buttonState: .addButton(.init(title: "Под заказ", backgroundColor: .lamaGreenDark,  action: {})))
        ]))
    }
}
