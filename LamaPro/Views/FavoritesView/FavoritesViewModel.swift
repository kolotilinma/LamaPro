//
//  FavoritesViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import Foundation
import Combine
import SwiftUI

class FavoritesViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigation: NavigationBarView.ViewModel
    @Published var searchBar: SearchBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false

    @Published var status: Status
    @Published var items: [ProductItemViewModel]

    let model: Model
    private var bindings = Set<AnyCancellable>()
    private var favoritesElements: FavoritesElements = .init(elementsId: [])
    private var catalogItem: [CatElement] = []
    
    internal init(model: Model, navigation: NavigationBarView.ViewModel, searchBar: SearchBarView.ViewModel, status: Status, items: [ProductItemViewModel]) {
        
        self.model = model
        self.navigation = navigation
        self.searchBar = searchBar
        self.status = status
        self.items = items
    }
    
    convenience init(model: Model) {
        
        self.init(model: model, navigation: .init(title: .text("Избранное")), searchBar: .init(), status: .loading, items: [])
        
        bind()
    }
    
    func bind() {
        
        model.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as TabBarAction.CloseAll:
                    link = nil
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as CatalogSectionViewModelAction.OpenItemDetail:
                    guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                    
                    link = .itemDetail(ProductDetailViewModel(model: model, item: item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default : break
                }
            }
            .store(in: &bindings)
        
        
        model.favoritesElements
            .combineLatest(model.catalogElements, model.catalogStatus)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] favoritesElements, catalogElements, catalogStatus in
                withAnimation {
                    switch catalogStatus {
                        
                    case .done:
                        if favoritesElements.elementsId.isEmpty == false {
                            
                            status = .normal

                            let favoritArr = catalogElements.filter { favoritesElements.elementsId.contains($0.id )}
                            let favoritViewModel = favoritArr.map { ProductItemViewModel(model: model, item: $0) }
                            
                            items = favoritViewModel
                            bind(favoritViewModel)
                            
                        } else {
                            
                            status = .empty
                        }
                        
                    case .loading:
                        status = .loading
                        
                    case .error(_):
                        status = .error
                    }
                }
            }
            .store(in: &bindings)

    }
    
    func bind(_ items: [ProductItemViewModel]) {
        for item in items {
            
            item.action
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] action in
                    switch action {
                     
                    case let payload as ProductItemViewModelAction.ItmemTapped:
                        self.action.send(CatalogSectionViewModelAction.OpenItemDetail(id: payload.id))
                        
                        
                    case let payload as ProductItemViewModelAction.FavoriteItmemTapped:
                        model.action.send(ModelAction.Favorites.Update(id: payload.id))
                        
                    default: break
                    }
                }
                .store(in: &bindings)
            
        }
    }
    
    enum State {
        
        case isSearch(SearchItemsViewModel)
        case normal
    }
    
    enum Status {
        
        case loading
        case empty
        case error
        case normal
    }
    
    enum Link {
        
        case catalogSections(CatalogSectionsListViewModel)
        case itemsList(ItemsListViewModel)
        case itemDetail(ProductDetailViewModel)
    }
}
enum FavoritesViewModelAction {
    
    struct OpenItemDetail: Action {
        let id: String
    }
}
