//
//  MainTabBarViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI
import Combine

class MainTabBarViewModel: ObservableObject {
    
    @Published var selectedTab = 1
    @Published var cartBageCount = 0
    @Published var favoritesBageCount = 0
    
    let main: MainViewModel
    let notifications: NotificationsViewModel
    let catalog: CatalogViewModel
    let cart: CartViewModel
    let favorites: FavoritesViewModel
    
    let model: Model
    
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model) {
        
        self.model = model
        self.main = .init(model: model)
        self.notifications = .init(model: model)
        self.catalog = .init(model: model)
        self.cart = .init(model: model)
        self.favorites = .init(model: model)
        
        bind()
    }
    
    func bind() {
        
        $selectedTab
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] selectedTab in
                model.action.send(TabBarAction.CloseAll())
            }
            .store(in: &bindings)
        
        model.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case let payload as TabBarAction.ChangeTab:
                    selectedTab = payload.selectedTab
                    
                    
                default:
                    break
                }
            }
            .store(in: &bindings)
    
        model.cartElements
            .combineLatest(model.catalogElements)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] cartElements, _ in
//                var count: Int = 0
//
//                cartElements.items.forEach { cartElement in
//                    if model.catalogElements.value.first(where: { $0.id == cartElement.elementId }) != nil {
//
//                        count += cartElement.count
//                    }
//                }
                cartBageCount = cartElements.items.count
            }
            .store(in: &bindings)
        
        model.favoritesElements
            .combineLatest(model.catalogElements)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] favorites, _ in
                
                favoritesBageCount = favorites.elementsId.count
            }
            .store(in: &bindings)
    }
}

//MARK: - TabBar Actions
enum TabBarAction {

    struct ChangeTab: Action {
        
        let selectedTab: Int
    }
    
    struct CloseAll: Action { }
    
}
