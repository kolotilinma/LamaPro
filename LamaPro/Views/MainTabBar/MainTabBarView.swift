//
//  MainTabBarView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI
import Combine

struct MainTabBarView: View {
        
    init(viewModel: MainTabBarViewModel) {
        self.viewModel = viewModel
        setupTabBar()
    }
    
    @ObservedObject var viewModel: MainTabBarViewModel
    
    var body: some View {
        
        TabView(selection: $viewModel.selectedTab) {
            
            NavigationView {
                
                MainView(viewModel: viewModel.main)
            }.tabItem("Главная",
                      imageName: "tabMain",
                      index: 1,
                      selection: $viewModel.selectedTab)
            
            NavigationView {
                
                NotificationsView(viewModel: viewModel.notifications)
            }.tabItem("Уведомления",
                      imageName: "tabNotification",
                      index: 2,
                      selection: $viewModel.selectedTab)
            
            NavigationView {
                
                CatalogView(viewModel: viewModel.catalog)
            }.tabItem("Каталог",
                      imageName: "tabCatalog",
                      index: 3,
                      selection: $viewModel.selectedTab)
            
            NavigationView {
                
                CartView(viewModel: viewModel.cart)
            }.tabItem("Корзина",
                      imageName: "tabCart",
                      index: 4,
                      selection: $viewModel.selectedTab)
            .badge(viewModel.cartBageCount)
            
            NavigationView {
                
                FavoritesView(viewModel: viewModel.favorites)
            }.tabItem("Избранное",
                      imageName: "tabFavorite",
                      index: 5,
                      selection: $viewModel.selectedTab)
            .badge(viewModel.favoritesBageCount)
        }
        .background(Color.lamaWhite)
        .accentColor(.lamaGreen)
    }
}

struct MainTabBarView_Previews: PreviewProvider {
    
    static var previews: some View {
        Group {
            
            MainTabBarView(viewModel: .init(model: .emptyMock))
         
        }
    }
}

extension MainTabBarView {
    
    func setupTabBar() {
        
        let image = UIImage.gradientImageWithBounds(
            bounds: CGRect( x: 0, y: 0, width: UIScreen.main.scale, height: 15),
            colors: [
                UIColor.clear.cgColor,
                UIColor.black.withAlphaComponent(0.05).cgColor
            ]
        )
        
        let appearance = UITabBarAppearance()
        appearance.configureWithDefaultBackground()
        appearance.backgroundColor = Color.lamaWhite.uiColor()
                
        appearance.backgroundImage = UIImage()
        appearance.shadowImage = image

        UITabBar.appearance().standardAppearance = appearance
    }
}

public struct TabViewItem<SelectionValue>: ViewModifier where SelectionValue: Hashable {
    @Binding private var selectedIndex: SelectionValue
    private let index: SelectionValue
    private let text: String
    private let imageName: String

    public init(selectedIndex: Binding<SelectionValue>, index: SelectionValue, text: String, imageName: String) {
        self._selectedIndex = selectedIndex
        self.index = index
        self.text = text
        self.imageName = imageName
    }

    public func body(content: Content) -> some View {
        content
            .tabItem {
                image
                    
                Text(text)
                    .font(Font.interRegular(size: 13))
            }
            .tag(index)
    }

    private var image: some View {
        guard selectedIndex == index else { return Image(imageName) }
        return Image(self.imageName)
    }
}

private extension View {
    
    func tabItem<Selection>(_ text: String, imageName: String, index: Selection, selection: Binding<Selection>) -> some View where Selection: Hashable {
        modifier(TabViewItem(selectedIndex: selection, index: index, text: text, imageName: imageName))
    }
}
