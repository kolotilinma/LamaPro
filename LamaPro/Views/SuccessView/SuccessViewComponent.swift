//
//  SuccessViewComponent.swift
//  LamaPro
//
//  Created by Михаил on 28.03.2023.
//

import SwiftUI

extension SuccessView {
    
    final class ViewModel: ObservableObject {
        
        @Published var navigationBar: NavigationBarView.ViewModel
        
        let title: String
        let subTitle: String
        @Published var successButton: ButtonSimpleView.ViewModel?
        
        internal init(navigationBar: NavigationBarView.ViewModel, title: String, subTitle: String, successButton: ButtonSimpleView.ViewModel? = nil) {
            
            self.navigationBar = navigationBar
            self.title = title
            self.subTitle = subTitle
            self.successButton = successButton
        }
        
        convenience init(title: String, subTitle: String, action: @escaping () -> Void) {
            self.init(navigationBar: .init(title: .text("")), title: title, subTitle: subTitle)
            
            successButton = .init(title: "Спасибо, понятно", backgroundColor: .lamaGreen, action: action)
        }
        
    }
    
}

struct SuccessView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        
        VStack(spacing: 40) {
            
            Spacer()
            
            Image("Success")
                .padding(.bottom, 6)
            
            VStack(alignment: .center, spacing: 25) {
                
                Text(viewModel.title)
                    .font(.interBold(size: 20))
                    .foregroundColor(.lamaBlack)
                
                Text(viewModel.subTitle)
                    .font(.interRegular(size: 16))
                    .foregroundColor(.lamaBlack)
                    .multilineTextAlignment(.center)
            }
            
            if let button = viewModel.successButton {
                
                ButtonSimpleView(viewModel: button)
                    .padding(.horizontal, 70)
            }
            
            Spacer()
        }
        .padding(16)
        .navigationBar(with: viewModel.navigationBar)
    }
}

struct SuccessViewComponent_Previews: PreviewProvider {
    
    static var previews: some View {
        NavigationView {
            
            SuccessView(viewModel: .init(navigationBar: .init(title: .text("")), title: "Заказ успешно оформлен!", subTitle: "Скоро наш менеджер свяжется с Вами для уточнения деталей", successButton: .init(title: "Спасибо, понятно", backgroundColor: .lamaGreen, action: {})))
        }
    }
}
