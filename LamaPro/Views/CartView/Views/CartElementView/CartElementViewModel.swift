//
//  CartElementViewModel.swift
//  LamaPro
//
//  Created by Михаил on 25.03.2023.
//

import Foundation
import Combine

class CartElementViewModel: ObservableObject, Identifiable {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    let id: String
    let title: String
    @Published var price: String
    let imageUrl: URL?
    
    @Published var countButton: ToCartButtonView.ViewModel?
    
    private let model: Model
    private var bindings = Set<AnyCancellable>()
    
    init(model: Model, id: String, title: String, price: String, imageUrl: URL?, countButton: ToCartButtonView.ViewModel?) {
        
        self.model = model
        self.id = id
        self.title = title
        self.price = price
        self.imageUrl = imageUrl
        self.countButton = countButton
    }
    
    convenience init?(model: Model, itemCart: CartData.Item) {
        
        guard let item = model.catalogElements.value.first(where: { $0.id == itemCart.elementId }) else { return nil }
        
        self.init(model: model, id: item.id, title: item.name, price: Self.totalPrice(from: item, with: itemCart.count), imageUrl: URL(string: model.host + item.previewPicture), countButton: nil)
        
        countButton = createButton(itemCart: itemCart)
        bind()
    }
    
    func bind() {
        
    }
    
    func bind(_ countButton: ToCartButtonView.ViewModel) {
        
        countButton.$itemsCount
            .dropFirst()
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] itemsCount in
                guard let item = model.catalogElements.value.first(where: { $0.id == id }) else { return }
                
                model.action.send(ModelAction.Cart.Update(id: item.id, count: itemsCount))
                
                self.price = Self.totalPrice(from: item, with: itemsCount)
            }
            .store(in: &bindings)
    }
    
    func createButton(itemCart: CartData.Item) -> ToCartButtonView.ViewModel? {
        
        guard let item = model.catalogElements.value.first(where: { $0.id == itemCart.elementId }) else { return nil }
     
        let button = ToCartButtonView.ViewModel(itemsCount: itemCart.count, maxCount: item.quantity)
        bind(button)
        return button
    }
    
    static func totalPrice(from item: CatElement, with count: Int) -> String {
        
        guard let price = item.price, let double = Double(price.price) else { return "" }
        let formatter = NumberFormatter.formatter(currencyCode: price.currency)
        let total = double * Double(count)
        
        return formatter.string(from: NSNumber(value: total)) ?? ""
    }
    
    func deleteItemFromCart() {
        
        model.action.send(ModelAction.Cart.Update(id: id, count: 0))
    }
    
    func OpenDetailTapped() {
        
        action.send(CartElementViewModelAction.OpenDetail(id: id))
    }
    
}

enum CartElementViewModelAction {
    
    struct OpenDetail: Action {
        
        let id: String
    }
}
