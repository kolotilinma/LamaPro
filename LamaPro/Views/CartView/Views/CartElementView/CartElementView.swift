//
//  CartElementView.swift
//  LamaPro
//
//  Created by Михаил on 25.03.2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct CartElementView: View {
    
    @ObservedObject var viewModel: CartElementViewModel
    
    var body: some View {
        
        VStack(spacing: 16) {
            
            HStack(alignment: .top, spacing: 8) {
                
                ZStack {
                    
                    Color.lamaWhite
                    
                    WebImage(url: viewModel.imageUrl)
                        .resizable()
                        .indicator(.activity)
                        .transition(.fade)
                        .scaledToFit()
                        .frame(alignment: .bottom)
                        .onTapGesture {
                            viewModel.OpenDetailTapped()
                        }
                }
                .frame(width: 108, height: 108)
                .cornerRadius(10)
                
                VStack {
                    
                    HStack(alignment: .top) {
                        
                        Text(viewModel.title)
                            .font(.interBold(size: 13))
                            .foregroundColor(.lamaBlack)
                            .lineLimit(3)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
//                            .onTapGesture {
//                                viewModel.OpenDetailTapped()
//                            }
                        
                        Button(action: viewModel.deleteItemFromCart) {
                            
                            Image(systemName: "xmark")
                                .resizable()
                                .frame(width: 10, height: 10)
                                .foregroundColor(.lamaGray)
                                .padding(4)
                        }
                        .padding(4)
                        
                    }
                    
                    Spacer()
                    
                    HStack {
                        
                        if let count = viewModel.countButton {
                            
                            ToCartButtonView(viewModel: count)
                        }
                        
                        Text(viewModel.price)
                            .font(.interBold(size: 14))
                            .foregroundColor(.lamaBlack)
                            .lineLimit(1)
                            .frame(maxWidth: .infinity, alignment: .trailing)
                    }
                }
                
            }
            .frame(height: 108)
            .background(Color.white.onTapGesture {
                viewModel.OpenDetailTapped()
            })
            
         Divider()
            
        }
    }
}

struct CartElementView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CartElementView(viewModel: .init(model: .emptyMock, id: "1", title: "Ножницы для живой изгороди Hsx92 PowerGear™X 1023631 Fiskars", price: "4 223.94 ₽", imageUrl: nil, countButton: .init(itemsCount: 2, maxCount: 6)))
            .previewLayout(.sizeThatFits)
    }
}
