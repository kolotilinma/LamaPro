//
//  CartSubViewModel.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI
import Combine

final class CartSubViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var status: Status
    @Published var items: [CartElementViewModel]
    @Published var footer: CartFooterViewModel?
    @Published var orderButton: ButtonSimpleView.ViewModel?
    
    private let model: Model
    
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, items: [CartElementViewModel], footer: CartFooterViewModel?, orderButton: ButtonSimpleView.ViewModel?, status: Status = .loading) {
        
        self.model = model
        self.items = items
        self.footer = footer
        self.orderButton = orderButton
        self.status = status
    }
    
    convenience init(model: Model) {
        
        self.init(model: model, items: [], footer: .init(total: 0, countElements: 0, weight: 0), orderButton: nil)
        
        bind()
    }
    
    func bind() {
        
//        action
//            .receive(on: DispatchQueue.main)
//            .sink { [unowned self] action in
//                switch action {
//
//                case _ as CartViewModelAction.OrderTapped:
//
//                    switch model.auth.value {
//
//                    case .active:
//                        self.link = .onfirmCart(ConfirmCartViewModel(model: model, backAction: { [weak self] in
//                            self?.link = nil
//                        }))
//
//                    default :
//                        self.link = .authentification(AuthentificationViewModel(model: model, backAction: { [weak self] in
//                            self?.link = nil
//                        }))
//
//                    }
//
//                case let payload as CartViewModelAction.OpenItemDetail:
//                    link = .itemDetail(ProductDetailViewModel(model: model, item: payload.item, backAction: { [weak self] in
//                        self?.link = nil
//                    }))
//
//                default: break
//                }
//            }
//            .store(in: &bindings)
        
        model.cartElements
            .combineLatest(model.catalogStatus)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] cartElements, catalogStatus in
                withAnimation {
                    switch catalogStatus {
                        
                    case .loading:
                        self.status = .loading
                        
                    case .done:
                        if cartElements.items.isEmpty == false {
                            
                            let items = cartElements.items.compactMap { CartElementViewModel(model: model, itemCart: $0) }
                            self.items = items
                            bind(items)
                            footer = createFooter(cartElements.items)
                            self.status = .normal
                            
                        } else {
                            
                            self.status = .empty
                        }
                    case .error(_):
                        self.status = .error
                    }
                }
            }
            .store(in: &bindings)
    }
    
    func bind(_ items: [CartElementViewModel]) {
        
        for item in items {
            
            item.action
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] action in
                   
                    switch action {
                     
                    case let payload as CartElementViewModelAction.OpenDetail:
                        guard let item = model.catalogElements.value.first(where: { $0.id == payload.id}) else { return }
                        self.action.send(CartViewModelAction.OpenItemDetail(item: item))
                        
                        
                    default: break
                    }
                }
                .store(in: &bindings)
        }
    }
    
    func bind(_ footer: CartFooterViewModel) {
        
        footer.$total
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] total in
                withAnimation {
                    
                    orderButton = .init(title: "Оформить заказ", backgroundColor: .lamaGreen, disabled: total < 3000, height: 50, action: { [weak self] in
                        self?.action.send(CartViewModelAction.OrderTapped())
                    })
                }
            }
            .store(in: &bindings)
    }
    
    func createFooter(_ cartElements: [CartData.Item]) -> CartFooterViewModel {
        
        var total: Double = 0
        var weight: Double = 0
        
        cartElements.forEach { cartElement in
            if let item = model.catalogElements.value.first(where: { $0.id == cartElement.elementId }) {
                
                let price = Double(item.price?.price ?? "")
                total += Double(cartElement.count) * (price ?? 0)
                weight += Double(cartElement.count) * Double(item.weight) / 1000
            }
        }
        
        let error: CartFooterViewModel.ErrorView? = (total > 3000) ? nil : CartFooterViewModel.ErrorView(title: "Минимальная сумма заказа 3000 ₽", subTitle: "Добавьте в корзину что-нибудь еще, чтобы закончить покупку", button: .init(title: "Вернуться в каталог", action: { [weak self] in
            self?.model.action.send(TabBarAction.ChangeTab(selectedTab: 3))
        }))
        
        let footer = CartFooterViewModel(total: total, countElements: cartElements.count, weight: weight, errorView: error)
        bind(footer)
        return footer
    }
    
    enum Status {
        
        case loading
        case empty
        case error
        case normal
    }
}
