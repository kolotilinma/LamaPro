//
//  CartSubView.swift
//  LamaPro
//
//  Created by Михаил on 27.03.2023.
//

import SwiftUI

struct CartSubView: View {
    
    @ObservedObject var viewModel: CartSubViewModel
    
    var body: some View {
        
        switch viewModel.status {
            
        case .normal:
            VStack {
                
                ScrollView(showsIndicators: false) {
                    
                    VStack {
                        
                        ForEach(viewModel.items) { item in
                            
                            CartElementView(viewModel: item)
                            
                        }
                        
                        if let footer = viewModel.footer {
                            
                            CartFooterView(viewModel: footer)
                        }
                    }
                    .padding()
                }
                .resignKeyboardOnDragGesture()
                
                if let orderButton = viewModel.orderButton {
                    
                    ButtonSimpleView(viewModel: orderButton)
                        .padding([.bottom, .horizontal], 16)
                        .padding(.top, 8)
                }
            }
            
        case .empty:
            VStack {
                Spacer()
                
                Image("EmptyCart")
                
//                Text("В вашей корзине пока ничего нет.")
//                    .font(.interBold(size: 13))
//                    .foregroundColor(.lamaBlack)
                
                Spacer()
            }
            
        case .loading:
            VStack {
                Spacer()
                
                Text("Подождите, идет загрузка каталога...")
                    .font(.interBold(size: 13))
                    .foregroundColor(.lamaBlack)
                
                AnimatedHUDView()
                    .frame(width: 40, height: 40)
                
                Spacer()
            }
        case .error:
            Text("error")
        }
    }
}

struct CartSubView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CartSubView(viewModel: .init(model: .emptyMock, items: [
            .init(model: .emptyMock, id: "1", title: "Ножницы для живой изгороди Hsx92 PowerGear™X 1023631 Fiskars", price: "4 223.94 ₽", imageUrl: nil, countButton: .init(itemsCount: 2, maxCount: 6)),
            .init(model: .emptyMock, id: "2", title: "Ножницы для живой изгороди Hsx92 PowerGear™X 1023631 Fiskars", price: "4 223.94 ₽", imageUrl: nil, countButton: .init(itemsCount: 1, maxCount: 2))
        ], footer: .init(total: 12_742.8, countElements: 5, weight: 6), orderButton: .init(title: "Оформить заказ", action: {}), status: .empty))
    }
}
