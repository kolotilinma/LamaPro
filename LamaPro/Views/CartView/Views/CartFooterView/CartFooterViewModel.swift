//
//  CartFooterViewModel.swift
//  LamaPro
//
//  Created by Михаил on 26.03.2023.
//

import Foundation

class CartFooterViewModel: ObservableObject {
    
    @Published var total: Double
    @Published var count: Int
    @Published var weight: Double
    @Published var errorView: ErrorView?
    
    internal init(total: Double, countElements: Int, weight: Double, errorView: ErrorView? = nil) {
        
        self.total = total
        self.count = countElements
        self.weight = weight
        self.errorView = errorView
    }
    
    var totalString: String? {
        
        let formatter = NumberFormatter.formatter(currencyCode: "RUB")

        return formatter.string(from: NSNumber(value: total))
    }
    
    
    var productsCount: String {
        
        if (count == 0) { return "нет товара" }
            
            if (count % 10 == 1 && count % 100 != 11) {
            
            return String(format: "%u товар", count)
                
        } else if ((count % 10 >= 2 && count % 10 <= 4) && !(count % 100 >= 12 && count % 100 <= 14)) {
            
            return String(format: "%u товара", count)
            
        } else  if (count % 10 == 0 || (count % 10 >= 5 && count % 10 <= 9) || (count % 100 >= 11 && count % 100 <= 14)) {
            
            return String(format: "%u товаров", count)
        }
        
        return "Oops!";
    }
    
    var weightString: String {
        
        return "\(weight) кг"
    }
    
    
    class ErrorView: ObservableObject {
         
        let title: String
        let subTitle: String
        let button: Button
        
        struct Button {
            
            let title: String
            let action: () -> Void
        }
        
        internal init(title: String, subTitle: String, button: CartFooterViewModel.ErrorView.Button) {
            
            self.title = title
            self.subTitle = subTitle
            self.button = button
        }
        
    }
}
