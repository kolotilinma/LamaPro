//
//  CartFooterView.swift
//  LamaPro
//
//  Created by Михаил on 26.03.2023.
//

import SwiftUI

struct CartFooterView: View {
    
    @ObservedObject var viewModel: CartFooterViewModel
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 8) {
            
            HStack {
                
                Text("Ваш заказ")
                
                Spacer()
                
                if let total = viewModel.totalString {
                    Text(total)
                }
            }
            .font(.interBold(size: 17))
            .foregroundColor(.lamaBlack)
            
            Text("* Стоимость доставки рассчитывается \nпосле подтверждения заказа менеджером")
                .font(.interMedium(size: 10))
                .foregroundColor(.lamaBlack)
            
            VStack(spacing: 15) {
                
                HStack {
                    
                    Text("Товаров")
                    
                    Spacer()
                    
                    Text(viewModel.productsCount)
                }
                .font(.interRegular(size: 13))
                .foregroundColor(.lamaBlack)
                
                Divider()
                
                HStack {
                    
                    Text("Общий вес")
                    
                    Spacer()
                    
                    Text(viewModel.weightString)
                }
                .font(.interRegular(size: 13))
                .foregroundColor(.lamaBlack)
                
                if let error = viewModel.errorView {
                    
                    HStack(alignment: .top) {
                        
                        Image("crossmark")
                        
                        VStack(alignment: .leading, spacing: 10) {
                            
                            VStack(alignment: .leading, spacing: 4) {
                                
                                Text(error.title)
                                    .font(.interBold(size: 13))
                                    .foregroundColor(.lamaBlack)
                                
                                Text(error.subTitle)
                                    .font(.interRegular(size: 10))
                                    .foregroundColor(.lamaBlack)
                                    .multilineTextAlignment(.leading)
                            }
                            
                            Button {
                                error.button.action()
                            } label: {
                                Text(error.button.title)
                                    .font(.interRegular(size: 10))
                                    .foregroundColor(.lamaGreen)
                                    .underline()
                            }

                        }
                        
                        Spacer()
                    }
                }
            }
            .padding(.top, 15)
        }
    }
}

struct CartFooterView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CartFooterView(viewModel: .init(total: 12_742.8, countElements: 4, weight: 5, errorView: .init(title: "Минимальная сумма заказа 3000 ₽", subTitle: "Добавьте в корзину что-нибудь еще, чтобы закончить покупку", button: .init(title: "Вернуться в каталог", action: {}))))
            .previewLayout(.sizeThatFits)
    }
}
