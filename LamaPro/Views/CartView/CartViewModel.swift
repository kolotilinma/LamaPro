//
//  CartViewModel.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI
import Combine

class CartViewModel: ObservableObject {
    
    let action: PassthroughSubject<Action, Never> = .init()
    
    @Published var navigationBar: NavigationBarView.ViewModel
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    
    let cartSubViewModel: CartSubViewModel
    
    private let model: Model
    
    private var bindings = Set<AnyCancellable>()
    
    internal init(model: Model, navigationBar: NavigationBarView.ViewModel, cartSubViewModel: CartSubViewModel) {
        
        self.model = model
        self.navigationBar = navigationBar
        self.cartSubViewModel = cartSubViewModel
    }
    
    convenience init(model: Model) {
        
        self.init(model: model, navigationBar: .init(title: .text("Корзина")), cartSubViewModel: .init(model: model))
        
        bind()
    }
    
    func bind() {
        
        model.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as TabBarAction.CloseAll:
                    link = nil
                    
                default: break
                }
            }
            .store(in: &bindings)
        
        cartSubViewModel.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    
                case _ as CartViewModelAction.OrderTapped:
                    
                    switch model.auth.value {
                        
                    case .active:
                        self.link = .onfirmCart(ConfirmCartViewModel(model: model, backAction: { [weak self] in
                            self?.link = nil
                        }))
                        
                    default :
                        self.link = .authentification(AuthentificationViewModel(model: model, backAction: { [weak self] in
                            self?.link = nil
                        }))
                        
                    }
                    
                case let payload as CartViewModelAction.OpenItemDetail:
                    link = .itemDetail(ProductDetailViewModel(model: model, item: payload.item, backAction: { [weak self] in
                        self?.link = nil
                    }))
                    
                default: break
                }
            }
            .store(in: &bindings)
        
    }
    
   
    
    func loadCatalog() {
        
    }
    
    enum Link {
        
        case onfirmCart(ConfirmCartViewModel)
        case itemDetail(ProductDetailViewModel)
        case authentification(AuthentificationViewModel)
    }
}

enum CartViewModelAction {
    
    struct OrderTapped: Action { }
    
    struct OpenItemDetail: Action {
        
        let item: CatElement
    }
}
