//
//  CartView.swift
//  LamaPro
//
//  Created by Михаил on 22.01.2023.
//

import SwiftUI

struct CartView: View {
    
    @ObservedObject var viewModel: CartViewModel
    
    var body: some View {
        
        ZStack {
            
            CartSubView(viewModel: viewModel.cartSubViewModel)
            
            //TODO: - из за этих переходов ломается навигация в каталоге
            NavigationLink("", isActive: $viewModel.isLinkActive) {

                if let link = viewModel.link  {

                    switch link {

                    case .onfirmCart(let confirmCart):
                        ConfirmCartView(viewModel: confirmCart)

                    case .itemDetail(let model):
                        ProductDetailView(viewModel: model)

                    case .authentification(let model):
                        AuthentificationView(viewModel: model)
                    }
                }
            }
        }
        .navigationBar(with: viewModel.navigationBar)
//        .onAppear {
//            viewModel.loadCatalog()
//        }
    }
}

struct CartView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CartView(viewModel: .init(model: .emptyMock, navigationBar: .init(title: .text("Корзина")), cartSubViewModel: .init(model: .emptyMock, items: [
            .init(model: .emptyMock, id: "1", title: "Ножницы для живой изгороди Hsx92 PowerGear™X 1023631 Fiskars", price: "4 223.94 ₽", imageUrl: nil, countButton: .init(itemsCount: 2, maxCount: 6)),
            .init(model: .emptyMock, id: "2", title: "Ножницы для живой изгороди Hsx92 PowerGear™X 1023631 Fiskars", price: "4 223.94 ₽", imageUrl: nil, countButton: .init(itemsCount: 1, maxCount: 2))
        ], footer: .init(total: 12_742.8, countElements: 5, weight: 6), orderButton: .init(title: "Оформить заказ", action: {}), status: .normal)))
    }
}
